# Ritchie Board Games

<img src="other/images/logo.png" alt="drawing" width="500"/>

## Используемые технологии
### Фронтенд
- React
- MobX
- Typescript
- Formik
- Yup

### Бэкенд
- .NET
- C#
- EF Core
- Swagger
- CQRS
- Docker


## Основные ссылки

[Диаграмы](https://drive.google.com/file/d/1q198rLfuR0xr7vxhTPsf_997cOEjliqr/view)

[Макет Figma](https://www.figma.com/file/snwkrSBUSPPXa9jF5NDmyW/Ritchie-Board-Games?node-id=0%3A1)



## Основные эндпоинты, доступные после запуска:

| Микросервис | Путь к Swagger | Путь внутри docker
| --- | ----------- | ---
| 🔒 Identity   | <http://localhost:4001/swagger/index.html> | http://ritchieboardgames.identity:4001
| 🎫 Game Event | <http://localhost:4002/swagger/index.html> | http://ritchieboardgames.gameevent.api:4002
| 🧑 Person     | <http://localhost:4003/swagger/index.html> | http://ritchieboardgames.person.api:4003
| 🗒️ Seq        | <http://localhost:8001/> | <http://seq_in_dc:5341>

| Сайт | Путь в интернете | Путь внутри docker |  Путь local
| --- | ----------- | --- | ---
| 🎨 UI        | <https://statuesque-fenglisu-012624.netlify.app/> | <http://ritchieboardgames.ui:3000> | http://localhost:3000/



## Запуск проекта

```
git clone https://gitlab.com/team-ritchie/course-project.git
cd course-project
docker-compose up
```

Дополнительно. Смотрим какие есть ветки, чтобы выбрать, какую запустить:
```
git fetch
git branch -v -a

...
remotes/origin/dev
```
Переходим, например, в ветку dev:
```
git switch dev
```

## Cхема архитектуры всего приложения:

<img src="other/images/structure.png" alt="drawing" width="500"/>

## Разработчики
- Иван Панченко
- Георгий Петров
- Ирина Юдинцева
- Александр Демичев
- Прилежаев Серафим
- Красавцева Татьяна
- Владимир Чернецов
