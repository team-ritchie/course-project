using MediatR;
using Microsoft.Extensions.DependencyInjection;
using RitchieBoardGames.GameEvents.Application.Interfaces;
using System.Reflection;

namespace RitchieBoardGames.GameEvents.Application;

public static class ApplicationServiceRegistration
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services)
    {
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        services.AddMediatR(Assembly.GetExecutingAssembly());
        return services;
    }
}
