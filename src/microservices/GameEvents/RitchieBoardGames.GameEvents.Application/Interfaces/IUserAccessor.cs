namespace RitchieBoardGames.GameEvents.Application.Interfaces;

public interface IUserAccessor
{
    string GetUserName();
}