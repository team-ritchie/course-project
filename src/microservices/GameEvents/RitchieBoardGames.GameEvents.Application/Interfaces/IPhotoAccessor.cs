using Microsoft.AspNetCore.Http;
using RitchieBoardGames.GameEvents.Application.Photos;

namespace RitchieBoardGames.GameEvents.Application.Interfaces;

public interface IPhotoAccessor
{
    Task<PhotoUploadResult> AddPhoto(IFormFile file);
    Task<string> DeletePhoto(string publicId);
}