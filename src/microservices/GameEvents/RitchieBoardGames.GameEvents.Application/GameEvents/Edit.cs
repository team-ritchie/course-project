using AutoMapper;
using FluentValidation;
using MediatR;
using RitchieBoardGames.GameEvents.Application.Core;
using RitchieBoardGames.GameEvents.Domain;
using RitchieBoardGames.GameEvents.Persistence;

namespace RitchieBoardGames.GameEvents.Application.GameEvents
{
    public class Edit
    {
        public class Command : IRequest<Result<Unit>>
        {
            public GameEvent GameEvent { get; set; } = null!;
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.GameEvent).SetValidator(new GameEventValidator());
            }
        }
        
        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                _mapper = mapper;
                _context = context;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var gameEvent = await _context.GameEvents.FindAsync(request.GameEvent.Id);

                if (gameEvent is null) return null;

                _mapper.Map(request.GameEvent, gameEvent);

                var result = await _context.SaveChangesAsync(cancellationToken) > 0;

                if (!result) return Result<Unit>.Failure("Не удалось обновить мероприятие");

                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}