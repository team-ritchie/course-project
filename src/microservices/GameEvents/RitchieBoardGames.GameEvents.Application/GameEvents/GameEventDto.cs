using RitchieBoardGames.GameEvents.Application.Profiles;

namespace RitchieBoardGames.GameEvents.Application.GameEvents;

public class GameEventDto
{
    public Guid Id { get; set; }
    public string? Title { get; set; }
    public DateTime Date { get; set; }
    public string? Description { get; set; }
    public string? City { get; set; }
    public string? Venue { get; set; }
    public int MinPlayersCount { get; set; }
    public int MaxPlayersCount { get; set; }
    public int MinAge { get; set; }
    public int MaxAge { get; set; }
    public string? BoardGame { get; set; }
    public string HostUsername { get; set; }
    public bool IsCancelled { get; set; }
    public ICollection<AttendeeDto> Attendees { get; set; } 
}