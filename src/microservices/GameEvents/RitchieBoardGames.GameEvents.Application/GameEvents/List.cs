using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.GameEvents.Application.Core;
using RitchieBoardGames.GameEvents.Domain;
using RitchieBoardGames.GameEvents.Persistence;

namespace RitchieBoardGames.GameEvents.Application.GameEvents;

public class List
{
    public class Query : IRequest<Result<List<GameEventDto>>> { }

    public class Handler : IRequestHandler<Query, Result<List<GameEventDto>>>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public Handler(DataContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<Result<List<GameEventDto>>> Handle(Query request,
            CancellationToken cancellationToken)
        {
            var gameEvents = await _context.GameEvents
                .ProjectTo<GameEventDto>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return Result<List<GameEventDto>>.Success(gameEvents);
        }
    }
}