using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using RitchieBoardGames.GameEvents.Application.Core;
using RitchieBoardGames.GameEvents.Persistence;

namespace RitchieBoardGames.GameEvents.Application.GameEvents
{
    public class Delete
    {
        public class Command : IRequest<Result<Unit>>
        {
            public Guid Id { get; set; }
        }


        public class DeleteHandler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly DataContext _context;
            public DeleteHandler(DataContext context)
            {
                _context = context;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var gameEvent = await _context.GameEvents.FindAsync(request.Id);

                if (gameEvent == null) return null;

                _context.Remove(gameEvent);

                var result = await _context.SaveChangesAsync(cancellationToken) > 0;

                if (!result) return Result<Unit>.Failure("Не удалось удалить мероприятие");

                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}