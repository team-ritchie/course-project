using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.GameEvents.Application.Core;
using RitchieBoardGames.GameEvents.Application.Interfaces;
using RitchieBoardGames.GameEvents.Domain;
using RitchieBoardGames.GameEvents.Persistence;

namespace RitchieBoardGames.GameEvents.Application.GameEvents
{
    public class Create
    {
        public class Command : IRequest<Result<Unit>>
        {
            public GameEvent GameEvent { get; set; } = null!;
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.GameEvent).SetValidator(new GameEventValidator());
            }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                _userAccessor = userAccessor;
                _context = context;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _context.Users
                    .FirstOrDefaultAsync(x => x.UserName == _userAccessor.GetUserName());

                var attendee = new GameEventAttendee
                {
                    AppUser = user,
                    GameEvent = request.GameEvent,
                    IsHost = true
                };

                request.GameEvent.Attendees.Add(attendee);

                _context.GameEvents.Add(request.GameEvent);
                var result = await _context.SaveChangesAsync() > 0;
                if (!result) return Result<Unit>.Failure("Не удалось создать мероприятие");
                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}