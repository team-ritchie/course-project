using MediatR;
using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.GameEvents.Application.Core;
using RitchieBoardGames.GameEvents.Application.Interfaces;
using RitchieBoardGames.GameEvents.Domain;
using RitchieBoardGames.GameEvents.Persistence;

namespace RitchieBoardGames.GameEvents.Application.GameEvents;

public class UpdateAttendance
{
    public class Command : IRequest<Result<Unit>>
    {
        public Guid Id { get; set; }
    }

    public class Handler : IRequestHandler<Command, Result<Unit>>
    {
        private readonly DataContext _context;
        private readonly IUserAccessor _userAccessor;

        public Handler(DataContext context, IUserAccessor userAccessor)
        {
            _context = context;
            _userAccessor = userAccessor;
        }

        public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
        {
            var gameEvent = await _context.GameEvents
                .Include(a => a.Attendees).ThenInclude(u => u.AppUser)
                .SingleOrDefaultAsync(x => x.Id == request.Id);

            if (gameEvent == null) return null;

            var user = await _context.Users
                .FirstOrDefaultAsync(x => x.UserName == _userAccessor.GetUserName());

            if (user is null) return null;

            var hostUsername = gameEvent.Attendees.FirstOrDefault(x => x.IsHost)?.AppUser?.UserName;

            var attendance = gameEvent.Attendees.FirstOrDefault(x => x.AppUser.UserName == user.UserName);

            if (attendance != null && hostUsername == user.UserName)
                gameEvent.IsCancelled = !gameEvent.IsCancelled;

            if (attendance != null && hostUsername != user.UserName)
                gameEvent.Attendees.Remove(attendance);

            if (attendance == null)
            {
                attendance = new GameEventAttendee
                {
                    AppUser = user,
                    GameEvent = gameEvent,
                    IsHost = false
                };

                gameEvent.Attendees.Add(attendance);
            }

            var result = await _context.SaveChangesAsync() > 0;

            return result
                ? Result<Unit>.Success(Unit.Value)
                : Result<Unit>.Failure("Не удалось обновить участие в мероприятии");
        }
    }
}