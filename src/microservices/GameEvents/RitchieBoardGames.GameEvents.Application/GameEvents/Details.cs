using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.GameEvents.Application.Core;
using RitchieBoardGames.GameEvents.Persistence;

namespace RitchieBoardGames.GameEvents.Application.GameEvents;

public class Details
{
    public class Query : IRequest<Result<GameEventDto>>
    {
        public Guid Id { get; set; }
    }

    public class Handler : IRequestHandler<Query, Result<GameEventDto>>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public Handler(DataContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<Result<GameEventDto>> Handle(Query request, 
            CancellationToken cancellationToken)
        {
            var gameEvent = await _context.GameEvents
                .ProjectTo<GameEventDto>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == request.Id);

            return Result<GameEventDto>
                .Success(gameEvent);
        }
    }
}