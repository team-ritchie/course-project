using FluentValidation;
using RitchieBoardGames.GameEvents.Domain;

namespace RitchieBoardGames.GameEvents.Application.GameEvents;

public class GameEventValidator : AbstractValidator<GameEvent>
{
    public GameEventValidator()
    {
        RuleFor(x=>x.Title).NotEmpty();
        RuleFor(x=>x.Description).NotEmpty();
        RuleFor(x=>x.Date).NotEmpty();
        RuleFor(x=>x.City).NotEmpty();
        RuleFor(x=>x.Venue).NotEmpty();
        RuleFor(x=>x.MinPlayersCount).NotEmpty();
        RuleFor(x=>x.MaxPlayersCount).NotEmpty();
        RuleFor(x=>x.MinAge).NotEmpty();
        RuleFor(x=>x.MaxAge).NotEmpty();
        RuleFor(x=>x.BoardGame).NotEmpty();
    }
}