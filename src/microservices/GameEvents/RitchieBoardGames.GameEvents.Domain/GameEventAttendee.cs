using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RitchieBoardGames.GameEvents.Domain
{
    public class GameEventAttendee
    {
        public string AppUserId { get; set; }
        public AppUser AppUser { get; set; }
        public Guid GameEventId { get; set; }
        public GameEvent GameEvent { get; set; }
        public bool IsHost { get; set; }
    }
}