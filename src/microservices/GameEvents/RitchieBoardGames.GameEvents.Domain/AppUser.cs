using Microsoft.AspNetCore.Identity;

namespace RitchieBoardGames.GameEvents.Domain;

 public class AppUser : IdentityUser
    {
        public string DisplayName { get; set; }
        public string? Bio { get; set; }
        public ICollection<GameEventAttendee> GameEvents { get; set; }
        public ICollection<Photo> Photos { get; set; }

    }