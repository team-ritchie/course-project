using Microsoft.AspNetCore.Identity;
using RitchieBoardGames.GameEvents.Domain;
using RitchieBoardGames.GameEvents.Persistence;

namespace Persistence;

public class Seed
{
    public static async Task SeedData(DataContext context, UserManager<AppUser> userManager)
    {
        if (!userManager.Users.Any() && !context.GameEvents.Any())
        {
            var users = new List<AppUser>{
            new AppUser{DisplayName = "Иван", UserName = "ivan", Email = "ivan@rbg.com"},
            new AppUser{DisplayName = "Ирина", UserName = "irina", Email = "irina@rbg.com"},
            new AppUser{DisplayName = "Владимир", UserName = "vladimir", Email = "vladimir@rbg.com"},
          };

            foreach (var user in users)
            {
                await userManager.CreateAsync(user, "!QAZxsw2");
            }


            var activities = new List<GameEvent>
        {
            new GameEvent
            {
                Title = "Прошедшее мероприятие 2",
                Date = DateTime.Now.AddMonths(-2),
                Description = "Мероприятие, которое закончилось 2 месяца назад",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 17,
                MaxAge = 30,
                BoardGame = "munchkin",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[2],
                        IsHost = true
                    }
                }
            },
            new GameEvent
            {
                Title = "Прошедшее мероприятие 2",
                Date = DateTime.Now.AddMonths(-2),
                Description = "Мероприятие, которое закончилось 2 месяца назад",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 17,
                MaxAge = 30,
                BoardGame = "neuderzhimyeEdinorozhki",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[0],
                        IsHost = true
                    },
                                   new GameEventAttendee
                    {
                        AppUser = users[1],
                        IsHost = false,
                    },
                                   new GameEventAttendee
                    {
                        AppUser = users[2],
                        IsHost = false
                    }
                }
            },
            new GameEvent
            {
                Title = "Прошедшее мероприятие 3",
                Date = DateTime.Now.AddMonths(-3),
                Description = "Мероприятие, которое закончилось 3 месяца назад",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 50,
                MaxAge = 90,
                BoardGame = "catan",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[1],
                        IsHost = true
                    },
                                   new GameEventAttendee
                    {
                        AppUser = users[0],
                        IsHost = false,
                    }
                }
            },
             new GameEvent
            {
                Title = "Прошедшее мероприятие 3",
                Date = DateTime.Now.AddMonths(-3),
                Description = "Мероприятие, которое закончилось 3 месяца назад",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 50,
                MaxAge = 90,
                BoardGame = "monopoly",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[2],
                        IsHost = true
                    },
                    new GameEventAttendee
                    {
                        AppUser = users[1],
                        IsHost = false
                    }
                }
            },
            new GameEvent
            {
                Title = "Прошедшее мероприятие 4",
                Date = DateTime.Now.AddMonths(-4),
                Description = "Мероприятие, которое закончилось 4 месяца назад",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 17,
                MaxAge = 30,
                BoardGame = "mafiyaVsyaSemyaVSboreMaski",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[0],
                        IsHost = true
                    }
                }
            },
            new GameEvent
            {
                Title = "Будущее мероприятие 1",
                Date = DateTime.Now.AddMonths(1),
                Description = "Мероприятие, которое будет через 1 месяц",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 17,
                MaxAge = 80,
                BoardGame = "DnD",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[1],
                        IsHost = true
                    }
                }
            },
            new GameEvent
            {
                Title = "Будущее мероприятие 2",
                Date = DateTime.Now.AddMonths(2),
                Description = "Мероприятие, которое будет через 2 месяца",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 17,
                MaxAge = 35,
                BoardGame = "munchkin",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[2],
                        IsHost = true
                    }
                }
            },
            new GameEvent
            {
                Title = "Будущее мероприятие 3",
                Date = DateTime.Now.AddMonths(3),
                Description = "Мероприятие, которое будет через 3 месяца",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 17,
                MaxAge = 30,
                BoardGame = "DnD",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[0],
                        IsHost = true
                    }
                }
            },
            new GameEvent
            {
                Title = "Будущее мероприятие 4",
                Date = DateTime.Now.AddMonths(4),
                Description = "Мероприятие, которое будет через 4 месяца",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 16,
                MaxAge = 64,
                BoardGame = "catan",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[1],
                        IsHost = true
                    }
                }
            },
            new GameEvent
            {
                Title = "Будущее мероприятие 5",
                Date = DateTime.Now.AddMonths(5),
                Description = "Мероприятие, которое будет через 5 месяцев",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 17,
                MaxAge = 30,
                BoardGame = "monopoly",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[2],
                        IsHost = true
                    }
                }
            },
            new GameEvent
            {
                Title = "Будущее мероприятие 6",
                Date = DateTime.Now.AddMonths(6),
                Description = "Мероприятие, которое будет через 6 месяцев",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 17,
                MaxAge = 35,
                BoardGame = "DnD",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[0],
                        IsHost = true
                    }
                }
            },
            new GameEvent
            {
                Title = "Будущее мероприятие 7",
                Date = DateTime.Now.AddMonths(7),
                Description = "Мероприятие, которое будет через 7 месяцев",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 50,
                MaxAge = 70,
                BoardGame = "mafiyaVsyaSemyaVSboreMaski",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[1],
                        IsHost = true
                    }
                }
            },
            new GameEvent
            {
                Title = "Будущее мероприятие 8",
                Date = DateTime.Now.AddMonths(8),
                Description = "Мероприятие, которое будет через 8 месяцев",
                City = "Москва",
                Venue = "Улица Мира, 3",
                MinPlayersCount = 2,
                MaxPlayersCount = 10,
                MinAge = 17,
                MaxAge = 50,
                BoardGame = "DnD",
                Attendees = new List<GameEventAttendee>{
                    new GameEventAttendee
                    {
                        AppUser = users[2],
                        IsHost = true
                    },
                    new GameEventAttendee
                    {
                        AppUser = users[0],
                        IsHost = false
                    },
                    new GameEventAttendee
                    {
                        AppUser = users[1],
                        IsHost = false
                    }
                }
            },
        };

            await context.GameEvents.AddRangeAsync(activities);
            await context.SaveChangesAsync();
        }
    }
}