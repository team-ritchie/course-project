namespace RitchieBoardGames.GameEvents.Persistence.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
