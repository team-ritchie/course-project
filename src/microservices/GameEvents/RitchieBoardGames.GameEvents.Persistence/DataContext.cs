using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.GameEvents.Domain;

namespace RitchieBoardGames.GameEvents.Persistence;


public class DataContext : IdentityDbContext<AppUser>
{
    public DbSet<GameEvent> GameEvents { get; set; }
    public DbSet<GameEventAttendee> GameEventAttendees { get; set; }
    public DbSet<Photo> Photos { get; set; }

    public DataContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.Entity<GameEventAttendee>(x =>
            x.HasKey(gea => new { gea.AppUserId, gea.GameEventId }));

        builder.Entity<GameEventAttendee>()
            .HasOne(gea => gea.AppUser)
            .WithMany(au => au.GameEvents)
            .HasForeignKey(gea => gea.AppUserId);

        builder.Entity<GameEventAttendee>()
            .HasOne(u => u.GameEvent)
            .WithMany(ge => ge.Attendees)
            .HasForeignKey(gea => gea.GameEventId);
    }
}
