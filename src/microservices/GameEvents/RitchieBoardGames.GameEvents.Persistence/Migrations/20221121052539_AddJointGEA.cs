﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RitchieBoardGames.GameEvents.Persistence.Migrations
{
    public partial class AddJointGEA : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tag",
                table: "GameEvents");

            migrationBuilder.CreateTable(
                name: "GameEventAttendees",
                columns: table => new
                {
                    AppUserId = table.Column<string>(type: "TEXT", nullable: false),
                    GameEventId = table.Column<Guid>(type: "TEXT", nullable: false),
                    IsHost = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameEventAttendees", x => new { x.AppUserId, x.GameEventId });
                    table.ForeignKey(
                        name: "FK_GameEventAttendees_AspNetUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameEventAttendees_GameEvents_GameEventId",
                        column: x => x.GameEventId,
                        principalTable: "GameEvents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GameEventAttendees_GameEventId",
                table: "GameEventAttendees",
                column: "GameEventId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameEventAttendees");

            migrationBuilder.AddColumn<string>(
                name: "Tag",
                table: "GameEvents",
                type: "TEXT",
                nullable: true);
        }
    }
}
