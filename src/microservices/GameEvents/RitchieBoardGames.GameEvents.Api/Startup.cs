
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using RitchieBoardGames.GameEvents.Api.Extensions;
using RitchieBoardGames.GameEvents.Api.Middleware;
using RitchieBoardGames.GameEvents.Application;
using RitchieBoardGames.GameEvents.Application.GameEvents;

namespace RitchieBoardGames.GameEvents.Api
{
    public class Startup
    {
        private readonly IConfiguration _config;
        public Startup(IConfiguration config)
        {
            _config = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationServices();
            services.AddControllers(configure =>
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                configure.Filters.Add(new AuthorizeFilter(policy));

                // чтобы возвращать ошибку 406, если подается плохой формат (не json)
                configure.ReturnHttpNotAcceptable = true;

                configure.Filters.Add(
                    new ProducesAttribute("application/json"));

                configure.Filters.Add(
                    new ConsumesAttribute("application/json"));

                configure.Filters.Add(
                    new ProducesResponseTypeAttribute(StatusCodes.Status400BadRequest));

                configure.Filters.Add(
                    new ProducesResponseTypeAttribute(StatusCodes.Status406NotAcceptable));

                configure.Filters.Add(
                    new ProducesResponseTypeAttribute(StatusCodes.Status500InternalServerError));
            });

            services.AddFluentValidationAutoValidation();
            services.AddValidatorsFromAssemblyContaining<Create>();

            services.AddApiServices(_config);
            services.AddIdentityServices(_config);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCustomExceptionHandler();

            if (env.IsDevelopment())
            {
                app.UseStaticFiles();
                app.UseSwagger();
                app.UseSwaggerUI(setupAction =>
                {
                    setupAction.SwaggerEndpoint("RitchieBoardGamesSpecification/swagger.json", "Board Games API");
                    setupAction.RoutePrefix = "swagger";

                    setupAction.InjectStylesheet("/assets/custom-ui.css");
                });
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseCustomRequestLogging();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
