using RitchieBoardGames.GameEvents.Api.Middleware;

namespace RitchieBoardGames.GameEvents.Api.Extensions;

public static class MiddlewareExtensions
{
    public static IApplicationBuilder UseCustomExceptionHandler(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<ExceptionMiddleware>();
    }

    public static void AddCustomSwaggerGen(this IServiceCollection services)
    {
        services.AddSwaggerGen(setupAction =>
        {
            setupAction.SwaggerDoc("RitchieBoardGamesSpecification", new()
            {
                Title = "Board Games API",
                Version = "1",
                Description = "С помощью данного API можно управлять организацией настольных" +
                " игр в ресторане в выбранном городе",
                Contact = new()
                {
                    Name = "Сайт разработчиков",
                    Url = new Uri("https://gitlab.com/team-ritchie/course-project/")
                },
                License = new()
                {
                    Name = "Лицензия MIT",
                    Url = new Uri("https://opensource.org/licenses/MIT")
                }
            });
        });
    }
}
