using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.GameEvents.Application.Interfaces;
using RitchieBoardGames.GameEvents.Infrastructure.Photos;
using RitchieBoardGames.GameEvents.Infrastructure.Security;
using RitchieBoardGames.GameEvents.Persistence;

namespace RitchieBoardGames.GameEvents.Api.Extensions;

public static class ApiServiceExtensions
{
    public static IServiceCollection AddApiServices(this IServiceCollection services,
        IConfiguration config)
    {
        services.AddEndpointsApiExplorer();
        services.AddCustomSwaggerGen();
        services.AddCors(opt =>
        {
            opt.AddPolicy("CorsPolicy", policy =>
            {
                policy.AllowAnyMethod().AllowAnyHeader().WithOrigins("http://localhost:3000");
            });
        });
        services.AddDbContext<DataContext>(opt =>
        {
            opt.UseSqlite(config.GetConnectionString("DefaultConnection"));
        });
        services.AddScoped<IUserAccessor, UserAccessor>();
        services.Configure<CloudinarySettings>(config.GetSection("Cloudinary"));
        services.AddScoped<IPhotoAccessor, PhotoAccessor>();
        return services;
    }
}