using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.GameEvents.Application.Profiles;

namespace RitchieBoardGames.GameEvents.Api.Controllers;

public class ProfilesController : BaseApiController
{
    [HttpGet("{username}")]
    public async Task<IActionResult> GetProfile(string username)
    {
        return HandleResult(await Mediator.Send(new Details.Query { Username = username }));
    }
}