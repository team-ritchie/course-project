using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.GameEvents.Domain;
using RitchieBoardGames.GameEvents.Application.GameEvents;
using Microsoft.AspNetCore.Authorization;

namespace RitchieBoardGames.GameEvents.Api.Controllers;

public class GameEventsController : BaseApiController
{
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> GetGameEvents()
    {
        return HandleResult(await Mediator.Send(new List.Query()));
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> GetGameEvent(Guid id)
    {
        return HandleResult(await Mediator.Send(new Details.Query { Id = id }));
    }

    [HttpPost]
    public async Task<IActionResult> CreateGameEvent(GameEvent gameEvent)
    {
        return HandleResult(await Mediator.Send(new Create.Command { GameEvent = gameEvent }));
    }

    [Authorize(Policy = "IsGameEventHost")]
    [HttpPut("{id}")]
    public async Task<IActionResult> EditGameEvent(Guid id, GameEvent gameEvent)
    {
        gameEvent.Id = id;
        return HandleResult(await Mediator.Send(new Edit.Command { GameEvent = gameEvent }));
    }

    [Authorize(Policy = "IsGameEventHost")]
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteGameEvent(Guid id)
    {
        return HandleResult(await Mediator.Send(new Delete.Command { Id = id }));
    }

    [HttpPost("{id}/attend")]
    public async Task<IActionResult> Attend(Guid id)
    {
        return HandleResult(await Mediator.Send(new UpdateAttendance.Command { Id = id }));
    }
}
