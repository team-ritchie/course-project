namespace RitchieBoardGames.Identity.Models
{
    public enum UserRoles
    {
        None,
        Admin,
        User
    }
}
