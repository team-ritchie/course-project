﻿namespace RitchieBoardGames.Identity.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}