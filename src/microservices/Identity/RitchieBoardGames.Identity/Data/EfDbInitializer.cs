﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace RitchieBoardGames.Identity.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly ApplicationContext _dataContext;

        public EfDbInitializer(ApplicationContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            if (_dataContext.Database.GetPendingMigrations().Any())
            {
                _dataContext.Database.Migrate();
            }
        }
    }
}