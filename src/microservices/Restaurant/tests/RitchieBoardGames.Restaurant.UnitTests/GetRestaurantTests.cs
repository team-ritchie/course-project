using Moq;
using RitchieBoardGames.Restaurant.Api.Controllers;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;
using RitchieBoardGames.Restaurant.Persistence.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace RitchieBoardGames.Restaurant.UnitTests
{

    public class GetRestaurantTests
    {
        private readonly Mock<IRestaurantRepository> _mockRestaurantRepository;
        private readonly Mock<IPersonRepository> _mockPersonRepository;
        public GetRestaurantTests()
        {
            _mockRestaurantRepository = RepositoryMocks.GetRestaurantRepository();
            _mockPersonRepository = RepositoryMocks.GetPersonRepository();
        }

        [Fact]
        public async Task Controller_GetRestaurantsReturnsValidResult()
        {

            var restController = new RestaurantController(_mockRestaurantRepository.Object,
                _mockPersonRepository.Object, null, null, null);

            try
            {
                await restController.GetRestaurantsAsync();

                var allGameEvents = await _mockRestaurantRepository.Object.GetAllAsync();
                Assert.Equal(2, allGameEvents.Count());
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }

        [Theory]
        [InlineData("D5550DBB-26FB-4D4B-A3B8-BF3BAA49D789", true)] // existing id
        [InlineData("64769ED2-35F8-4C74-994A-50F1123E9740", false)] // unexisting id
        public async Task Controller_GetRestaurantByIdReturnsValidResult(string strId, bool expectedId)
        {
            Guid id = System.Guid.Parse(strId);

            var restController = new RestaurantController(_mockRestaurantRepository.Object,
                _mockPersonRepository.Object, null,null, null);

            try
            {
                var result = await restController.GetRestaurantByIdAsync(id);

                if (expectedId)
                {
                    Assert.NotNull(result);
                    Assert.Equal(id, result.Value.Id);
                }
                else 
                {
                    if (result.Value == null)
                        return;

                    throw new Exception("Restarant was returned by different id.");
                }
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }
    }
}