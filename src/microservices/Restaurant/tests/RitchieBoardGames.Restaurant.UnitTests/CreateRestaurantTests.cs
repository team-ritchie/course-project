using Moq;
using FluentAssertions;
using RitchieBoardGames.Restaurant.Api.Controllers;
using RitchieBoardGames.Restaurant.Api.Models;
using RitchieBoardGames.Restaurant.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;
using RitchieBoardGames.Restaurant.Api.Infrastructure.Services;
using RitchieBoardGames.Restaurant.Core.Infrastructure.Services;

namespace RitchieBoardGames.Restaurant.UnitTests
{

    public class CreateRestaurantTests
    {
        private readonly Mock<IRestaurantRepository> _mockRestaurantRepository;
        private readonly Mock<IPersonRepository> _mockPersonRepository;
        private readonly Mock<IIdentityService> _mockIdentityServise;
        public CreateRestaurantTests()
        {
            _mockRestaurantRepository = RepositoryMocks.GetRestaurantRepository();
            _mockPersonRepository = RepositoryMocks.GetPersonRepository();
            _mockIdentityServise = RepositoryMocks.GetIdentityService();
        }

        [Fact]
        public async Task Controller_ValidRestaurantAddedToRestaurantRepo()
        {

            var restController = new RestaurantController(_mockRestaurantRepository.Object,
                _mockPersonRepository.Object, _mockIdentityServise.Object,null, null);

            try
            {
                await restController.CreateRestaurantAsync(
                    new CreateOrEditRestaurantRequest()
                    {
                        Name = "Test1",
                        Address = "Test1 Address",
                        Coordinates = "55.801550, 37.576037",
                        City = "Москва",
                        Description = "Test1",
                        Phone = "+79998887766",
                        ImageUri = "localhost/img/image1.jpg",
                        WorkingHours = "ЕЖЕДНЕВНО c 9:00 до 20:00"
                    }); 
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }

        [Theory]
        [InlineData("")]
        [InlineData("12345670910 12345670920 12345670930 12345670940 12345670950 12345670960 12345670970 12345670980 12345670990 123456709100")]
        public async Task Controller_CreateReturnsErrorIfNameIsNotCorrect(string name)
        {
            var restController = new RestaurantController(_mockRestaurantRepository.Object,
                _mockPersonRepository.Object, null, null, null);

            try
            {
                await restController.CreateRestaurantAsync(
                    new CreateOrEditRestaurantRequest()
                    {
                        Name = name,
                        Address = "Test1 Address",
                        Coordinates = "55.801550, 37.576037",
                        City = "Москва",
                        Description = "Test1",
                        Phone = "+79998887766",
                        ImageUri = "localhost/img/image1.jpg",
                        WorkingHours = "ЕЖЕДНЕВНО c 9:00 до 20:00"
                    }); ;

                Assert.True(false, "Name is not valiadated.");

            }
            catch (Core.Exceptions.ValidationException ve) 
            {
                if (string.IsNullOrEmpty(name))
                {
                    if (ve.ValidationErrors.Contains("Название не указано."))
                        return;
                }
                else 
                {
                    if (ve.ValidationErrors.Contains("Название слишком длинное."))
                        return;
                }

                Assert.True(false, "Name is not valiadated. " + ve.Message);
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }

        [Theory]
        [InlineData("")]
        [InlineData("12345670910 12345670920 12345670930 12345670940 12345670950 12345670960 12345670970 12345670980 12345670990 123456709100")]
        public async Task Controller_CreateReturnsErrorIfCityIsNotCorrect(string city)
        {
            var restController = new RestaurantController(_mockRestaurantRepository.Object,
                _mockPersonRepository.Object, null, null, null);

            try
            {
                await restController.CreateRestaurantAsync(
                    new CreateOrEditRestaurantRequest()
                    {
                        Name = "Test",
                        Address = "Test1 Address",
                        Coordinates = "55.801550, 37.576037",
                        City = city,
                        Description = "Test1",
                        Phone = "+79998887766",
                        ImageUri = "localhost/img/image1.jpg",
                        WorkingHours = "ЕЖЕДНЕВНО c 9:00 до 20:00"
                    }); ;

                Assert.True(false, "City is not valiadated.");

            }
            catch (Core.Exceptions.ValidationException ve)
            {
                if (string.IsNullOrEmpty(city))
                {
                    if (ve.ValidationErrors.Contains("Город не указан."))
                        return;
                }
                else
                {
                    if (ve.ValidationErrors.Contains("Название города слишком длинное."))
                        return;
                }

                Assert.True(false, "City is not valiadated. ");
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }
        [Fact]
        public async Task Controller_CreateReturnsErrorIfAddressIsNotSet()
        {
            var restController = new RestaurantController(_mockRestaurantRepository.Object,
                _mockPersonRepository.Object, null, null, null);

            try
            {
                await restController.CreateRestaurantAsync(
                    new CreateOrEditRestaurantRequest()
                    {
                        Name = "Test",
                        Address = "",
                        Coordinates = "55.801550, 37.576037",
                        City = "Москва",
                        Description = "Test1",
                        Phone = "+79998887766",
                        ImageUri = "localhost/img/image1.jpg",
                        WorkingHours = "ЕЖЕДНЕВНО c 9:00 до 20:00"
                    }); ;

                Assert.True(false, "Address is not valiadated.");

            }
            catch (Core.Exceptions.ValidationException ve)
            {

                if (ve.ValidationErrors.Contains("Адрес не указан."))
                    return;

                Assert.True(false, "Address is not valiadated. ");
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }

        [Fact]
        public async Task Controller_CreateReturnsErrorIfPhoneIsNotCorrect()
        {
            var restController = new RestaurantController(_mockRestaurantRepository.Object,
                _mockPersonRepository.Object, null,null, null);

            try
            {
                await restController.CreateRestaurantAsync(
                    new CreateOrEditRestaurantRequest()
                    {
                        Name = "Test",
                        Address = "test",
                        Coordinates = "55.801550, 37.576037",
                        City = "Москва",
                        Description = "Test1",
                        Phone = "+7999888776600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                        ImageUri = "localhost/img/image1.jpg",
                        WorkingHours = "ЕЖЕДНЕВНО c 9:00 до 20:00"
                    }); ;

                Assert.True(false, "Phone is not valiadated.");

            }
            catch (Core.Exceptions.ValidationException ve)
            {

                if (ve.ValidationErrors.Contains("Неверный формат телефона."))
                    return;

                Assert.True(false, "Phone is not valiadated. ");
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }

    }
}