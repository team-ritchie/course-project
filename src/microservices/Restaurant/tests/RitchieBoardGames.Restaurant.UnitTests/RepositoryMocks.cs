using Microsoft.AspNetCore.Mvc;
using Moq;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;
using RitchieBoardGames.Restaurant.Persistence.Repositories;
using RitchieBoardGames.Restaurant.Api.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using RitchieBoardGames.Restaurant.Core.Infrastructure.Services;

namespace RitchieBoardGames.Restaurant.UnitTests
{

    internal class RepositoryMocks
    {
        public static Mock<IIdentityService> GetIdentityService()
        {
            var mockIdentityService = new Mock<IIdentityService>();
            var person = new Core.Domain.Person() { Id = Guid.Parse("0838C731-136C-4E71-A89D-4447430929C6"), Name = "Петр Иванов" };

            mockIdentityService.Setup(r => r.GetUserIdentity()).Returns("");
            mockIdentityService.Setup(r => r.GetUserName()).Returns("PeterIvanov");

            return mockIdentityService;
        }

        public static Mock<IRestaurantRepository> GetRestaurantRepository()
        {
            var restaurants = 
                new List<Core.Domain.Restaurant>
            {
                new Core.Domain.Restaurant
                {
                    Id = System.Guid.Parse("D5550DBB-26FB-4D4B-A3B8-BF3BAA49D789"),
                    Name = "N1",
                    City = "Москва",
                    Address = "Адрес1",
                    Phone = "+79998887766",
                    PersonIds = new List<Guid>() { Guid.Parse("0838C731-136C-4E71-A89D-4447430929C6") }
                },
                new Core.Domain.Restaurant
                {
                    Id = System.Guid.Parse("C9EFE17C-1987-4634-B648-5053551C07BF"),
                    Name = "N2",
                    City = "Москва",
                    Address = "Адрес2",
                    Phone = "+79998887755",
                    PersonIds = new List<Guid>() { Guid.Parse("750E458F-EAE1-4A84-965C-A6819A388BF2") }
                },
            };

            var mockRestaurantRepository = new Mock<IRestaurantRepository>();

            mockRestaurantRepository.Setup(r => r.GetAllAsync()).ReturnsAsync(restaurants);
            mockRestaurantRepository.Setup(r => r.AddAsync(It.IsAny<Core.Domain.Restaurant>()));
            mockRestaurantRepository.Setup(r => r.UpdateAsync(It.IsAny<Core.Domain.Restaurant>()));
            mockRestaurantRepository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(
                (Guid id) =>
                {
                    return restaurants.FirstOrDefault(x => x.Id == id);
                });
            mockRestaurantRepository.Setup(r => r.DeleteAsync(It.IsAny<Core.Domain.Restaurant>())).Callback(
            (Core.Domain.Restaurant restaurant) =>
            {
                restaurants.Remove(restaurant);
            });


            return mockRestaurantRepository;
        }
        public static Mock<IPersonRepository> GetPersonRepository()
        {
            var persons = new List<Core.Domain.Person>
            {
                new Core.Domain.Person(){ Id = Guid.Parse("0838C731-136C-4E71-A89D-4447430929C6"), Name = "Петр Иванов" },
                new Core.Domain.Person() { Id = Guid.Parse("750E458F-EAE1-4A84-965C-A6819A388BF2"), Name = "Сергей Иванов" },
            };

            var mockPersonRepository = new Mock<IPersonRepository>();

            mockPersonRepository.Setup(r => r.GetAllAsync()).ReturnsAsync(persons);
            mockPersonRepository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(
                (Guid id) =>
                {
                    return persons.FirstOrDefault(x => x.Id == id);
                });

            mockPersonRepository.Setup(r => r.AddAsync(It.IsAny<Core.Domain.Person>()));
            mockPersonRepository.Setup(r => r.UpdateAsync(It.IsAny<Core.Domain.Person>()));
            mockPersonRepository.Setup(r => r.GetRangeByIdsAsync(It.IsAny<List<Guid>>()))
            .ReturnsAsync(
                (List<Guid> ids) =>
                { 
                    return ( from row in persons
                             where ids.Contains(row.Id)
                             select row); 
                });

            return mockPersonRepository;
        }
    }
}
