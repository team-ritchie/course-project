using Moq;
using FluentAssertions;
using RitchieBoardGames.Restaurant.Api.Controllers;
using RitchieBoardGames.Restaurant.Api.Models;
using RitchieBoardGames.Restaurant.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;

namespace RitchieBoardGames.Restaurant.UnitTests
{

    public class DeleteRestaurantTests
    {
        private readonly Mock<IRestaurantRepository> _mockRestaurantRepository;
        private readonly Mock<IPersonRepository> _mockPersonRepository;
        public DeleteRestaurantTests()
        {
            _mockRestaurantRepository = RepositoryMocks.GetRestaurantRepository();
            _mockPersonRepository = RepositoryMocks.GetPersonRepository();
        }

        [Fact]
        public async Task Controller_ValidRestaurantDeletedFromRestaurantRepo()
        {

            var restController = new RestaurantController(_mockRestaurantRepository.Object,
                _mockPersonRepository.Object, null, null, null);

            try
            {
                Guid id = System.Guid.Parse("D5550DBB-26FB-4D4B-A3B8-BF3BAA49D789");

                await restController.DeleteRestaurantAsync(id); 

                var allGameEvents = await _mockRestaurantRepository.Object.GetAllAsync();
                Assert.Single(allGameEvents);


                var res = allGameEvents.FirstOrDefault(r => r.Id == id);
                if(res != null)
                    Assert.True(false, "Restaurant was not deleted.");
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }

        [Fact]
        public async Task Controller_DeleteNothingIfIdIsNotCorrect()
        {
            Guid id = Guid.Parse("D5550DBB-26FB-4D4B-A3B8-BF3BAA49D721");

            // even
            var restController = new RestaurantController(_mockRestaurantRepository.Object,
                _mockPersonRepository.Object, null, null, null);

            try
            {
                await restController.DeleteRestaurantAsync(id);
                var allGameEvents = await _mockRestaurantRepository.Object.GetAllAsync();
                Assert.Equal(2, allGameEvents.Count());
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }
    }
}