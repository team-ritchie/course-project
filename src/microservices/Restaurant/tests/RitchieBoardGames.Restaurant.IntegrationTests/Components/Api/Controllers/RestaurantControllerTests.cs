﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Driver;
using RitchieBoardGames.Restaurant.Core.Domain;
using RitchieBoardGames.Restaurant.Persistence.Repositories;
using RitchieBoardGames.Restaurant.Api.Controllers;
using Xunit;
using Microsoft.Extensions.Logging;
using RitchieBoardGames.Restaurant.Api;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System.Collections.Generic;
using RitchieBoardGames.Restaurant.Api.Models;
using System.Net.Http;
using System.Text;

namespace RitchieBoardGames.Restaurant.IntegrationTests.Components.Api.Controllers
{
    [Collection(EfDatabaseCollection.DbCollection)]
    public class RestaurantsControllerTests: IClassFixture<TestWebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        public RestaurantsControllerTests(TestWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task RestaurantController_GetList_ReturnsSuccessResult()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/restaurants/");

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<List<RestaurantShortResponse>>(responseString);

            Assert.IsType<List<RestaurantShortResponse>>(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task RestaurantController_GetByCity_ReturnsSuccessResult()
        {
            var client = _factory.CreateClient();
            
            var response = await client.GetAsync("/api/restaurants/by-city?city=Москва");

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<List<RestaurantShortResponse>>(responseString);

            Assert.IsType<List<RestaurantShortResponse>>(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task RestaurantController_Create_ReturnsSuccessResult()
        {
            var client = _factory.CreateClient();
            string restaurantLoad = JsonConvert.SerializeObject(new
            {
                name = "test5",
                city = "Новгород",
                address = "ул. Ленина, д.5",
                coordinates = "55.914291, 37.802723",
                workinghours = "Ежедневно: c 10:00 до 23:00",
                phone = "+79998887766",
                descrtiption = "тест",
                personids = new List<int> { 1 }
            });

            var content = new StringContent(restaurantLoad, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync("/api/restaurants", content);
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RestaurantController_GetById_ReturnsSuccessResult()
        {
            var client = _factory.CreateClient();
            HttpResponseMessage response = await client.GetAsync("/api/restaurants/1F19D734-05A5-472B-9929-558466AE5648");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<RestaurantResponse>(responseString);
            Assert.IsType<RestaurantResponse>(result);
            Assert.NotNull(result);
            string returned_id = result != null ? result.Id.ToString().ToUpper() : "";
            Assert.Equal("1F19D734-05A5-472B-9929-558466AE5648", returned_id);
        }

        [Fact]
        public async Task RestaurantController_Update_ReturnsSuccessResult()
        {
            var client = _factory.CreateClient();
            string restaurantLoad = JsonConvert.SerializeObject(new
            {
                name = "test6",
                city = "Новгород",
                address = "ул. Ленина, д.6",
                coordinates = "55.914291, 37.802723",
                workinghours = "Ежедневно: c 10:00 до 23:00",
                phone = "+79998887766",
                descrtiption = "тест",
                personids = new List<int> { 1 }
            });

            var content = new StringContent(restaurantLoad, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync("/api/restaurants/1F19D734-05A5-472B-9929-558466AE5648", content);
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RestaurantController_Delete_ReturnsSuccessResult()
        {
            var client = _factory.CreateClient();
            var response = await client.DeleteAsync("/api/restaurants/F704135A-7DED-4930-AD48-677697CE1474");
            response.EnsureSuccessStatusCode();
        }

    }
}