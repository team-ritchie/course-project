﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Driver;
using RitchieBoardGames.Restaurant.Core.Domain;
using RitchieBoardGames.Restaurant.Persistence.Repositories;
using RitchieBoardGames.Restaurant.Api.Controllers;
using Xunit;
using Microsoft.Extensions.Logging;
using RitchieBoardGames.Restaurant.Api;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System.Collections.Generic;
using RitchieBoardGames.Restaurant.Api.Models;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;

namespace RitchieBoardGames.Restaurant.IntegrationTests.Components.Api.Controllers
{
    [Collection(EfDatabaseCollection.DbCollection)]
    public class PersonControllerTests: IClassFixture<TestWebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        public PersonControllerTests(TestWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task PersonController_GetList_ReturnsSuccessResult()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/persons/");


            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<List<PersonResponse>>(responseString);

            Assert.IsType<List<PersonResponse>>(result);
            Assert.NotEmpty(result);
        }
    }
}