﻿using System;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using RitchieBoardGames.Restaurant.IntegrationTests.Data;

namespace RitchieBoardGames.Restaurant.IntegrationTests
{
    public class EfDatabaseFixture: IDisposable
    {
        private readonly EfTestDbInitializer _efTestDbInitializer;
        
        public EfDatabaseFixture()
        {
            var config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.test.json")
            .Build();

            var connectionString = config["TestDb:ConnectionString"];
            var dbName = config["TestDb:Database"];

            var mongoClient = new MongoClient(connectionString);
            DbContext = mongoClient.GetDatabase(dbName);

            _efTestDbInitializer= new EfTestDbInitializer(DbContext);
        }

        public void Dispose()
        {
            _efTestDbInitializer.CleanDb();
        }

        public IMongoDatabase DbContext { get; private set; }
    }
}