﻿using MongoDB.Driver;
using RitchieBoardGames.Restaurant.Core.Domain;
using RitchieBoardGames.Restaurant.Persistence;
using RitchieBoardGames.Restaurant.Persistence.Data;

namespace RitchieBoardGames.Restaurant.IntegrationTests.Data
{
    public class EfTestDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _db;

        public EfTestDbInitializer(IMongoDatabase dataContext)
        {
            _db = dataContext;
        }

        public void InitializeDb()
        {
            var _RestaurantCollection = _db.GetCollection<Core.Domain.Restaurant>("Restaurant");
            var _PersonCollection = _db.GetCollection<Person>("Person");

            if (_RestaurantCollection.CountDocuments<Core.Domain.Restaurant>(_ => true) > 0)
                return;

            _PersonCollection.InsertManyAsync(TestDataFactory.Persons);
            _RestaurantCollection.InsertManyAsync(TestDataFactory.Restaurants);
        }

        public void CleanDb()
        {
            var _RestaurantCollection = _db.GetCollection<Core.Domain.Restaurant>("Restaurant");
            var _PersonCollection = _db.GetCollection<Person>("Person");

            if (_PersonCollection.EstimatedDocumentCount() > 0)
                _PersonCollection.DeleteMany(_ => true);

            if (_RestaurantCollection.EstimatedDocumentCount() > 0)
                _RestaurantCollection.DeleteMany(_ => true);

            _db.DropCollection("Restaurant");
            _db.DropCollection("Person");
        }
    }
}