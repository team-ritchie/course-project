﻿using System;
using System.Collections.Generic;
using System.Linq;
using RitchieBoardGames.Restaurant.Core.Domain;

namespace RitchieBoardGames.Restaurant.IntegrationTests.Data
{
    public static class TestDataFactory
    {
        public static List<Core.Domain.Restaurant> Restaurants => new List<Core.Domain.Restaurant>()
        {
            new Core.Domain.Restaurant()
            {
                Id = Guid.Parse("F704135A-7DED-4930-AD48-677697CE1474"),
                Address = "Address1",
                Coordinates= "55.914291, 37.802723",
                City = "Москва",
                Description = "test restaurant 1",
                Name = "N1",
                ImageUri = "",
                Phone = "+74955554433",
                WorkingHours = "Ежедневно: c 9:00 до 20:00 ",
                PersonIds = new List<Guid>(){ Guid.Parse("88013AFE-7068-4CED-BB6E-995DEB62F463") }
            },
            new Core.Domain.Restaurant()
            {
                Id = Guid.Parse("1F19D734-05A5-472B-9929-558466AE5648"),
                Address = "Address2",
                Coordinates= "55.914291, 37.802725",
                City = "Москва",
                Description = "test restaurant 2",
                Name = "N2",
                ImageUri = "",
                Phone = "+74955554422",
                WorkingHours = "пн-пт,вс: c 12:00 до 22:00, сб.: с 12:00 до 5:00 ",
                PersonIds = new List<Guid>(){ Guid.Parse("88013AFE-7068-4CED-BB6E-995DEB62F463") }
            },
            new Core.Domain.Restaurant()
            {
                Id = Guid.Parse("1998F687-1B41-4FEB-A8E6-9ACF232AD5A6"),
                Address = "Address3",
                Coordinates= "55.914291, 37.802725",
                City = "Москва",
                Description = "test restaurant 3",
                Name = "N3",
                ImageUri = "",
                Phone = "+74955554422",
                WorkingHours = "пн-пт,вс: c 12:00 до 22:00, сб.: с 12:00 до 5:00 ",
                PersonIds = new List<Guid>(){ Guid.Parse("88013AFE-7068-4CED-BB6E-995DEB62F463") }
            },
        };
        public static List<Person> Persons => new List<Person>()
        {
            new Person() { Id = Guid.Parse("88013AFE-7068-4CED-BB6E-995DEB62F463"), Name = "Илья Соколов" },
            new Person() { Id = Guid.Parse("AC74D2EC-7EDE-43B6-979E-6874DC55B445"), Name = "Анастасия Иванова" }
        };

    }
}