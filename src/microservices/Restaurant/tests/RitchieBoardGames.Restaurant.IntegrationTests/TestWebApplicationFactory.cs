﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using RitchieBoardGames.Restaurant.Core.Abstractions;
using RitchieBoardGames.Restaurant.Core.Domain;
using RitchieBoardGames.Restaurant.Persistence;
using RitchieBoardGames.Restaurant.IntegrationTests.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Authorization.Policy;
using Microsoft.Extensions.Configuration;

namespace RitchieBoardGames.Restaurant.IntegrationTests
{
    public class TestWebApplicationFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup: class
    {

        public IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.test.json")
                .AddEnvironmentVariables()
                .Build();
            return config;
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                
                // remove MongoDb elements
                RemoveElementFromServices(services, typeof(IMongoCollection<Core.Domain.Restaurant>));
                RemoveElementFromServices(services, typeof(IMongoCollection<Core.Domain.Person>));
                RemoveElementFromServices(services, typeof(IMongoDatabase));
                RemoveElementFromServices(services, typeof(IMongoClient));

                // Disable authentication and authorization.
                services.RemoveAll<IPolicyEvaluator>();
                services.TryAddSingleton<IPolicyEvaluator, DisableAuthenticationPolicyEvaluator>();

                // Init test database
                IMongoDatabase dbContext = InitMongoDb(services);

                var sp = services.BuildServiceProvider();

                using var scope = sp.CreateScope();
                var scopedServices = scope.ServiceProvider;
                var logger = scopedServices
                    .GetRequiredService<ILogger<TestWebApplicationFactory<TStartup>>>();
                
                try
                {
                    new EfTestDbInitializer(dbContext).InitializeDb();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Проблема во время заполнения тестовой базы. " +
                                        "Ошибка: {Message}", ex.Message);
                }
            });
        }

        private void RemoveElementFromServices(IServiceCollection services, System.Type type)
        {
            ServiceDescriptor descriptor = services.SingleOrDefault(
                d => d.ServiceType == type);
            if (descriptor != null)
                services.Remove(descriptor);
        }

        private IMongoDatabase InitMongoDb(IServiceCollection services)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.test.json")
                .Build();

            var connectionString = config["TestDb:ConnectionString"];
            var dbName = config["TestDb:Database"];

            var mongoClient = new MongoClient(connectionString);
            var database = mongoClient.GetDatabase(dbName);
            var restaurantCollection = database.GetCollection<Core.Domain.Restaurant>("Restaurant");
            var personCollection = database.GetCollection<Core.Domain.Person>("Person");

            services.AddSingleton<IMongoClient>(_ => mongoClient);
            services.AddSingleton(serviceProvider => database);
            services.AddSingleton(serviceProvider => restaurantCollection);
            services.AddSingleton(serviceProvider => personCollection);

            services.AddScoped(serviceProvider =>
                                serviceProvider.GetRequiredService<IMongoClient>()
                .StartSession());

            return database;
        }


    }
}