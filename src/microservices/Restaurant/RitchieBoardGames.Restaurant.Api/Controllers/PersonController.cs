﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Restaurant.Api.Models;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;
using RitchieBoardGames.Restaurant.Core.Domain;
using System;
using Microsoft.Extensions.Logging;
using RitchieBoardGames.Restaurant.Core.Exceptions;
using Microsoft.AspNetCore.Authorization;
using RitchieBoardGames.Restaurant.Api.Infrastructure.Services;
using RitchieBoardGames.Restaurant.Core.Infrastructure.Services;

namespace RitchieBoardGames.Restaurant.Api.Controllers
{
    /// <summary>
    /// Организаторы
    /// </summary>
    [ApiController]
//  [Authorize]
    [Route("api/persons")]
    public class PersonController
        : ControllerBase
    {
        private readonly IPersonRepository _personRepository;
        private readonly IIdentityService _identityService;
        private readonly ILogger<PersonController> _logger;

        public PersonController(IPersonRepository personRepository, 
                                IIdentityService identityService, 
                                ILogger<PersonController> logger)
        {
            _personRepository = personRepository;
            _identityService = identityService;
            _logger = logger;
        }
        
        /// <summary>
        /// Получить всех пользователей организовавших мероприятия в ресторанах
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PersonResponse>> GetPersonsAsync()
        {
            var Persons = await _personRepository.GetAllAsync();

            var PersonsModelList = Persons.Select(x => 
                new PersonResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();

            return PersonsModelList;
        }

        private void AddLog(string message) 
        {
            if (string.IsNullOrEmpty(message))
                return;
            if (_logger == null)
                return;

            _logger.LogInformation(message);

        }

    }
}