﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RitchieBoardGames.Restaurant.Api.Mappers;
using RitchieBoardGames.Restaurant.Api.Models;
using RitchieBoardGames.Restaurant.Api.Validators;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;
using RitchieBoardGames.Restaurant.Core.Domain;
using RitchieBoardGames.Restaurant.Api.Infrastructure.Services;
using RitchieBoardGames.Restaurant.Core.Infrastructure.Services;
using RitchieBoardGames.Restaurant.Core.Abstractions.Infrastructure.Services;

namespace RitchieBoardGames.Restaurant.Api.Controllers
{
    /// <summary>
    /// Рестораны
    /// </summary>
    [ApiController]
    [Authorize]
    [Route("api/restaurants")]
    public class RestaurantController
        : ControllerBase
    {
        private readonly IRestaurantRepository _restaurantRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IIdentityService _identityService;
        private readonly IPersonService _personService;
        private readonly ILogger<RestaurantController> _logger;

        public RestaurantController(IRestaurantRepository RestaurantRepository,
                                    IPersonRepository PersonRepository,
                                    IIdentityService identityService,
                                    IPersonService personService,
                                    ILogger<RestaurantController> logger)
        {
            _restaurantRepository = RestaurantRepository;
            _personRepository = PersonRepository;
            _identityService = identityService;
            _personService = personService;
            _logger = logger;
        }
        
        /// <summary>
        /// Получить список всех ресторанов
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<List<RestaurantShortResponse>> GetRestaurantsAsync()
        {
            var restaurants = await _restaurantRepository.GetAllAsync();

            var restaurantsModelList = restaurants.Select(x => 
                new RestaurantShortResponse()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Address = x.Address,
                        Coordinates = x.Coordinates,
                    }).ToList();

            return restaurantsModelList;
        }

        /// <summary>
        /// Получить список всех ресторанов по городу
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("by-city")]
        public async Task<List<RestaurantShortResponse>> GetRestaurantsByCityAsync([FromQuery(Name = "city")] string city)
        {
            if (String.IsNullOrEmpty(city)) 
            {
                // remove incorrect symbols
                city = city.Trim().Replace("'", "").Replace("\"", "").Replace(">", "").Replace("<", "").Replace(",", "");
            }

            // if city is not set, then return full list, else filter by city
            var restaurants = String.IsNullOrEmpty(city) ?
                await _restaurantRepository.GetAllAsync() :
                await _restaurantRepository.GetWhere(x => x.City.Equals(city));

            var restaurantsModelList = restaurants.Select(x =>
                new RestaurantShortResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Address = x.Address,
                    Coordinates = x.Coordinates,
                }).ToList();

            return restaurantsModelList;
        }

        /// <summary>
        /// Получить данные ресторана по id
        /// </summary>
        /// <param name="id">Id ресторана</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("{id:Guid}")]
        public async Task<ActionResult<RestaurantResponse>> GetRestaurantByIdAsync(Guid id)
        {
            var restaurant = await _restaurantRepository.GetByIdAsync(id);

            if (restaurant == null)
                return NotFound();

            if (restaurant.PersonIds != null && restaurant.PersonIds.Count > 0)
            {
                var persons = await _personRepository.GetRangeByIdsAsync(restaurant.PersonIds);
                if (persons != null)
                    restaurant.Persons = persons.ToList();
                else
                    restaurant.Persons = new List<Person>();
            }
            
            var restaurantModel = new RestaurantResponse()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                Description = restaurant.Description,
                City = restaurant.City,
                Address = restaurant.Address,
                Phone = restaurant.Phone,
                WorkingHours = restaurant.WorkingHours,
                ImageUri = restaurant.ImageUri,
                Persons = restaurant.Persons
            };

            return restaurantModel;
        }

        /// <summary>
        /// Создать новый ресторан
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<RestaurantResponse>> CreateRestaurantAsync(CreateOrEditRestaurantRequest request)
        {
            var validator = new RestaurantValidator(_restaurantRepository);
            var validationResult = await validator.ValidateAsync(request);

            if (validationResult != null && validationResult.Errors.Count > 0)
                throw new Core.Exceptions.ValidationException(validationResult);
            
            Person person = await GetPersonForUser();
            Core.Domain.Restaurant restaurant = 
                RestaurantMapper.MapFromModel(request, person);

            await _restaurantRepository.AddAsync(restaurant);

            return CreatedAtAction(nameof(GetRestaurantByIdAsync), new { id = restaurant.Id }, restaurant.Id);
        }


        /// <summary>
        /// Обновить данные рестарана 
        /// </summary>
        /// <param name="id">Id ресторана</param>
        /// <param name="request">Данные запроса></param>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateRestaurantAsync(Guid id, CreateOrEditRestaurantRequest request)
        {
            var restaurant = await _restaurantRepository.GetByIdAsync(id);

            if (restaurant == null)
                return NotFound();

            var validator = new RestaurantValidator(_restaurantRepository);
            var validationResult = await validator.ValidateAsync(request);

            if (validationResult != null && validationResult.Errors.Count > 0)
                throw new Core.Exceptions.ValidationException(validationResult);

            Person person = await GetPersonForUser();
            RestaurantMapper.MapFromModel(request, person, restaurant);

            await _restaurantRepository.UpdateAsync(restaurant);
            AddLog("Изменены данные ресторана id = " + id);

            return NoContent();
        }

        /// <summary>
        /// Удалить ресторан
        /// </summary>
        /// <param name="id">Id ресторан</param>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteRestaurantAsync(Guid id)
        {
            var restaurant = await _restaurantRepository.GetByIdAsync(id);

            if (restaurant == null)
                return NoContent(); 

            await _restaurantRepository.DeleteAsync(restaurant);

            return NoContent();
        }

        private void AddLog(string message)
        {
            if (string.IsNullOrEmpty(message))
                return;

            if (_logger == null)
                return;

            _logger.LogInformation(message);

        }
        private async Task<Person> GetPersonForUser()
        {
            Person person = null;

            var userName = _identityService.GetUserName();

            // find person by userid from current db
            person = await _personRepository.GetFirstWhere(x => x.Name == userName);
            if (person == null)
            {
                person = new Person();
                person.Name = userName;
                person.Id = Guid.NewGuid();

                if(_personService != null)
                    person.ProfileId = await _personService.GetPersonId(userName);

                await _personRepository.AddAsync(person);
            }

            return person;
        }


    }
}