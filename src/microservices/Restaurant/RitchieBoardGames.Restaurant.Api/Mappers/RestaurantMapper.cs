﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RitchieBoardGames.Restaurant.Core.Domain;
using RitchieBoardGames.Restaurant.Api.Models;

namespace RitchieBoardGames.Restaurant.Api.Mappers
{
    public class RestaurantMapper
    {

        public static Core.Domain.Restaurant MapFromModel(CreateOrEditRestaurantRequest model, Person person, Core.Domain.Restaurant Restaurant = null)
        {
            if (Restaurant == null)
                Restaurant = new Core.Domain.Restaurant();
            
            Restaurant.Name = model.Name;
            Restaurant.Description = model.Description;
            Restaurant.City = model.City;
            Restaurant.Address = model.Address;
            Restaurant.Coordinates = model.Coordinates;
            Restaurant.Phone = model.Phone;
            Restaurant.WorkingHours = model.WorkingHours;
            Restaurant.ImageUri = model.ImageUri;
            Restaurant.MaxGamesAtTheSameTime = model.MaxGamesAtTheSameTime;
            Restaurant.IsAnyCanCreateEvents = model.IsAnyCanCreateEvents;

            if (person != null)
            {
                if (Restaurant.PersonIds == null)
                    Restaurant.PersonIds = new List<Guid>();

                if (!Restaurant.PersonIds.Contains(person.Id))
                    Restaurant.PersonIds.Add(person.Id);
            }

            return Restaurant;
        }
    }
}
