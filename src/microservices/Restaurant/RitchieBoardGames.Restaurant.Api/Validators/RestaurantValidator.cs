using FluentValidation;
using RitchieBoardGames.Restaurant.Api.Models;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;

namespace RitchieBoardGames.Restaurant.Api.Validators
{

    public class RestaurantValidator : AbstractValidator<CreateOrEditRestaurantRequest>
    {
        private readonly IRestaurantRepository _restaurantRepository;

        public RestaurantValidator(IRestaurantRepository restaurantRepository)
        {
            _restaurantRepository = restaurantRepository;

            RuleFor(p => p.Name).NotNull().NotEmpty().WithMessage("Название не указано.");
            RuleFor(p => p.Name).MaximumLength(100).WithMessage("Название слишком длинное.");

            RuleFor(p => p.City).NotNull().NotEmpty().WithMessage("Город не указан.");
            RuleFor(p => p.City).MaximumLength(100).WithMessage("Название города слишком длинное.");

            RuleFor(p => p.Address).NotNull().NotEmpty().WithMessage("Адрес не указан.");
            RuleFor(p => p.Address).MaximumLength(500).WithMessage("Адрес слишком длинный.");

            RuleFor(p => p.Coordinates).NotNull().NotEmpty().WithMessage("Координаты местоположения не указаны.");
            RuleFor(p => p.Coordinates).MaximumLength(25).WithMessage("Формат координат местоположения не верен.");

            RuleFor(p => p.Description).MaximumLength(3000).WithMessage("Описание слишком длинное.");
            
            RuleFor(p => p.Phone).MaximumLength(50).WithMessage("Неверный формат телефона.");

            RuleFor(p => p.ImageUri).MaximumLength(1000).WithMessage("Неверный формат ссылки на изображение.");

            RuleFor(p => p.WorkingHours).MaximumLength(500).WithMessage("Неверный формат описания времени работы.");
        }
    }
}