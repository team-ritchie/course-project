using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;
using RitchieBoardGames.Restaurant.Persistence.Data;
using RitchieBoardGames.Restaurant.Persistence.Repositories;
using RitchieBoardGames.Restaurant.Core.Domain;
using RitchieBoardGames.Restaurant.Api.Middleware;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using MongoDB.Driver;
using System.IdentityModel.Tokens.Jwt;
using RitchieBoardGames.Restaurant.Api.Infrastructure.Services;
using System;
using RitchieBoardGames.Restaurant.Core.Infrastructure.Services;
using RitchieBoardGames.Restaurant.Core.Abstractions.Infrastructure.Services;

namespace RitchieBoardGames.Restaurant.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x=> 
                x.SuppressAsyncSuffixInActionNames = false);

            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped<IIdentityService, IdentityService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            InitMongoDb(services);

            services.AddTransient<IRestaurantRepository, RestaurantRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();

            services.AddHttpClient<IPersonService, PersonService>(c =>
            {
                c.BaseAddress = new Uri(Configuration["IntegrationSettings:PersonApiUrl"]);
            });


            AddCustomAuthentication(services, Configuration);
            services.AddCustomSwaggerGen();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStaticFiles();
                app.UseSwagger();
                app.UseSwaggerUI(setupAction =>
                {
                    setupAction.SwaggerEndpoint("RitchieBoardGamesSpecification/swagger.json", "Restaurant API");
                    setupAction.RoutePrefix = "swagger";

                    setupAction.InjectStylesheet("/assets/custom-ui.css");
                });
            }
            else
            {
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            dbInitializer.InitializeDb();
        }

        private void InitMongoDb(IServiceCollection services)
        {
            var mongoClient = new MongoClient(Configuration["RitchieBoardGamesRestaurantDb:ConnectionString"]);
            var database = mongoClient.GetDatabase(Configuration["RitchieBoardGamesRestaurantDb:Database"]);
            var restaurantCollection = database.GetCollection<Core.Domain.Restaurant>("Restaurant");
            var personCollection = database.GetCollection<Person>("Person");

            services.AddSingleton<IMongoClient>(_ => mongoClient);
            services.AddSingleton(serviceProvider => database);
            services.AddSingleton(serviceProvider => restaurantCollection);
            services.AddSingleton(serviceProvider => personCollection);

            services.AddScoped(serviceProvider =>
                                serviceProvider.GetRequiredService<IMongoClient>()
                .StartSession());

        }

        public void AddCustomAuthentication(IServiceCollection services, IConfiguration configuration)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("sub");

            var identityUrl = configuration.GetValue<string>("IntegrationSettings:IdentityUrl");

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = identityUrl;
                options.RequireHttpsMetadata = false;
                options.Audience = "restaurants";
            });
        }

    }
}