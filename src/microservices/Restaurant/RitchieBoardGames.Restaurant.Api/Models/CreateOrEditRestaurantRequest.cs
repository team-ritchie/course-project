﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RitchieBoardGames.Restaurant.Api.Models
{
    /// <example>
    /// {
    ///    "Name": "N1",
    ///    "Description": "Уютный ресторан с европейской кухней.",
    ///    "City": "Москва",
    ///    "Address": "Москва, ул. Арбат, д.1",
    ///    "Phone": "+7(945)666-55-44",
    ///    "WorkingHours": "Ежедневно: c 9:00 до 23:00 ",
    ///    "ImageUri": "", 
    ///    "PersonIds": [ "1" ]
    /// }
    /// </example>>
    public class CreateOrEditRestaurantRequest
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public string Coordinates { get; set; }
        
        public string City { get; set; }

        public string Phone { get; set; }

        public string WorkingHours { get; set; }

        public string ImageUri { get; set; }

        public int MaxGamesAtTheSameTime { get; set; } = 0;

        public bool IsAnyCanCreateEvents { get; set; } = true;


    }
}