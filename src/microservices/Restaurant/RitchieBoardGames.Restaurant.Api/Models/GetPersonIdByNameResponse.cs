﻿using System;

namespace RitchieBoardGames.Restaurant.Api.Models
{
    public class GetPersonIdByNameResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
