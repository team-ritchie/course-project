﻿using System;

namespace RitchieBoardGames.Restaurant.Api.Models
{
    public class PersonResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

    }
}