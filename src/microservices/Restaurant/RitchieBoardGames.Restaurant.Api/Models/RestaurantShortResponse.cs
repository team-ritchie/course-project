﻿using System;

namespace RitchieBoardGames.Restaurant.Api.Models
{
    public class RestaurantShortResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }

        public string Address { get; set; }

        public string Coordinates { get; set; }

        public int MaxGamesAtTheSameTime { get; set; } 

        public bool IsAnyCanCreateEvents { get; set; } 

    }
}