﻿using System;
using System.Collections.Generic;
using RitchieBoardGames.Restaurant.Core.Domain;

namespace RitchieBoardGames.Restaurant.Api.Models
{
    public class RestaurantResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Phone { get; set; }

        public string WorkingHours { get; set; }

        public string ImageUri { get; set; }

        public List<Person> Persons { get; set; }

        public int MaxGamesAtTheSameTime { get; set; }

        public bool IsAnyCanCreateEvents { get; set; }


    }
}