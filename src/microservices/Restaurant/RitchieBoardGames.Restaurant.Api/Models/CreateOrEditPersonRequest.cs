﻿namespace RitchieBoardGames.Restaurant.Api.Models
{
    /// <example>
    /// {
    ///    "Name": "Иван Петров"
    /// }
    /// </example>>
    public class CreateOrEditPersonRequest
    {
        public string Name { get; set; }
    }
}