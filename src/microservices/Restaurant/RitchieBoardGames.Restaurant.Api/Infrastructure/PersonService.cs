﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RitchieBoardGames.Restaurant.Api.Models;
using RitchieBoardGames.Restaurant.Core.Abstractions.Infrastructure.Services;

namespace RitchieBoardGames.Restaurant.Api.Infrastructure.Services
{
    public class PersonService
        : IPersonService
    {
        private readonly HttpClient _httpClient;

        public PersonService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<int> GetPersonId(string name) 
        {
            int personId = 0;
            try
            {
                CancellationTokenSource cancellation = new CancellationTokenSource(10000);
                var response = await _httpClient.GetAsync("api/persons/?name=" + name, cancellation.Token);
                response.EnsureSuccessStatusCode();
                var responseString = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<GetPersonIdByNameResponse>(responseString);
                if(result != null)
                    return result.Id;
            }
            catch { }

            return personId;
        }
    }
}