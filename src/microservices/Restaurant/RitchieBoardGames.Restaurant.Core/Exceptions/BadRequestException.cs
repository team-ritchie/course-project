using FluentValidation.Results;
using System;
using System.Collections.Generic;

namespace RitchieBoardGames.Restaurant.Core.Exceptions
{

    public class BadRequestException : ApplicationException
    {
        public BadRequestException(string message) : base(message)
        {
        }
    }
}