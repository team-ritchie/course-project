using FluentValidation.Results;
using System;
using System.Collections.Generic;

namespace RitchieBoardGames.Restaurant.Core.Exceptions
{

    public class NotFoundException : ApplicationException
    {
        public NotFoundException(string name, object key)
            : base($"{name} ({key}): не найдено")
        {
        }
    }
}
