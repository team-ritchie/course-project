﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using RitchieBoardGames.Restaurant.Core.Domain;

namespace RitchieBoardGames.Restaurant.Core.Abstractions.Repositories
{
    public interface IRestaurantRepository
    {
        Task<IEnumerable<Core.Domain.Restaurant>> GetAllAsync();
        
        Task<Core.Domain.Restaurant> GetByIdAsync(Guid id);
        
        Task<IEnumerable<Core.Domain.Restaurant>> GetRangeByIdsAsync(List<Guid> ids);
        
        Task<Core.Domain.Restaurant> GetFirstWhere(Expression<Func<Core.Domain.Restaurant, bool>> predicate);
        
        Task<IEnumerable<Core.Domain.Restaurant>> GetWhere(Expression<Func<Core.Domain.Restaurant, bool>> predicate);

        Task AddAsync(Core.Domain.Restaurant entity);

        Task UpdateAsync(Core.Domain.Restaurant entity);

        Task DeleteAsync(Core.Domain.Restaurant entity);
    }
}