﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using RitchieBoardGames.Restaurant.Core.Domain;

namespace RitchieBoardGames.Restaurant.Core.Abstractions.Repositories
{
    public interface IPersonRepository
    {
        Task<IEnumerable<Person>> GetAllAsync();
        
        Task<Person> GetByIdAsync(Guid id);
        
        Task<IEnumerable<Person>> GetRangeByIdsAsync(List<Guid> ids);
        
        Task<Person> GetFirstWhere(Expression<Func<Person, bool>> predicate);
        
        Task<IEnumerable<Person>> GetWhere(Expression<Func<Person, bool>> predicate);

        Task AddAsync(Person entity);

        Task UpdateAsync(Person entity);

        Task DeleteAsync(Person entity);
    }
}