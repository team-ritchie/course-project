﻿namespace RitchieBoardGames.Restaurant.Core.Infrastructure.Services
{
    public interface IIdentityService
    {
        string GetUserIdentity();

        string GetUserName();
    }
}
