﻿using System.Threading.Tasks;

namespace RitchieBoardGames.Restaurant.Core.Abstractions.Infrastructure.Services
{
    public interface IPersonService
    {
        Task<int> GetPersonId(string name);
    }
}