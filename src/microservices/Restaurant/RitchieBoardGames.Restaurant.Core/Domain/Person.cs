﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace RitchieBoardGames.Restaurant.Core.Domain
{
    public class Person
    {
        [BsonId]
        public Guid Id { get; set; }
 
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public int ProfileId { get; set; } = 0;
    }
}