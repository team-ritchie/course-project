﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RitchieBoardGames.Restaurant.Core.Domain
{
    public class Restaurant
    {
        [BsonId]
        public Guid Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(3000)]
        public string Description { get; set; }

        [Required]
        [StringLength(500)]
        public string Address { get; set; }

        [Required]
        [StringLength(25)]
        public string Coordinates { get; set; }
        
        [Required]
        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(500)]
        public string WorkingHours { get; set; }

        [StringLength(1000)]
        public string ImageUri { get; set; }

        public List<Guid> PersonIds { get; set; }

        public virtual List<Person> Persons { get; set; }

        public int MaxGamesAtTheSameTime { get; set; } = 0;

        public bool IsAnyCanCreateEvents { get; set; } = true;
    }
}