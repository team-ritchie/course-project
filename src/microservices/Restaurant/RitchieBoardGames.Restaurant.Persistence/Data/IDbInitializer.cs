﻿namespace RitchieBoardGames.Restaurant.Persistence.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}