﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using RitchieBoardGames.Restaurant.Core.Domain;

namespace RitchieBoardGames.Restaurant.Persistence.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _db;
        private IMongoCollection<Core.Domain.Restaurant> _restaurantCollection;
        private IMongoCollection<Core.Domain.Person> _personCollection;

        public EfDbInitializer(IMongoDatabase database)
        {
            _db = database;
            _restaurantCollection = _db.GetCollection<Core.Domain.Restaurant>("Restaurant");
            _personCollection = _db.GetCollection<Core.Domain.Person>("Person");
        }

        public void InitializeDb()
        {
            // если коллекции содержат данные, то выходим
            if (_restaurantCollection.Find(_ => true).Any<Core.Domain.Restaurant>() ||
                _personCollection.Find(_ => true).Any<Core.Domain.Person>())
                return;

            _restaurantCollection.InsertManyAsync(FakeDataFactory.Restaurans);
        }
    }
}