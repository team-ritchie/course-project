﻿using System;
using System.Collections.Generic;
using System.Linq;
using RitchieBoardGames.Restaurant.Core.Domain;

namespace RitchieBoardGames.Restaurant.Persistence.Data
{
    public static class FakeDataFactory
    {
        public static List<Person> Persons => new List<Person>()
        {
            new Person() { Id = Guid.Parse("1B277B62-364A-499D-B3B1-C065FF45447F"), Name = "Иван Петров" },
            new Person() { Id = Guid.Parse("850A8606-C879-4A1D-863A-69443DE7CE28"), Name = "Василий Сидоров" }
        };

        public static List<Core.Domain.Restaurant> Restaurans => new List<Core.Domain.Restaurant>()
        {
            new Core.Domain.Restaurant()
            {
                Name = "Слава",
                Description = "Ресторан Slava — это винный бар. Здесь вы можете провести время за общением в компании или " +
                              "просто по-настоящему расслабиться после рабочего дня," +
                              "в чём вам с удовольствием помогут отменные бармены." +
                              "А также предлагается блюда нескольких видов кухонь: русской, украинской," +
                              "азербайджанской, армянской, грузинской, европейской, японской, но," +
                              "наверное, самой популярной из них является европейская.",
                Address = "127015, г. Москва, ул. Вятская, д.27к7",
                Coordinates = "55.814325, 37.580061",
                City = "Москва",
                Phone = "+7(499)811-00-77",
                WorkingHours = "Пн-чт, вс: 12:00—24:00; пт-сб: 12:00—05:00",
                ImageUri = "",
                PersonIds = new List<Guid>(){ Guid.Parse("1B277B62-364A-499D-B3B1-C065FF45447F") }
            },
            new Core.Domain.Restaurant()
            {
                Name = "Бакинский бульвар",
                Description = "Летняя веранда, доставка еды, оплата картой, завтрак, "+"" +
                              "Предзаказ онлайн, караоке, Wi-Fi, еда навынос, кофе с собой, "+
                              "средний счёт 1000–1500 ₽, детская комната. Цены: выше среднего.",
                Address = "г. Москва, ул. Стартовая, д.12",
                Coordinates= "55.888609, 37.691803",
                City = "Москва",
                Phone = "+7(495)255-56-66",
                WorkingHours = "Ежедневно: с 9:00 до 1:00",
                ImageUri = "",
                PersonIds = new List<Guid>(){ Guid.Parse("1B277B62-364A-499D-B3B1-C065FF45447F") }
            },
            new Core.Domain.Restaurant()
            {
                Name = "Amsterdam Bar",
                Description = "Amsterdam Bar – это гастробар с европейской кухней, авторскими коктейлями и приятной атмосферой.",
                Address = "г. Москва, Большая Новодмитровская улица, 36с12, подъезд 1",
                Coordinates= "55.804561, 37.585922",
                City = "Москва",
                Phone = "+7(966)104-31-31",
                WorkingHours = "Ежедневно: с 12:00 до 23:00",
                ImageUri = "",
                PersonIds = new List<Guid>(){ Guid.Parse("1B277B62-364A-499D-B3B1-C065FF45447F") }
            },
            new Core.Domain.Restaurant()
            {
                Name = "Ресторан Генацвале",
                Description = "Вкус и атмосфера настоящей Грузии",
                Address = "г. Москва, проезд Дежнева, 13",
                Coordinates= "55.870785, 37.641035",
                City = "Москва",
                Phone = "+79262910306",
                WorkingHours = "Ежедневно: с 12:00 до 24:00",
                ImageUri = "",
                PersonIds = new List<Guid>(){ Guid.Parse("1B277B62-364A-499D-B3B1-C065FF45447F") }
            },
            new Core.Domain.Restaurant()
            {
                Name = "Тануки",
                Description = "Ресторан. Суши-бар.",
                Address = "Московская обл., г. Мытищи, ул. Мира, д.26",
                Coordinates= "55.915877, 37.727277",
                City = "Мытищи",
                Phone = "+7(499)649-22-23",
                WorkingHours = "Ежедневно: с 9:00 до 1:00",
                ImageUri = "",
                PersonIds = new List<Guid>(){ Guid.Parse("1B277B62-364A-499D-B3B1-C065FF45447F") }
            },
            new Core.Domain.Restaurant()
            {
                Name = "Ресторан Оджахури",
                Description = "Летняя веранда, доставка еды, оплата картой, завтрак, "+"" +
                              "Предзаказ онлайн, караоке, Wi-Fi, еда навынос, кофе с собой, "+
                              "средний счёт 1000–1500 ₽, детская комната. Цены: выше среднего.",
                Address = "Московская областьб г.Королёв, проспект Королёва, 6Г",
                Coordinates= "55.920869, 37.839483",
                City = "Королёв",
                Phone = "+7(495)544-24-73",
                WorkingHours = "Ежедневно: с 9:00 до 0:00",
                ImageUri = "",
                PersonIds = new List<Guid>(){ Guid.Parse("1B277B62-364A-499D-B3B1-C065FF45447F") }
            },
            new Core.Domain.Restaurant()
            {
                Name = "Ресторан Цукини",
                Description = "Ресторан «Цукини» - ресторан средиземноморской кухни в Королеве,"+
                             "созданный любителями путешествий по прибрежным регионам Италии, "+
                             "Франции и Испании. В меню присутствуют рецепты всемирно известных "+
                             "домашних блюд.",
                Address = "Московская обл., г. Королёв, Пионерская улица, 15к1",
                Coordinates= "55.914291, 37.802723",
                City = "Королёв",
                Phone = "+7 (904) 568-24-70",
                WorkingHours = "Ежедневно: с 9:00 до 0:00",
                ImageUri = "",
                PersonIds = new List<Guid>(){ Guid.Parse("1B277B62-364A-499D-B3B1-C065FF45447F") }
            },
        };
    }
}