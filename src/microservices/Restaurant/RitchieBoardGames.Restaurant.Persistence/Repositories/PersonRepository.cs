using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;
using RitchieBoardGames.Restaurant.Core.Domain;


namespace RitchieBoardGames.Restaurant.Persistence.Repositories
{
    public class PersonRepository
        : IPersonRepository
    {
        private readonly IMongoCollection<Person> _entityCollection;
        private string location = "en_US";

        public PersonRepository(IMongoCollection<Person> entityCollection)
        {
            _entityCollection = entityCollection; 
        }


        public async Task<IEnumerable<Person>> GetAllAsync()
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(_ => true,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Person> GetByIdAsync(Guid id)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(l => l.Id == id,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    }).FirstOrDefaultAsync(cancellation.Token);
        }

        public async Task<IEnumerable<Person>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var list = new List<Person>();
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            try
            {
                list = await _entityCollection.Find(l => ids.Contains(l.Id),
                        new FindOptions
                        {
                            Collation = new Collation(location)
                        })
                    .ToListAsync(cancellation.Token);
            }
            catch 
            {
            }

            return list;
        }

        public async Task<Person> GetFirstWhere(Expression<Func<Person, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            var result = await _entityCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .ToListAsync(cancellation.Token);

            return (result == null || result.Count == 0) ? null : result[0];
        }

        public async Task<IEnumerable<Person>> GetWhere(Expression<Func<Person, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task AddAsync(Person entity)
        {
            await _entityCollection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(Person entity)
        {
            var filter = Builders<Person>.Filter.Eq("Id", entity.Id);

            var updatestatement = Builders<Person>.Update.Set("Name", entity.Name);
        
            var result = await _entityCollection.UpdateOneAsync(filter, updatestatement);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to update Person " + entity.Name);
        }

        public async Task DeleteAsync(Person entity)
        {
            var result = await _entityCollection.DeleteOneAsync<Person>( l => l.Id == entity.Id);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to delete Person " + entity.Name);
        }

    }
}