using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.Restaurant.Core.Abstractions.Repositories;
using RitchieBoardGames.Restaurant.Core.Domain;


namespace RitchieBoardGames.Restaurant.Persistence.Repositories
{
    public class RestaurantRepository
        : IRestaurantRepository
    {
        private readonly IMongoCollection<Core.Domain.Restaurant> _entityCollection;
        private string location = "en_US";

        public RestaurantRepository(IMongoCollection<Core.Domain.Restaurant> entityCollection)
        {
            _entityCollection = entityCollection;
        }


        public async Task<IEnumerable<Core.Domain.Restaurant>> GetAllAsync()
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(_ => true,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Core.Domain.Restaurant> GetByIdAsync(Guid id)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(l => l.Id == id,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    }).FirstOrDefaultAsync(cancellation.Token);
        }

        public async Task<IEnumerable<Core.Domain.Restaurant>> GetRangeByIdsAsync(List<Guid> ids)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(l => ids.Contains(l.Id),
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Core.Domain.Restaurant> GetFirstWhere(Expression<Func<Core.Domain.Restaurant, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            var result = await _entityCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .ToListAsync(cancellation.Token);

            return (result == null || result.Count == 0) ? null : result[0];
        }

        public async Task<IEnumerable<Core.Domain.Restaurant>> GetWhere(Expression<Func<Core.Domain.Restaurant, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task AddAsync(Core.Domain.Restaurant entity)
        {
            entity.Id = Guid.NewGuid();
            await _entityCollection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(Core.Domain.Restaurant entity)
        {
            var filter = Builders<Core.Domain.Restaurant>.Filter.Eq("Id", entity.Id);

            var updatestatement = Builders<Core.Domain.Restaurant>.Update.Set("Name", entity.Name);
            updatestatement = updatestatement.Set("Description", entity.Description);
            updatestatement = updatestatement.Set("Address", entity.Address);
            updatestatement = updatestatement.Set("Coordinates", entity.Coordinates);
            updatestatement = updatestatement.Set("City", entity.City);
            updatestatement = updatestatement.Set("Phone", entity.Phone);
            updatestatement = updatestatement.Set("WorkingHours", entity.WorkingHours);
            updatestatement = updatestatement.Set("ImageUri", entity.ImageUri);
            updatestatement = updatestatement.Set("PersonIds", entity.PersonIds);

            var result = await _entityCollection.UpdateOneAsync(filter, updatestatement);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to update Resaurant " + entity.Name);
        }

        public async Task DeleteAsync(Core.Domain.Restaurant entity)
        {
            var result = await _entityCollection.DeleteOneAsync<Core.Domain.Restaurant>( l => l.Id == entity.Id);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to delete Restaurant " + entity.Name);
        }

    }
}