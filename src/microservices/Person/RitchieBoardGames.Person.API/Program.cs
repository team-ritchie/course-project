using Serilog;
using System.Text;

// Logger
Log.Logger = CreateLogger()!;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog();

// Add services to the container.
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

try
{
    Log.Information("Запуск микросервиса Person");
    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Микросервис Person неожиданно завершил свою работу");
    return;
}
finally
{
    Log.CloseAndFlush();
}

static Serilog.Core.Logger? CreateLogger()
{
    var name = typeof(Program).Assembly.GetName().Name;

    return new LoggerConfiguration()
       .MinimumLevel.Override("Microsoft", Serilog.Events.LogEventLevel.Information)
       .Enrich.FromLogContext()
       .Enrich.WithMachineName()
       .Enrich.WithProperty("Assembly", name ?? "(Name of Person Assembly)")
       .WriteTo.Seq(serverUrl: "http://seq_in_dc:5341")
       .WriteTo.Console()
       .CreateLogger();
}
