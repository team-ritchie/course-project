namespace RitchieBoardGames.Person.Core;

public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Phone { get; set; }
    public DateTime Birthday { get; set; }
    public string? ImageUri { get; set; }
    public string Email { get; set; }

    public Person(int id, string name, string phone, DateTime birthday, string? imageUri, string email)
    {
        Name = name;
        Phone = phone;
        Birthday = birthday;
        ImageUri = imageUri;
        Email = email;
    }
}
