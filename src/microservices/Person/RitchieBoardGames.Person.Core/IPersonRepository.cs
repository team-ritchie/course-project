namespace RitchieBoardGames.Person.Core;

public interface IPersonRepository
{
    public Task<Person> GetFullInfoAsync(long id);

}
