using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Persistence.DbContexts;

namespace RitchieBoardGames.Api.IntegrationTests.Base
{
    internal class Utilities
    {
        internal static void InitializeDbForTests(BoardGamesDbContext context)
        {
            context.Tags.Add(new Tag
            {
                Id = 1,
                Name = "Логическая"
            });
            context.Tags.Add(new Tag
            {
                Id = 2,
                Name = "Семейная"
            });
            context.Tags.Add(new Tag
            {
                Id = 3,
                Name = "Музыкальная"
            });

            context.SaveChanges();
        }
    }
}
