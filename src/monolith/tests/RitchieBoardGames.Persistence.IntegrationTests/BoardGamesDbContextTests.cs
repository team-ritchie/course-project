using Microsoft.EntityFrameworkCore;
using Moq;
using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Persistence.DbContexts;
using Shouldly;

namespace RitchieBoardGames.Persistence.IntegrationTests;

public class BoardGamesDbContextTests
{
    private readonly BoardGamesDbContext _boardGamesDbContext;
    private readonly Mock<ILoggedInUserService> _loggedInUserServiceMock;
    private readonly string _loggedInUserId;

    public BoardGamesDbContextTests()
    {
        var dbContextOptions = new DbContextOptionsBuilder<BoardGamesDbContext>().UseInMemoryDatabase("TestDb").Options;

        _loggedInUserId = 1.ToString();
        _loggedInUserServiceMock = new Mock<ILoggedInUserService>();
        _loggedInUserServiceMock.Setup(x => x.UserId).Returns(_loggedInUserId);

        _boardGamesDbContext = new BoardGamesDbContext(dbContextOptions, _loggedInUserServiceMock.Object);
    }

    [Fact]
    public async void Save_SetCreatedByProperty()
    {
        var tag = new Tag { Id = 42, Name = "Test tag" };

        _boardGamesDbContext.Tags.Add(tag);
        await _boardGamesDbContext.SaveChangesAsync();

        tag.CreatedBy.ShouldBe(_loggedInUserId);
    }
}
