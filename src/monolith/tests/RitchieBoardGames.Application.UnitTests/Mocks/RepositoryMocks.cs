using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsByFilters;

namespace RitchieBoardGames.Application.UnitTests.Mocks;

internal class RepositoryMocks
{
    public static Mock<ITagRepository> GetTagRepository()
    {
        var tags = new List<Tag>
        {
            new Tag
            {
                Id = 1,
                Name = "Семья"
            },
            new Tag
            {
                Id = 2,
                Name = "Логика"
            },
            new Tag
            {
                Id = 3,
                Name = "Стратегия"
            },
            new Tag
            {
                Id = 4,
                Name = "Эрудиция"
            }
        };

        var mockTagRepository = new Mock<ITagRepository>();

        mockTagRepository.Setup(r => r.ListAllAsync()).ReturnsAsync(tags);

        mockTagRepository.Setup(r => r.AddAsync(It.IsAny<Tag>())).ReturnsAsync(
            (Tag tag) =>
            {
                tags.Add(tag);
                return tag;
            });
        return mockTagRepository;
    }
    public static Mock<ICityRepository> GetCityRepository()
    {
        var cities = new List<City>
        {
            new City
            {
                Id = 1,
                Name = "Москва",
                CreatedBy = "",
                CreatedDate = DateTime.Now,
            }
        };

        var mockCityRepository = new Mock<ICityRepository>();

        mockCityRepository.Setup(r => r.ListAllAsync()).ReturnsAsync(cities);
#pragma warning disable CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.
        mockCityRepository.Setup(r => r.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(
            (int id) =>
            {
                var result = cities.FirstOrDefault<City>(x => x.Id == id);
                if (result == null)
                    return null;
                return result;
            });
#pragma warning restore CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.

        mockCityRepository.Setup(r => r.AddAsync(It.IsAny<City>())).ReturnsAsync(
            (City city) =>
            {
                cities.Add(city);
                return city;
            });
        mockCityRepository.Setup(r => r.DeleteAsync(It.IsAny<City>())).Callback(
            (City city) =>
            {
                cities.Remove(city);
            });
        mockCityRepository.Setup(r => r.IsNameUnique(It.IsAny<string>())).ReturnsAsync(
            (string name) =>
            {
                return cities.Any<City>(x => x.Name == name);
            });
        return mockCityRepository;
    }
    public static Mock<IBoardGameRepository> GetBoardGameRepository()
    {
        var boardGames = new List<BoardGame>
        {
            new BoardGame
            {
                Id = 1,
                Name = "BoardGame1",
                CreatedBy = "",
                CreatedDate = DateTime.Now,
                Description = "BoardGame1 for test",
                ImageUri = "game5.ru",
                MaxAge = 100,
                MinAge = 0,
                MaxDuration = 3,
                MinDuration = 1,
                MaxPersons = 10,
                MinPersons = 2,
            }
        };

        var mockBoardGameRepository = new Mock<IBoardGameRepository>();

        mockBoardGameRepository.Setup(r => r.ListAllAsync()).ReturnsAsync(boardGames);
#pragma warning disable CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.
        mockBoardGameRepository.Setup(r => r.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(
            (int id) =>
            {
                var result = boardGames.FirstOrDefault<BoardGame>(x => x.Id == id);
                if (result == null)
                    result = null;
                return result;
            });
#pragma warning restore CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.

        mockBoardGameRepository.Setup(r => r.AddAsync(It.IsAny<BoardGame>())).ReturnsAsync(
            (BoardGame boardGame) =>
            {
                boardGames.Add(boardGame);
                return boardGame;
            });


        return mockBoardGameRepository;
    }
    public static Mock<IRestaurantRepository> GetRestaurantRepository()
    {
        var restaurants = new List<Restaurant>
        {
            new Restaurant
            {
                Id = 1,
                Name = "Restaurant5",
                ClosingHour = 12,
                CreatedBy = "",
                CreatedDate = DateTime.Now,
                AddressId = 1,
                Description = "Restaurant5 for test",
                ImageUri = "rest5.ru",
                OpenHour = 0,
                Phone = "+77777777777"

            }
        };

        var mockRestaurantRepository = new Mock<IRestaurantRepository>();

        mockRestaurantRepository.Setup(r => r.ListAllAsync()).ReturnsAsync(restaurants);
#pragma warning disable CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.
        mockRestaurantRepository.Setup(r => r.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(
            (int id) =>
            {
                var result = restaurants.FirstOrDefault<Restaurant>(x => x.Id == id);
                if (result == null)
                    return null;
                return result;
            });
#pragma warning restore CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.


        mockRestaurantRepository.Setup(r => r.AddAsync(It.IsAny<Restaurant>())).ReturnsAsync(
            (Restaurant restaurant) =>
            {
                restaurants.Add(restaurant);
                return restaurant;
            });
        return mockRestaurantRepository;
    }
    public static Mock<IGameEventRepository> GetGameEventRepository()
    {
        var gameEvents = new List<GameEvent>
        {
            new GameEvent
            {
                Id = 1,
                Name = "Test1",
                RestaurantId = 1,
                TimeStart = DateTime.Now,
                BoardGameId = 1,
                CreatedBy = "",
                CreatedDate = DateTime.Now,
                MaxPlayersCount = 5,
                MaxAge = 100,
                MinAge = 10,
                MinPlayersCount = 2,
                Note = "for test"
            },
            new GameEvent
            {
                Id = 2,
                Name = "Test2",
                RestaurantId = 1,
                TimeStart = DateTime.Now,
                BoardGameId = 1,
                CreatedBy = "",
                CreatedDate = DateTime.Now,
                MaxPlayersCount = 5,
                MaxAge = 100,
                MinAge = 10,
                MinPlayersCount = 2,
                Note = "for test"
            },
            new GameEvent
            {
                Id = 3,
                Name = "Test3",
                RestaurantId = 1,
                TimeStart = DateTime.Now,
                BoardGameId = 1,
                CreatedBy = "",
                CreatedDate = DateTime.Now,
                MaxPlayersCount = 5,
                MaxAge = 100,
                MinAge = 10,
                MinPlayersCount = 2,
                Note = "for test"
            },
            new GameEvent
            {
                Id = 4,
                Name = "Test4",
                RestaurantId = 1,
                TimeStart = DateTime.Now,
                BoardGameId = 2,
                CreatedBy = "",
                CreatedDate = DateTime.Now,
                MaxPlayersCount = 5,
                MaxAge = 100,
                MinAge = 10,
                MinPlayersCount = 2,
                Note = "for test"
            }
        };

        var mockGameEventRepository = new Mock<IGameEventRepository>();

        mockGameEventRepository.Setup(r => r.ListAllAsync()).ReturnsAsync(gameEvents);
#pragma warning disable CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.
        mockGameEventRepository.Setup(r => r.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(
            (int id) => 
            {
                var result = gameEvents.FirstOrDefault<GameEvent>(x => x.Id == id);
                if (result == null)
                    return null;
                return result;
            } 
            );
#pragma warning restore CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.

        mockGameEventRepository.Setup(r => r.AddAsync(It.IsAny<GameEvent>())).ReturnsAsync(
            (GameEvent gameEvent) =>
            {
                var lastEventId = gameEvents.OrderByDescending(x => x.Id).First<GameEvent>().Id;
                gameEvent.Id = lastEventId++;
                gameEvents.Add(gameEvent);
                return gameEvent;
            });
        
        mockGameEventRepository.Setup(r => r.DeleteAsync(It.IsAny<GameEvent>())).Callback(
            (GameEvent gameEvent) =>
            {
                gameEvents.Remove(gameEvent);
            });
        
        mockGameEventRepository.Setup(r => r.IsGameEventNameUnique(It.IsAny<int>(), It.IsAny<string>())).ReturnsAsync(
            (int id, string gameEventName) =>
            {
                return gameEvents.Any<GameEvent>(x => x.Name == gameEventName && x.Id != id);
            });
 
        mockGameEventRepository.Setup(r => r.GetGameEventsByFilters(It.IsAny<GetGameEventsByFiltersQuery>())).ReturnsAsync(
         (GetGameEventsByFiltersQuery gameEventName) =>
         {
             return gameEvents;
         });

        return mockGameEventRepository;
    }

    private static object TaskStatus()
    {
        throw new NotImplementedException();
    }
}
