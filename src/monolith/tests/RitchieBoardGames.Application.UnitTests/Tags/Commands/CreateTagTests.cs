using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Application.Features.Tags.Commands.CreateTag;

namespace RitchieBoardGames.Application.UnitTests.Tags.Commands;

public class CreateTagTests
{
    private readonly IMapper _mapper;
    private readonly Mock<ITagRepository> _mockTagRepository;
    public CreateTagTests()
    {
        _mockTagRepository = RepositoryMocks.GetTagRepository();
        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Fact]
    public async Task Handle_ValidTag_AddedToTagsRepo()
    {
        var handler = new CreateTagCommandHandler(_mapper, _mockTagRepository.Object, new Mock<IEmailService>().Object);

        await handler.Handle(new CreateTagCommand() { Name = "Test" }, CancellationToken.None);

        var allTags = await _mockTagRepository.Object.ListAllAsync();
        allTags.Count.ShouldBe(5);
    }
}
