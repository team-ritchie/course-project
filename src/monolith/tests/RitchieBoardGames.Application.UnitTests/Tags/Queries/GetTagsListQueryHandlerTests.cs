using RitchieBoardGames.Application.Features.Tags.Queries.GetTagsList;

namespace RitchieBoardGames.Application.UnitTests.Tags.Queries;

public class GetTagsListQueryHandlerTests
{
    private readonly IMapper _mapper;
    private readonly Mock<ITagRepository> _mockTagRepository;
    public GetTagsListQueryHandlerTests()
    {
        _mockTagRepository = RepositoryMocks.GetTagRepository();
        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Fact]
    public async Task GetTagsListTest()
    {
        var handler = new GetTagsListQueryHandler(_mapper, _mockTagRepository.Object);

        var result = await handler.Handle(new GetTagsListQuery(), CancellationToken.None);

        result.ShouldBeOfType<List<TagListVm>>();

        result.Count.ShouldBe(4);
    }
}
