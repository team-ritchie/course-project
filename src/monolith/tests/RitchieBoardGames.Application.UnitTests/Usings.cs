global using Xunit;
global using AutoMapper;
global using Moq;
global using RitchieBoardGames.Application.Contracts.Persistence;
global using RitchieBoardGames.Application.Profiles;
global using RitchieBoardGames.Application.UnitTests.Mocks;
global using RitchieBoardGames.Domain.Entities;
global using Shouldly;
