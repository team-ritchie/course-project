using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Application.Features.Cities.Commands.AddCity;
using System.ComponentModel.DataAnnotations;

namespace RitchieBoardGames.Application.UnitTests.Cities.Commands;

public class CreateCityTests
{
    private readonly IMapper _mapper;
    private readonly Mock<ICityRepository> _mockCityRepository;
    public CreateCityTests()
    {
        _mockCityRepository = RepositoryMocks.GetCityRepository();
 
        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Fact]
    public async Task Handler_ValidCityAddedToCityRepo()
    {
        var handler = new AddCityCommandHandler(_mockCityRepository.Object,
             _mapper);

        try
        {
            var allCity = await _mockCityRepository.Object.ListAllAsync();
            int count = allCity != null ? allCity.Count : 0;

            await handler.Handle(new AddCityCommand()
            {
                Name = "Mosc" + Guid.NewGuid(),
            }, CancellationToken.None);


            allCity = await _mockCityRepository.Object.ListAllAsync();
            allCity.Count.ShouldBe(count + 1);
        }
        catch (Exception e) { 
            Assert.True(false, e.Message);
        }
    }
    
    [Fact]
    public async Task Handler_ReturnErrorIfNameIsNotSet()
    {
        var handler = new AddCityCommandHandler(_mockCityRepository.Object, _mapper);

        try
        {
            await handler.Handle(new AddCityCommand()
            {
                Name = ""
            }, CancellationToken.None); ;

            Assert.True(false, "Name is not valiadated.");
        }
        catch (RitchieBoardGames.Application.Exceptions.ValidationException ev)
        {
            Assert.NotNull(ev.ValidationErrors);
            Assert.NotEmpty(ev.ValidationErrors);
            Assert.Contains<string>("Имя города не должно быть пустым", ev.ValidationErrors);
        }
        catch (Exception exp)
        {
            Assert.True(false, exp.Message);
        }
    }

    [Fact]
    public async Task Handler_ReturnErrorForNotUniqueName()
    {
        var handler = new AddCityCommandHandler(_mockCityRepository.Object, _mapper);

        var testCityName = "Test Unique Name" + Guid.NewGuid().ToString();
        try
        {
            await handler.Handle(new AddCityCommand()
            {
                Name = testCityName
            }, CancellationToken.None);

            await handler.Handle(new AddCityCommand()
            {
                Name = testCityName
            }, CancellationToken.None);

            Assert.True(false, "City with the same name was added to repository without error.");

        }
        catch (RitchieBoardGames.Application.Exceptions.ValidationException e) 
        {
            Assert.NotNull(e.ValidationErrors);
            Assert.NotEmpty(e.ValidationErrors);
            Assert.Equal("Название города должно быть уникальным", e.ValidationErrors[0]);
        }
        catch (Exception exp2)
        {
            Assert.True(false, exp2.Message);
        }
    }

}
