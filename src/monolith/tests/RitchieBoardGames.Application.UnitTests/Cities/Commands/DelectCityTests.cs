using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Application.Features.Cities.Commands.DeleteCity;

namespace RitchieBoardGames.Application.UnitTests.Cities.Commands;

public class DeleteCityTests
{
    private readonly IMapper _mapper;
    private readonly Mock<ICityRepository> _mockCityRepository;
    public DeleteCityTests()
    {
        _mockCityRepository = RepositoryMocks.GetCityRepository();

        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Theory]
    [InlineData(1)] // existing City
    [InlineData(5555)] // City does not exist
    public async Task Handle_DeleteCity_NoError(int id)
    {
        var handler = new DeleteCityCommandHandler(_mockCityRepository.Object);

        try
        {
            await handler.Handle(new DeleteCityCommand()
            {
                Id = id
            }, CancellationToken.None);

            var delGameEvent = await _mockCityRepository.Object.GetByIdAsync(id);
            Assert.Null(delGameEvent);
        }
        catch (Exception e) { 
            Assert.True(false, e.Message);
        }
    }
}
