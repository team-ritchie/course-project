using RitchieBoardGames.Application.Features.Cities.Queries.GetCitiesList;

namespace RitchieBoardGames.Application.UnitTests.Cities.Queries;

public class GetCitiesListQueryHandlerTests
{
    private readonly IMapper _mapper;
    private readonly Mock<ICityRepository> _mockCityRepository;
    public GetCitiesListQueryHandlerTests()
    {
        _mockCityRepository = RepositoryMocks.GetCityRepository();
        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Fact]
    public async Task GetCitesListTest()
    {
        var handler = new GetCitiesListQueryHandler(_mapper, _mockCityRepository.Object);
        var result = await handler.Handle(new GetCitiesListQuery(), CancellationToken.None);

        result.ShouldBeOfType<List<CityListVm>>();
        result.Count.ShouldBe(1);
    }
}
