using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Application.Features.GameEvents.Commands.UpdateGameEvent;

namespace RitchieBoardGames.Application.UnitTests.GameEvents.Commands;

public class UpdateGameEventTests
{
    private readonly IMapper _mapper;
    private readonly Mock<IGameEventRepository> _mockGameEventRepository;
    private readonly Mock<ICityRepository> _mockCityRepository;
    private readonly Mock<IRestaurantRepository> _mockRestaurantRepository;
    private readonly Mock<IBoardGameRepository> _mockBoardGameRepository;
    public UpdateGameEventTests()
    {
        _mockGameEventRepository = RepositoryMocks.GetGameEventRepository();
        _mockCityRepository = RepositoryMocks.GetCityRepository();
        _mockBoardGameRepository = RepositoryMocks.GetBoardGameRepository();
        _mockRestaurantRepository = RepositoryMocks.GetRestaurantRepository();

        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Fact]
    public async Task Handle_ValidGameEvent_UpdateToGameEventsRepo()
    {
        var handler = new UpdateGameEventCommandHandler(_mapper, _mockGameEventRepository.Object,
            _mockCityRepository.Object, _mockBoardGameRepository.Object,
            _mockRestaurantRepository.Object, new Mock<IEmailService>().Object);

        try
        {
            await handler.Handle(new UpdateGameEventCommand()
            {
                Id = 1,
                GameEvent = new GameEventForUpdateDto
                {
                    Name = "TestUpdateGE",
                    GameId = 1,
                    MaxAge = 50,
                    MaxPlayersCount = 50,
                    MinAge = 5,
                    MinPlayersCount = 10,
                    Note = "test create GameEvent",
                    RestaurantId = 1,
                    TimeStart = DateTime.Now
                }
            }, CancellationToken.None);


            var updGameEvent = await _mockGameEventRepository.Object.GetByIdAsync(1);
            Assert.NotNull(updGameEvent);
            Assert.Equal(1, updGameEvent.Id);
            Assert.Equal("TestUpdateGE", updGameEvent.Name);
        }
        catch (Exception e) { 
            Assert.True(false, e.Message);

        }
    }


    [Fact]
    public async Task Handler_ReturnErrorIfNameIsNotSet()
    {
        var handler = new UpdateGameEventCommandHandler(_mapper, _mockGameEventRepository.Object,
            _mockCityRepository.Object, _mockBoardGameRepository.Object,
            _mockRestaurantRepository.Object, new Mock<IEmailService>().Object);

        try
        {
            await handler.Handle(new UpdateGameEventCommand()
            {
                Id = 1,
                GameEvent = new GameEventForUpdateDto
                {
                    GameId = 1,
                    Note = "test update GameEvent",
                    RestaurantId = 1
                }
            }, CancellationToken.None);

            Assert.True(false, "Name is not valiadated.");
        }
        catch (RitchieBoardGames.Application.Exceptions.ValidationException ev)
        {
            Assert.NotNull(ev.ValidationErrors);
            Assert.NotEmpty(ev.ValidationErrors);
            Assert.Contains<string>("Название мероприятия обязательно", ev.ValidationErrors);
        }
        catch (Exception exp)
        {
            Assert.True(false, exp.Message);
        }
    }
    [Fact]
    public async Task Handler_ReturnErrorIfNameIsTooLong()
    {
        var handler = new UpdateGameEventCommandHandler(_mapper, _mockGameEventRepository.Object,
            _mockCityRepository.Object, _mockBoardGameRepository.Object,
            _mockRestaurantRepository.Object, new Mock<IEmailService>().Object);

        var updateGameEventCommand = new UpdateGameEventCommand()
        {
            Id = 2,
            GameEvent = new GameEventForUpdateDto
            {
                Name = "Too LongName ",
                GameId = 1,
                Note = "test update GameEvent",
                RestaurantId = 1
            }
        };

        for (int i = 1; i <= 500; i++)
        {
            updateGameEventCommand.GameEvent.Name += i.ToString();
        }

        try
        {
            await handler.Handle(updateGameEventCommand, CancellationToken.None);
            Assert.True(false, "Name is not valiadated.");
        }
        catch (RitchieBoardGames.Application.Exceptions.ValidationException ev)
        {
            Assert.NotNull(ev.ValidationErrors);
            Assert.NotEmpty(ev.ValidationErrors);
            Assert.Contains<string>("Название мероприятия слишком длинное, должно быть не более 500 символов.", ev.ValidationErrors);
        }
        catch (Exception exp)
        {
            Assert.True(false, exp.Message);
        }
    }
    [Fact]
    public async Task Handler_ReturnErrorForNotUniqueName()
    {
        var handler = new UpdateGameEventCommandHandler(_mapper, _mockGameEventRepository.Object,
            _mockCityRepository.Object, _mockBoardGameRepository.Object,
            _mockRestaurantRepository.Object, new Mock<IEmailService>().Object);

        try
        {
            await handler.Handle(new UpdateGameEventCommand()
            {
                Id = 3,
                GameEvent = new GameEventForUpdateDto
                {
                    Name = "Test Unique Name",
                    GameId = 1,
                    Note = "test create GameEvent",
                    RestaurantId = 1,
                }
            }, CancellationToken.None);

            await handler.Handle(new UpdateGameEventCommand()
            {
                Id = 4,
                GameEvent = new GameEventForUpdateDto
                {
                    Name = "Test Unique Name",
                    GameId = 1,
                    Note = "test update GameEvent",
                    RestaurantId = 1
                }
            }, CancellationToken.None);

            Assert.True(false, "GameEvent with the same name was added to repository without error.");

        }
        catch (RitchieBoardGames.Application.Exceptions.ValidationException e)
        {
            Assert.NotNull(e.ValidationErrors);
            Assert.NotEmpty(e.ValidationErrors);
            Assert.Equal("Название мероприятия должно быть уникально", e.ValidationErrors[0]);
        }
        catch (Exception exp2)
        {
            Assert.True(false, exp2.Message);
        }
    }
    [Fact]
    public async Task Handler_ReturnErrorIfMinAgeGraterMaxAge()
    {
        var handler = new UpdateGameEventCommandHandler(_mapper, _mockGameEventRepository.Object,
            _mockCityRepository.Object, _mockBoardGameRepository.Object,
            _mockRestaurantRepository.Object, new Mock<IEmailService>().Object);

        try
        {
            await handler.Handle(new UpdateGameEventCommand()
            {
                Id = 4,
                GameEvent = new GameEventForUpdateDto
                {
                    Name = "Test MinAge and MaxAge",
                    GameId = 1,
                    MaxAge = 00,
                    MinAge = 5,
                    Note = "test update GameEvent",
                    RestaurantId = 1
                }
            }, CancellationToken.None);

            Assert.True(false, "MinAge and MaxAge were not valiadated.");
        }
        catch (RitchieBoardGames.Application.Exceptions.ValidationException e)
        {
            Assert.NotNull(e.ValidationErrors);
            Assert.NotEmpty(e.ValidationErrors);
            Assert.Equal("Минимальный возраст должен быть меньше максимального", e.ValidationErrors[0]);
        }
        catch (Exception exp)
        {
            Assert.True(false, exp.Message);
        }
    }
    [Fact]
    public async Task Handler_ReturnErrorIfMinPlayersCountGraterMaxPlayersCount()
    {
        var handler = new UpdateGameEventCommandHandler(_mapper, _mockGameEventRepository.Object,
            _mockCityRepository.Object, _mockBoardGameRepository.Object,
            _mockRestaurantRepository.Object, new Mock<IEmailService>().Object);

        try
        {
            await handler.Handle(new UpdateGameEventCommand()
            {
                Id = 4,
                GameEvent = new GameEventForUpdateDto
                {
                    Name = "Test MinPlayersCount and MaxPlayersCount",
                    GameId = 1,
                    MaxPlayersCount = 0,
                    MinPlayersCount = 10,
                    Note = "test update GameEvent",
                    RestaurantId = 1
                }
            }, CancellationToken.None);

            Assert.True(false, "MinPlayersCount and MaxPlayersCount were not valiadated.");
        }
        catch (RitchieBoardGames.Application.Exceptions.ValidationException e)
        {
            Assert.NotNull(e.ValidationErrors);
            Assert.NotEmpty(e.ValidationErrors);
            Assert.Equal("Минимальное количество участников должно быть меньше максимального", e.ValidationErrors[0]);
        }
        catch (Exception exp)
        {
            Assert.True(false, exp.Message);
        }
    }
    [Theory]
    [InlineData(0)] // GameId is not set
    [InlineData(600000)] // BoardGame does not exist
    public async Task Handler_ReturnErrorIfGameIdIsNotValid(int gameId)
    {
        var handler = new UpdateGameEventCommandHandler(_mapper, _mockGameEventRepository.Object,
            _mockCityRepository.Object, _mockBoardGameRepository.Object,
            _mockRestaurantRepository.Object, new Mock<IEmailService>().Object);

        try
        {
            await handler.Handle(new UpdateGameEventCommand()
            {
                Id = 4,
                GameEvent = new GameEventForUpdateDto
                {
                    Name = "Test GameId = " + gameId + " " + Random.Shared.Next(),
                    GameId = gameId,
                    Note = "test update GameEvent",
                    RestaurantId = 1
                }
            }, CancellationToken.None);

            Assert.True(false, "GameId is not valiadated.");
        }
        catch (RitchieBoardGames.Application.Exceptions.ValidationException ev)
        {
            Assert.NotNull(ev.ValidationErrors);
            Assert.NotEmpty(ev.ValidationErrors);

            if (gameId == 0)
                Assert.Contains<string>("Игра не выбрана", ev.ValidationErrors);
            else
                Assert.True(false, ev.ValidationErrors[0]);
        }
        catch (ArgumentException e)
        {
            if (gameId == 600000)
                Assert.Equal("Игра не найдена.", e.Message);
            else
                Assert.True(false, e.Message);
        }
        catch (Exception exp)
        {
            Assert.True(false, exp.Message);
        }
    }
    [Theory]
    [InlineData(0)] // RestaurantId is not set
    [InlineData(600000)] // Restaurant does not exist
    public async Task Handler_ReturnErrorIfRestaurantIdIsNotValid(int restaurantId)
    {
        var handler = new UpdateGameEventCommandHandler(_mapper, _mockGameEventRepository.Object,
            _mockCityRepository.Object, _mockBoardGameRepository.Object,
            _mockRestaurantRepository.Object, new Mock<IEmailService>().Object);

        try
        {
            await handler.Handle(new UpdateGameEventCommand()
            {
                Id = 4,
                GameEvent = new GameEventForUpdateDto
                {
                    Name = "Test RestaurantId",
                    GameId = 1,
                    Note = "test update Restaurant",
                    RestaurantId = restaurantId
                }
            }, CancellationToken.None);

            Assert.True(false, "RestaurantId is not valiadated.");
        }
        catch (RitchieBoardGames.Application.Exceptions.ValidationException ev)
        {
            Assert.NotNull(ev.ValidationErrors);
            Assert.NotEmpty(ev.ValidationErrors);

            if (restaurantId == 0)
                Assert.Contains<string>("Место проведения мероприятия обязательно", ev.ValidationErrors);
            else
                Assert.True(false, ev.ValidationErrors[0]);
        }
        catch (ArgumentException e)
        {
            if (restaurantId == 600000)
                Assert.Equal("Ресторан не найден.", e.Message);
            else
                Assert.True(false, e.Message);
        }
        catch (Exception exp)
        {
            Assert.True(false, exp.Message);
        }
    }


}
