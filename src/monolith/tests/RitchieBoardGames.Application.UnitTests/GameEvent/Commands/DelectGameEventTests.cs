using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Application.Features.GameEvents.Commands.DeleteGameEvent;

namespace RitchieBoardGames.Application.UnitTests.GameEvents.Commands;

public class DeleteGameEventTests
{
    private readonly IMapper _mapper;
    private readonly Mock<IGameEventRepository> _mockGameEventRepository;
    public DeleteGameEventTests()
    {
        _mockGameEventRepository = RepositoryMocks.GetGameEventRepository();

        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Theory]
    [InlineData(4)] // existing GameEvent
    [InlineData(5555)] // GameEvent does not exist
    public async Task Handle_DeleteGameEvent_NoError(int id)
    {
        var handler = new DeleteGameEventCommandHandler(_mapper, _mockGameEventRepository.Object);

        try
        {
            await handler.Handle(new DeleteGameEventCommand()
            {
                Id = id
            }, CancellationToken.None);

            var delGameEvent = await _mockGameEventRepository.Object.GetByIdAsync(id);
            Assert.Null(delGameEvent);
        }
        catch (Exception e) { 
            Assert.True(false, e.Message);
        }
    }
}
