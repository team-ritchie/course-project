using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventDetail;

namespace RitchieBoardGames.Application.UnitTests.GameEvents.Queries;

public class GetGameEventDetailQueryHandlerTests
{
    private readonly IMapper _mapper;
    private readonly Mock<IGameEventRepository> _mockGameEventRepository;
    private readonly Mock<ICityRepository> _mockCityRepository;
    private readonly Mock<IBoardGameRepository> _mockBoardGameRepository;
    private readonly Mock<IRestaurantRepository> _mockRestaurantRepository;
    public GetGameEventDetailQueryHandlerTests()
    {
        _mockGameEventRepository = RepositoryMocks.GetGameEventRepository();
        _mockCityRepository = RepositoryMocks.GetCityRepository();
        _mockBoardGameRepository = RepositoryMocks.GetBoardGameRepository();
        _mockRestaurantRepository = RepositoryMocks.GetRestaurantRepository();

        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Fact]
    public async Task Handler_ReturnNullForUnexistingId()
    {
        var handler = new GetGameEventDetailQueryHandler(_mapper, _mockGameEventRepository.Object,
            _mockRestaurantRepository.Object, _mockBoardGameRepository.Object);

        try
        {
            var result = await handler.Handle(new GetGameEventDetailQuery()
            {
                Id = 500,
            }, CancellationToken.None);

            Assert.Null(result);
        }
        catch (Exception e) 
        {
            Assert.True(false, e.Message);
        }
    }
}
