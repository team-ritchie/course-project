using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsByFilters;

namespace RitchieBoardGames.Application.UnitTests.GameEvents.Queries;

public class GetGameEventsByFiltersQueryHandlerTests
{
    private readonly IMapper _mapper;
    private readonly Mock<IGameEventRepository> _mockGameEventRepository;
    private readonly Mock<ICityRepository> _mockCityRepository;
    private readonly Mock<IRestaurantRepository> _mockRestaurantRepository;
    private readonly Mock<IBoardGameRepository> _mockBoardGameRepository;

    public GetGameEventsByFiltersQueryHandlerTests()
    {
        _mockGameEventRepository = RepositoryMocks.GetGameEventRepository();
        _mockCityRepository = RepositoryMocks.GetCityRepository();
        _mockBoardGameRepository = RepositoryMocks.GetBoardGameRepository();
        _mockRestaurantRepository = RepositoryMocks.GetRestaurantRepository();

        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Theory]
//    [InlineData(1, 1, 1,"2022-07-07")] // all data are exist in system
//    [InlineData(null, null, null, null)] // filters are not set
    [InlineData(0, 0, 0, "")] // filters are not set
//    [InlineData(0, 1, 1,"2022-07-07")] // use unexisting cityId
//    [InlineData(null, 1, 1, "2022-07-07")] // cityId is not set
//    [InlineData(1, null, 1, "2022-07-07")] // restaurantId is not set
//    [InlineData(1, 1, null, "2022-07-07")] // gameId is not set
//    [InlineData(1, 1, 1, null)] // date is not set
//    [InlineData(1, null, null, null)] // only cityId is set
//    [InlineData(null, 1, null, null)] // only restaurantId is set
//    [InlineData(null, null, 1, null)] // only gameId is set
//    [InlineData(null, null, null, "2022-07-07")] // date is set
    public async Task GetGameEventsByFiltersTest(int? cityId, int? restaurantId, int? gameId, string? date)
    {
        var handler = new GetGameEventsByFiltersQueryHandler(_mapper, _mockGameEventRepository.Object,
            _mockCityRepository.Object, _mockBoardGameRepository.Object,
            _mockRestaurantRepository.Object);
        try
        {
          var result = await handler.Handle(new GetGameEventsByFiltersQuery() 
            {
                CityId = cityId,
                GameId = gameId,
                RestaurantId = restaurantId,
                Date = date,
            }, CancellationToken.None);

            result.ShouldBeOfType<List<GameEventsByFiltersVm>>();
            result.Count.ShouldBe(4);
        }
        catch (Exception e) 
        {
            Assert.True(false, e.Message);
        }
    }
}
