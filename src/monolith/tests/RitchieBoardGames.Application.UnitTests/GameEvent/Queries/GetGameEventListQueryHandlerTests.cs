using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsList;

namespace RitchieBoardGames.Application.UnitTests.GameEvents.Queries;

public class GetGameEventsListQueryHandlerTests
{
    private readonly IMapper _mapper;
    private readonly Mock<IGameEventRepository> _mockGameEventRepository;
    public GetGameEventsListQueryHandlerTests()
    {
        _mockGameEventRepository = RepositoryMocks.GetGameEventRepository();
        var configurationProvider = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });

        _mapper = configurationProvider.CreateMapper();
    }

    [Fact]
    public async Task GetGameEventsListTest()
    {
        var handler = new GetGameEventsListQueryHandler(_mapper, _mockGameEventRepository.Object);
        var result = await handler.Handle(new GetGameEventsListQuery(), CancellationToken.None);

        result.ShouldBeOfType<List<GameEventListVm>>();
        result.Count.ShouldBe(4);
    }
}
