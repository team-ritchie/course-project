using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RitchieBoardGames.Application.Contracts.Identity;
using RitchieBoardGames.Application.Models.Authentication;
using RitchieBoardGames.Identity.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace RitchieBoardGames.Identity.Services;

public class AuthenticationService : IAuthenticationService
{
    private readonly JwtSettings _jwtSettins;

    public AuthenticationService(IOptions<JwtSettings> jwtSettins)
    {
        _jwtSettins = jwtSettins.Value;
    }

    public async Task<AuthenticationResponse> AuthenticateAsync(AuthenticationRequest request)
    {
        var user = ValidateUserCredentials(request.Email, request.Password);

        JwtSecurityToken jwtSecurityToken = GenerateToken(user);

        AuthenticationResponse response = new AuthenticationResponse
        {
            Id = user.Id.ToString(),
            Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
            Email = "test@test.ru",
            Name = user.Name
        };

        return await Task.FromResult(response);
    }

    private JwtSecurityToken GenerateToken(ApplicationUser user)
    {
        var securityKey = 
            new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_jwtSettins.Key));
        var signingCredentials =
            new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        var claimsForToken = new List<Claim>();
        claimsForToken.Add(new Claim("sub", user.Id.ToString()));
        claimsForToken.Add(new Claim("city", user.City ?? ""));

        var jwtSecurityToken = new JwtSecurityToken(
            _jwtSettins.Issuer,
            _jwtSettins.Audience,
            claimsForToken,
            DateTime.UtcNow,
            DateTime.UtcNow.AddMinutes(_jwtSettins.DurationInMinutes),
            signingCredentials);

        return jwtSecurityToken;
    }

    private ApplicationUser ValidateUserCredentials(string? email, string? password)
    {
        // 1 шаг. Найти пользователя
        //var user = await _userManager.FindByEmailAsync(request.Email);

        // кинуть ошибку, если такого пользователя нет
        //if (user is null)
        //{
        //    throw new Exception($"Пользователь с email {request.Email} не найден.");
        //}


        // 2 шаг. Проверить валидность пароля
        //var result = await _signInManager.PasswordSignInAsync(user.UserName, request.Password, false, lockoutOnFailure: false);

        //if (!result.Succeeded)
        //{
        //    throw new Exception($"Учетные данные аккаунта {request.Email} невалидны.");
        //}


        // TODO: у нас пока нет таблицы с пользователями, там нужно проверить, и если есть, то
        // возвращаем реальный результат валидации, сейчас временная реализация и аутентификация
        // будет валидна всегда
        //
        // здесь я создаю токен для вымышленного пользователя Разработчик, чтобы пока
        // упростить код, в будущем здесь должен быть настоящий User

        // Мы здесь, если все предыдущие шаги выполнены успешно.
        return new ApplicationUser { Id = 1, Name = "Разработчик", City = "Москва" };
    }
}
