using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using RitchieBoardGames.Application.Contracts.Identity;
using RitchieBoardGames.Application.Models.Authentication;
using RitchieBoardGames.Identity.Services;
using System.Text;

namespace RitchieBoardGames.Identity;

public static class IdentityServiceExtensions
{
    public static void AddIdentityServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<JwtSettings>(configuration.GetSection("JwtSettings"));

        services.AddTransient<IAuthenticationService, AuthenticationService>();

        services.AddAuthentication("Bearer")
            .AddJwtBearer(options =>
            {

                options.TokenValidationParameters = new()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = configuration["JwtSettings:Issuer"],
                    ValidAudience = configuration["JwtSettings:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.ASCII.GetBytes(configuration["JwtSettings:Key"]))
                };

                // настроить как обрабатывать разные события при аутентификации
            });
    }
}
