namespace RitchieBoardGames.Identity.Models;

public class ApplicationUser
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string? City { get; set; }
}
