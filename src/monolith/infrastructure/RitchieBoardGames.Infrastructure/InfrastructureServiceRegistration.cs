using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Application.Models.Mail;
using RitchieBoardGames.Infrastructure.LoggedInUser;
using RitchieBoardGames.Infrastructure.Mail;

namespace RitchieBoardGames.Infrastructure;

public static class InfrastructureServiceRegistration
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

        services.AddScoped<ILoggedInUserService, LoggedInUserService>();

        services.Configure<EmailSettings>(configuration.GetSection("EmailSettings"));

        services.AddTransient<IEmailService, EmailService>();

        return services;
    }
}
