using Microsoft.AspNetCore.Http;
using RitchieBoardGames.Application.Contracts.Infrastructure;
using System.Security.Claims;

namespace RitchieBoardGames.Infrastructure.LoggedInUser;

public class LoggedInUserService : ILoggedInUserService
{
    public LoggedInUserService(IHttpContextAccessor httpContextAccessor)
    {
        UserId = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);
    }

    public string? UserId { get; }
}
