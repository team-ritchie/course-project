using RitchieBoardGames.Application.Models.Services.BoardGame;
using RitchieBoardGames.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RitchieBoardGames.Services.BoardGame
{
    public static class StaticCast
    {
        public static BoardGameListItem ToDal(this RitchieBoardGames.Domain.Entities.BoardGame e)
        {
            return new BoardGameListItem()
            {
                Description = e.Description,
                GameEventsCount = e.GameEvents.Count,
                Id = e.Id,
                ImageUri = e.ImageUri,
                MaxAge = e.MaxAge,
                MaxDuration = e.MaxDuration,
                MaxPersons = e.MaxPersons,
                MinAge = e.MinAge,
                MinDuration = e.MinDuration,
                MinPersons = e.MinPersons,
                Name = e.Name,
                Tags = e.Tags.Select(e => e.Name).ToList()

            };
        }
    }
}
