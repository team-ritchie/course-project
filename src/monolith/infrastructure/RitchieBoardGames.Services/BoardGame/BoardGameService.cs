using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Contracts.Services;
using RitchieBoardGames.Application.Models.Services.BoardGame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RitchieBoardGames.Services.BoardGame
{
    public class BoardGameService : IBoardGameService
    {
        private readonly IBoardGameRepository _boardGameRepository;

        public BoardGameService(IBoardGameRepository boardGameRepository)
        {
            _boardGameRepository = boardGameRepository;
        }
        public async Task<BoardGameGetListResponce> GetAll()
        {
            var items = await _boardGameRepository.ListAllAsync();
            var games = items.Select(x => x.ToDal()).ToList();
            var response = new BoardGameGetListResponce()
            {
                Games = games
            };
            return response;
        }
    }
}
