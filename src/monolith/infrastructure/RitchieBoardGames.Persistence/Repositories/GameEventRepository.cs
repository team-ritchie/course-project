using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsByFilters;
using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Persistence.DbContexts;

namespace RitchieBoardGames.Persistence.Repositories;

public class GameEventRepository : BaseRepository<GameEvent>, IGameEventRepository
{
    public GameEventRepository(BoardGamesDbContext dbContext) : base(dbContext)
    {
    }

    public async Task<List<GameEvent>> GetGameEventsByFilters(GetGameEventsByFiltersQuery filters)
    {

        if (filters.RestaurantId > 0)
        {
            return await _dbContext.Set<GameEvent>().Where(x =>
                ((filters.GameId == null || (x.BoardGame != null && x.BoardGame.Id == filters.GameId)) &&
                 (x.Restaurant.Id == filters.RestaurantId) &&
                 (filters.Date == null || x.TimeStart.ToShortDateString().Equals(filters.Date)))
                ).ToListAsync();
        }

        if (filters.CityId > 0)
        {
            var restIds = await _dbContext.Set<Restaurant>().Where(x => x.Address.CityId == filters.CityId).Select(x => x.Id).ToListAsync();
            if (restIds == null)
                return new List<GameEvent>();

            return await _dbContext.Set<GameEvent>().Where(x =>
                (filters.GameId == null || (x.BoardGame != null && x.BoardGame.Id == filters.GameId)) &&
                 (x.Restaurant != null && restIds.Contains(x.Restaurant.Id)) &&
                (filters.Date == null || x.TimeStart.ToShortDateString().Equals(filters.Date))
                ).ToListAsync();
        }


        return await _dbContext.Set<GameEvent>().Where(x =>
            (filters.GameId == null || (x.BoardGame != null && x.BoardGame.Id == filters.GameId)) &&
            (filters.Date == null || x.TimeStart.ToShortDateString().Equals(filters.Date))
        ).ToListAsync();

    }

    public Task<bool> IsGameEventNameUnique(int id, string gameEventName)
    {
        var matches = _dbContext.GameEvents.Any(e => e.Name.Equals(gameEventName) && e.Id != id);
        return Task.FromResult(matches);
    }

}
