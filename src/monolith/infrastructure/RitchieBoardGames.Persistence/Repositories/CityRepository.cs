using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Persistence.DbContexts;

namespace RitchieBoardGames.Persistence.Repositories;

public class CityRepository : BaseRepository<City>, ICityRepository
{
    public CityRepository(BoardGamesDbContext dbContext) : base(dbContext)
    {
    }

    public Task<bool> IsNameUnique(string name)
    {
        var matches = _dbContext.Cities.Any(e => e.Name.Equals(name));
        return Task.FromResult(matches);
    }
    public Task<City?> GetCityByName(string name)
    {
        var result = _dbContext.Set<City>().FirstOrDefault(x => x.Name.Equals(name, StringComparison.Ordinal));
        return Task.FromResult(result);
    }
}
