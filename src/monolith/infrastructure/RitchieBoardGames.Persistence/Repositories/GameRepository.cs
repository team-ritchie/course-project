using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Persistence.DbContexts;

namespace RitchieBoardGames.Persistence.Repositories;

public class BoardGameRepository : BaseRepository<BoardGame>, IBoardGameRepository
{
    public BoardGameRepository(BoardGamesDbContext dbContext) : base(dbContext)
    {
    }

    public Task<bool> IsNameExist(string name)
    {
        var matches = _dbContext.BoardGames.Any(e => e.Name.Equals(name));
        return Task.FromResult(matches);
    }

    public async Task<IReadOnlyList<BoardGame>> ListAllByTagIdAsync(int tagId)
    {

        var tag = await _dbContext.Set<Tag>().Include(t=>t.BoardGames).FirstOrDefaultAsync();
        if (await _dbContext.Set<Tag>().AnyAsync(t=>t.Id == tagId))
        {
            return await _dbContext.Set<BoardGame>().Where(bg => bg.Tags.Any(t=> t.Id == tagId)).ToListAsync();
        }
        else
        {
            return new List<BoardGame>();
        }
    }
}
