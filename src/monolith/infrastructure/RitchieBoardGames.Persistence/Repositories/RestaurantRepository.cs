using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Persistence.DbContexts;

namespace RitchieBoardGames.Persistence.Repositories;

public class RestaurantRepository : BaseRepository<Restaurant>, IRestaurantRepository
{
    public RestaurantRepository(BoardGamesDbContext dbContext) : base(dbContext)
    {
    }
}
