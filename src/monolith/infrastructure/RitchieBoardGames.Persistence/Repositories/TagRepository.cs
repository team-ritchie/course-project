using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Persistence.DbContexts;

namespace RitchieBoardGames.Persistence.Repositories;

public class TagRepository : BaseRepository<Tag>, ITagRepository
{
    public TagRepository(BoardGamesDbContext dbContext) : base(dbContext)
    {
    }

    public async Task<List<Tag>> GetTagsWithGames(bool includeTrendingInfo)
    {
        return await _dbContext.Tags.Include(t => t.BoardGames).ToListAsync();
    }

    public Task<bool> IsNameUnique(string name)
    {
        var matches = _dbContext.Tags.Any(e => e.Name.Equals(name));
        return Task.FromResult(matches);
    }
}
