using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.Application.Contracts.Persistence;
using System.Linq.Expressions;
using RitchieBoardGames.Application.Exceptions;
using RitchieBoardGames.Persistence.DbContexts;

namespace RitchieBoardGames.Persistence.Repositories;

public class BaseRepository<T> : IAsyncRepository<T> where T : class
{
    protected readonly BoardGamesDbContext _dbContext;

    public BaseRepository(BoardGamesDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<T> AddAsync(T entity)
    {
        await _dbContext.Set<T>().AddAsync(entity);
        await _dbContext.SaveChangesAsync();

        return entity;
    }

    public async Task DeleteAsync(T entity)
    {
        _dbContext.Set<T>().Remove(entity);
        await _dbContext.SaveChangesAsync();
    }

    public virtual async Task<T> GetByIdAsync(int id)
    {
        var result = await _dbContext.Set<T>().FindAsync(id);
        if (result is null)
        {
            throw new NotFoundException("Id", id);
        }
        return result;
    }

    public async Task<IReadOnlyList<T>> ListAllAsync()
    {
        return await _dbContext.Set<T>().ToListAsync();
    }

    public async Task UpdateAsync(T entity)
    {
        _dbContext.Entry(entity).State = EntityState.Modified;
        await _dbContext.SaveChangesAsync();
    }

    public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
    {
        var result = await _dbContext.Set<T>().FirstOrDefaultAsync(predicate);
        if (result is null)
        {
            throw new NotFoundException("Entity", _dbContext.Set<T>().EntityType);
        }
        return result;

    }

    public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
    {
        return await _dbContext.Set<T>().Where(predicate).ToListAsync();
    }

}
