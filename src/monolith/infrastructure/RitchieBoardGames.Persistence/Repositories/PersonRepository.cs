using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Exceptions;
using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Persistence.DbContexts;

namespace RitchieBoardGames.Persistence.Repositories;

public class PersonRepository : BaseRepository<Person>, IPersonRepository
{
    public PersonRepository(BoardGamesDbContext dbContext) : base(dbContext)
    {
    }

    public async Task<Person> GetFullInfoAsync(int id)
    {
        var person = await _dbContext.Persons.Where(p => p.Id == id).Include(p => p.Restaurants).Include(p => p.GameEvents).FirstOrDefaultAsync();
        if (person is null)
        {
            throw new NotFoundException("Id", id);
        }
        return person;
    }
}
