using Microsoft.EntityFrameworkCore;
using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Domain.Common;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Persistence.DbContexts;

public class BoardGamesDbContext : DbContext
{
    private readonly ILoggedInUserService _loggedInUserService;

    public DbSet<Address> Addresses { get; set; } = null!;
    public DbSet<City> Cities { get; set; } = null!;
    public DbSet<BoardGame> BoardGames { get; set; } = null!;
    public DbSet<GameEvent> GameEvents { get; set; } = null!;
    public DbSet<Person> Persons { get; set; } = null!;
    public DbSet<Restaurant> Restaurants { get; set; } = null!;
    public DbSet<Tag> Tags { get; set; } = null!;

    

    public BoardGamesDbContext(DbContextOptions<BoardGamesDbContext> options, ILoggedInUserService loggedInUserService)
    : base(options)
    {
        _loggedInUserService = loggedInUserService;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // найдет все конфигурации в сборке и применит их
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(BoardGamesDbContext).Assembly);

        var city = new City
        {
            Id = 1,
            Name = "Москва"
        };
        modelBuilder.Entity<City>().HasData(
        city
        );
        modelBuilder.Entity<Address>().HasData(
            new Address
            {
                Id = 1,
                FullAddress = "Арбат, 23",
                CityId = city.Id,

            }, new Address
            {
                Id = 2,
                FullAddress = "Кутузовский проспект, 23",
                CityId = city.Id
            }
            );


        base.OnModelCreating(modelBuilder);
    }

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
        {
            switch (entry.State)
            {
                case EntityState.Modified:
                    entry.Entity.LastModifiedDate = DateTime.Now;
                    entry.Entity.LastModifiedBy = _loggedInUserService.UserId;
                    break;
                case EntityState.Added:
                    entry.Entity.CreatedDate = DateTime.Now;
                    entry.Entity.CreatedBy = _loggedInUserService.UserId;
                    break;
            }
        }
        return await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
    }
}
