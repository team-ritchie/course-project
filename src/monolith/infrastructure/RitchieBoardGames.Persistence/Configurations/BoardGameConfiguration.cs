using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Persistence.Configurations;

public class BoardGameConfiguration : IEntityTypeConfiguration<BoardGame>
{
    public void Configure(EntityTypeBuilder<BoardGame> builder)
    {
        builder.Property(e => e.Name)
            .IsRequired()
            .HasMaxLength(50);

        builder.Property(e => e.MinPersons)
            .IsRequired();

        builder.Property(e => e.MaxPersons)
            .IsRequired();

        builder.Property(e => e.MinAge)
            .IsRequired();
    }
}
