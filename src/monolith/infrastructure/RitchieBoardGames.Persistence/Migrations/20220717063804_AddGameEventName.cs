using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RitchieBoardGames.Persistence.Migrations
{
    public partial class AddGameEventName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameEvents_BoardGames_GameId",
                table: "GameEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonRestaurant_Persons_PersonId",
                table: "PersonRestaurant");

            migrationBuilder.RenameColumn(
                name: "PersonId",
                table: "PersonRestaurant",
                newName: "PersonsId");

            migrationBuilder.AlterColumn<int>(
                name: "GameId",
                table: "GameEvents",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "GameEvents",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddForeignKey(
                name: "FK_GameEvents_BoardGames_GameId",
                table: "GameEvents",
                column: "GameId",
                principalTable: "BoardGames",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRestaurant_Persons_PersonsId",
                table: "PersonRestaurant",
                column: "PersonsId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameEvents_BoardGames_GameId",
                table: "GameEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonRestaurant_Persons_PersonsId",
                table: "PersonRestaurant");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "GameEvents");

            migrationBuilder.RenameColumn(
                name: "PersonsId",
                table: "PersonRestaurant",
                newName: "PersonId");

            migrationBuilder.AlterColumn<int>(
                name: "GameId",
                table: "GameEvents",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AddForeignKey(
                name: "FK_GameEvents_BoardGames_GameId",
                table: "GameEvents",
                column: "GameId",
                principalTable: "BoardGames",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRestaurant_Persons_PersonId",
                table: "PersonRestaurant",
                column: "PersonId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
