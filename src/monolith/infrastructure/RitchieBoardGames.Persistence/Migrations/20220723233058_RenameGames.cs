using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RitchieBoardGames.Persistence.Migrations
{
    public partial class RenameGames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BoardGameTag_BoardGames_GamesId",
                table: "BoardGameTag");

            migrationBuilder.DropForeignKey(
                name: "FK_GameEvents_BoardGames_GameId",
                table: "GameEvents");

            migrationBuilder.RenameColumn(
                name: "GameId",
                table: "GameEvents",
                newName: "BoardGameId");

            migrationBuilder.RenameIndex(
                name: "IX_GameEvents_GameId",
                table: "GameEvents",
                newName: "IX_GameEvents_BoardGameId");

            migrationBuilder.RenameColumn(
                name: "GamesId",
                table: "BoardGameTag",
                newName: "BoardGamesId");

            migrationBuilder.AddForeignKey(
                name: "FK_BoardGameTag_BoardGames_BoardGamesId",
                table: "BoardGameTag",
                column: "BoardGamesId",
                principalTable: "BoardGames",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameEvents_BoardGames_BoardGameId",
                table: "GameEvents",
                column: "BoardGameId",
                principalTable: "BoardGames",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BoardGameTag_BoardGames_BoardGamesId",
                table: "BoardGameTag");

            migrationBuilder.DropForeignKey(
                name: "FK_GameEvents_BoardGames_BoardGameId",
                table: "GameEvents");

            migrationBuilder.RenameColumn(
                name: "BoardGameId",
                table: "GameEvents",
                newName: "GameId");

            migrationBuilder.RenameIndex(
                name: "IX_GameEvents_BoardGameId",
                table: "GameEvents",
                newName: "IX_GameEvents_GameId");

            migrationBuilder.RenameColumn(
                name: "BoardGamesId",
                table: "BoardGameTag",
                newName: "GamesId");

            migrationBuilder.AddForeignKey(
                name: "FK_BoardGameTag_BoardGames_GamesId",
                table: "BoardGameTag",
                column: "GamesId",
                principalTable: "BoardGames",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameEvents_BoardGames_GameId",
                table: "GameEvents",
                column: "GameId",
                principalTable: "BoardGames",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
