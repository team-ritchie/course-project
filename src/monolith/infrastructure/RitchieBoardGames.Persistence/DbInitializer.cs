using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Persistence.DbContexts;

namespace RitchieBoardGames.Persistence;

public static class DbInitializer
{
    public static void Seed(IApplicationBuilder applicationBuilder)
    {
        BoardGamesDbContext context = applicationBuilder.ApplicationServices.CreateScope()
            .ServiceProvider.GetRequiredService<BoardGamesDbContext>();

        if (!context.Tags.Any())
        {
            context.Tags.AddRange(Tags.Select(t => t.Value));
        }
        
        if (!context.BoardGames.Any())
        {
            context.BoardGames.AddRange(BoardGames.Select(bg => bg.Value));
        }

        context.SaveChanges();
    }

    private static Dictionary<string, Tag>? tags;
    public static Dictionary<string, Tag> Tags
    {
        get
        {
            if (tags is null)
            {
                var tagsArray = new Tag[]
                {
                    new Tag { Name = "Вечериночные игры"},
                    new Tag { Name = "Викторины"},
                    new Tag { Name = "Детективные игры"},
                    new Tag { Name = "Карточные игры"},
                    new Tag { Name = "Командные игры"},
                    new Tag { Name = "Классические игры"},
                    new Tag { Name = "Логические игры"},
                    new Tag { Name = "Стратегические игры"},
                    new Tag { Name = "Экономические игры"},
                };

                tags = new();

                foreach (Tag tag in tagsArray)
                {
                    tags.Add(tag.Name, tag);
                }
            }

            return tags;
        }
    }

    private static Dictionary<string, BoardGame>? boardGames;
    public static Dictionary<string, BoardGame> BoardGames
    {
        get
        {
            if (boardGames is null)
            {
                var boardGamesArray = new BoardGame[]
                {
                    new BoardGame
                    {
                        Name = "Цитадели",
                        Description = "Интриги и строительство",
                        ImageUri = @"https://hobbygames.cdnvideo.ru/image/cache/hobbygames_beta/data/HobbyWorld/Citadels_Classic/citadels_classic_00-209x273.jpg",
                        MaxAge = 0,
                        MinAge = 10,
                        MinDuration = 30,
                        MaxDuration = 60,
                        MinPersons = 2,
                        MaxPersons = 7,
                        Tags = new List<Tag>{Tags["Вечериночные игры"]}
                    },
                    new BoardGame
                    {
                        Name = "Неудержимые единорожки",
                        Description = "Взрослый ржач",
                        ImageUri = @"https://hobbygames.cdnvideo.ru/image/cache/hobbygames_beta/data/HobbyWorld/Neuderzhimye_Edinorozhki18/Photos/Neuderzhimye_Edinorozhki18_00-209x273.jpg",
                        MaxAge = 0,
                        MinAge = 18,
                        MinDuration = 30,
                        MaxDuration = 60,
                        MinPersons = 2,
                        MaxPersons = 8,
                        Tags = new List<Tag>{Tags["Вечериночные игры"]}
                    },
                    new BoardGame
                    {
                        Name = "Манчкин",
                        Description = "Хапай сокровища, бей монстров!",
                        ImageUri = "https://hobbygames.cdnvideo.ru/image/cache/hobbygames_beta/data/HobbyWorld/Munchkin/Munchkin/Munchkin00-209x273.jpg",
                        MaxAge = 0,
                        MinAge = 10,
                        MinDuration = 30,
                        MaxDuration = 0,
                        MinPersons = 3,
                        MaxPersons = 6,
                        Tags = new List<Tag>{Tags["Вечериночные игры"]}
                    },
                };

                boardGames = new();
                foreach (BoardGame boardGame in boardGamesArray)
                {
                    boardGames.Add(boardGame.Name, boardGame);
                }

            }
            return boardGames;
        }
    }

}
