using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Contracts.Services;
using RitchieBoardGames.Persistence.DbContexts;
using RitchieBoardGames.Persistence.Repositories;
using RitchieBoardGames.Services.BoardGame;

namespace RitchieBoardGames.Persistence;

public static class PersistenceServiceRegistration
{
    public static IServiceCollection AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<BoardGamesDbContext>(options =>
        options.UseSqlite(configuration["ConnectionStrings:BoardGamesConnectionString"]));

        services.AddScoped(typeof(IAsyncRepository<>), typeof(BaseRepository<>));

        services.AddScoped<IAddressRepository, AddressRepository>();
        services.AddScoped<ICityRepository, CityRepository>();
        services.AddScoped<IBoardGameRepository, BoardGameRepository>();
        services.AddScoped<IGameEventRepository, GameEventRepository>();
        services.AddScoped<IPersonRepository, PersonRepository>();
        services.AddScoped<IRestaurantRepository, RestaurantRepository>();
        services.AddScoped<ITagRepository, TagRepository>();

        services.AddScoped<IBoardGameService, BoardGameService>();

        return services;
    }
}
