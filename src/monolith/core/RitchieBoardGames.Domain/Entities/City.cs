using RitchieBoardGames.Domain.Common;

namespace RitchieBoardGames.Domain.Entities;

public class City : AuditableEntity
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public List<Address> Addresses { get; set; } = new();
}
