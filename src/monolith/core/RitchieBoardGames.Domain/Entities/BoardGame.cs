using RitchieBoardGames.Domain.Common;

namespace RitchieBoardGames.Domain.Entities;

public class BoardGame : AuditableEntity
{
    /// <summary>
    /// Id настольной игры
    /// </summary>
    public int Id { get; set; }
    
    /// <summary>
    /// Название настольной игры
    /// </summary>
    public string Name { get; set; } = null!;
    
    /// <summary>
    /// Описание настольной игры
    /// </summary>
    public string? Description { get; set; }
    
    /// <summary>
    /// Минимальное количество игроков
    /// </summary>
    public int? MinPersons { get; set; }

    /// <summary>
    /// Максимальное количество игроков
    /// </summary>
    public int? MaxPersons { get; set; }
    
    /// <summary>
    /// С какого возраста (в годах) можно играть в игру
    /// </summary>
    public int? MinAge { get; set; }

    /// <summary>
    /// До какого возраста (в годах) игра будет интересна
    /// </summary>
    public int? MaxAge { get; set; }
    
    /// <summary>
    /// Минимальная продолжительность игры
    /// </summary>
    public int? MinDuration { get; set; }
    
    /// <summary>
    /// Максимальная продолжительность игры
    /// </summary>
    public int? MaxDuration { get; set; }
    
    /// <summary>
    /// Ссылка на изображение игры
    /// </summary>
    public string ImageUri { get; set; } = string.Empty;
    
    /// <summary>
    /// Мероприятия, в которых использовалась данная игра
    /// </summary>
    public List<GameEvent> GameEvents { get; set; } = new();
    
    /// <summary>
    /// Теги, применимые к данной игре
    /// </summary>
    public List<Tag> Tags { get; set; } = new();

}
