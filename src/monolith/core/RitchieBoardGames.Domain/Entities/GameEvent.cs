using RitchieBoardGames.Domain.Common;

namespace RitchieBoardGames.Domain.Entities;

public class GameEvent : AuditableEntity
{
    public int Id { get; set; }
    public string Name { get; set; } = String.Empty;
    public DateTime TimeStart { get; set; } = DateTime.Now;
    public int MaxPlayersCount { get; set; } = 0;
    public int MinPlayersCount { get; set; } = 0;
    public int MaxAge { get; set; } = 0;
    public int MinAge { get; set; } = 0;
    public string Note { get; set; } = string.Empty;
    public int BoardGameId { get; set; } = 0;
    public BoardGame BoardGame { get; set; } = null!;
    public int RestaurantId { get; set; } = 0; 
    public Restaurant Restaurant { get; set; } = null!;
    public List<Person> Persons { get; set; } = new();
    public int NumberOfPersons => Persons.Count;
}
