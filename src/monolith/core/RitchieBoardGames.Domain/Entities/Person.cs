using RitchieBoardGames.Domain.Common;

namespace RitchieBoardGames.Domain.Entities;

public class Person : AuditableEntity
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Phone { get; set; } = string.Empty;
    public DateTime Birthday { get; set; }
    public string ImageUri { get; set; } = string.Empty;
    public string Email { get; set; } = null!;
    public List<Restaurant> Restaurants { get; set; } = new();
    public List<GameEvent> GameEvents { get; set; } = new();
}
