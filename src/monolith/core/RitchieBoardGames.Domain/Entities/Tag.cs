using RitchieBoardGames.Domain.Common;

namespace RitchieBoardGames.Domain.Entities;

public class Tag : AuditableEntity
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public List<BoardGame> BoardGames { get; set; } = new();
}
