using RitchieBoardGames.Domain.Common;

namespace RitchieBoardGames.Domain.Entities;

public class Restaurant : AuditableEntity
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public Address Address { get; set; } = null!;
    public int AddressId { get; set; }
    public string Phone { get; set; } = null!;
    public short OpenHour { get; set; }
    public short ClosingHour { get; set; }
    public string ImageUri { get; set; } = null!;
    public List<Person> Persons { get; set; } = new();
    public List<GameEvent> GameEvents { get; set; } = new();
}
