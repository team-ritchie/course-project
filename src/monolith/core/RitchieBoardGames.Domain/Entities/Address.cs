using RitchieBoardGames.Domain.Common;

namespace RitchieBoardGames.Domain.Entities;

public class Address : AuditableEntity
{
    public int Id { get; set; }
    public string FullAddress { get; set; } = String.Empty;
    public Restaurant Restaurant { get; set; } = null!;
    public int RestaurandId { get; set; }
    public City City { get; set; } = null!;
    public int CityId { get; set; }
}
