namespace RitchieBoardGames.Application.Contracts.Infrastructure;

public interface ILoggedInUserService
{
    public string? UserId { get; }
}
