using RitchieBoardGames.Application.Models.Mail;

namespace RitchieBoardGames.Application.Contracts.Infrastructure;

public interface IEmailService
{
    Task<bool> SendEmailAsync(Email email);
}
