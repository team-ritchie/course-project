using RitchieBoardGames.Application.Models.Authentication;

namespace RitchieBoardGames.Application.Contracts.Identity;

public interface IAuthenticationService
{
    Task<AuthenticationResponse> AuthenticateAsync(AuthenticationRequest request);
}
