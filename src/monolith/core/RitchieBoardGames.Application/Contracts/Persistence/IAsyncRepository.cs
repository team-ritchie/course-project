using System.Linq.Expressions;

namespace RitchieBoardGames.Application.Contracts.Persistence;

public interface IAsyncRepository<T> where T : class
{
    Task<T> GetByIdAsync(int id);
    Task<IReadOnlyList<T>> ListAllAsync();
    Task<T> AddAsync(T entity);
    Task UpdateAsync(T entity);
    Task DeleteAsync(T entity);
    Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate);
    Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);

}
