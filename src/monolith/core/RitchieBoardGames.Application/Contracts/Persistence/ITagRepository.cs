using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Contracts.Persistence;

public interface ITagRepository : IAsyncRepository<Tag>
{
    Task<List<Tag>> GetTagsWithGames(bool includeTrendingInfo);
    Task<bool> IsNameUnique(string name);
}
