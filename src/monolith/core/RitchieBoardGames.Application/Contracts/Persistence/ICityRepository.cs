using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Contracts.Persistence;

public interface ICityRepository : IAsyncRepository<City>
{
    
    Task<bool> IsNameUnique(string name);
    Task<City?> GetCityByName(string name);
}
