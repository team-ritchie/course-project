using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Contracts.Persistence;

public interface IRestaurantRepository : IAsyncRepository<Restaurant>
{
}
