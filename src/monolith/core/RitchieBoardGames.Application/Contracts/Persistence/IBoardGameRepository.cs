using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Contracts.Persistence;

public interface IBoardGameRepository : IAsyncRepository<BoardGame>
{
    Task<bool> IsNameExist(string name);
    Task<IReadOnlyList<BoardGame>> ListAllByTagIdAsync(int tagId);
}
