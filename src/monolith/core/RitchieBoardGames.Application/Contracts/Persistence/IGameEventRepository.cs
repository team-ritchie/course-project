using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsByFilters;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Contracts.Persistence;

public interface IGameEventRepository : IAsyncRepository<GameEvent>
{
    Task<List<GameEvent>> GetGameEventsByFilters(GetGameEventsByFiltersQuery gameEventInfo);
    Task<bool> IsGameEventNameUnique(int id, string gameEventName);
}
