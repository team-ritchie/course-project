using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Contracts.Persistence;

public interface IAddressRepository : IAsyncRepository<Address>
{
}
