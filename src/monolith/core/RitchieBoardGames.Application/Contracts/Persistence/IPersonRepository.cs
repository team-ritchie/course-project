using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Contracts.Persistence;

public interface IPersonRepository : IAsyncRepository<Person>
{
    public Task<Person> GetFullInfoAsync(int id);
}
