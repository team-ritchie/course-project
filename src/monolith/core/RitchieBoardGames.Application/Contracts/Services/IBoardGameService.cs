using RitchieBoardGames.Application.Models.Services.BoardGame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RitchieBoardGames.Application.Contracts.Services
{
    public interface IBoardGameService
    {
        Task<BoardGameGetListResponce> GetAll();
    }
}
