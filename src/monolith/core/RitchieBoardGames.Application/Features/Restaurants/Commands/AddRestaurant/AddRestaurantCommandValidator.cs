using FluentValidation;

namespace RitchieBoardGames.Application.Features.Restaurants.Commands.AddRestaurant;

public class AddRestaurantCommandValidator : AbstractValidator<AddRestaurantCommand>
{
    public AddRestaurantCommandValidator()
    {
    }
}
