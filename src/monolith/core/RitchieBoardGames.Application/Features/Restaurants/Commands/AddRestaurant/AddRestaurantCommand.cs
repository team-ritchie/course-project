using MediatR;

namespace RitchieBoardGames.Application.Features.Restaurants.Commands.AddRestaurant;

public class AddRestaurantCommand : IRequest<int>
{
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string Phone { get; set; } = null!;
    public short OpenHour { get; set; }
    public short ClosingHour { get; set; }
}
