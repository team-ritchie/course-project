using MediatR;

namespace RitchieBoardGames.Application.Features.Restaurants.Commands.DeleteRestaurant
{
    public class DeleteRestaurantCommand : IRequest
    {
        public int Id { get; set; }
    }
}
