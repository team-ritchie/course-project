using MediatR;
using Microsoft.AspNetCore.JsonPatch;

namespace RitchieBoardGames.Application.Features.Restaurants.Commands.PatchRestaurant;

public class PatchRestaurantCommand : IRequest
{
    public int Id { get; set; }
    public JsonPatchDocument<RestaurantForPatch> PatchDocument { get; set; } = null!;
}
