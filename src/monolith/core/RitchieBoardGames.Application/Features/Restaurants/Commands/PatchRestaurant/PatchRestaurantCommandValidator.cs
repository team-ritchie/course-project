using FluentValidation;

namespace RitchieBoardGames.Application.Features.Restaurants.Commands.PatchRestaurant;

public class PatchRestaurantCommandValidator : AbstractValidator<RestaurantForPatch>
{
    public PatchRestaurantCommandValidator()
    {
    }
}
