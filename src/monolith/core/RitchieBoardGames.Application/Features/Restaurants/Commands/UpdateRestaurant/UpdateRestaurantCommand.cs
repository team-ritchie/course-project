using MediatR;

namespace RitchieBoardGames.Application.Features.Restaurants.Commands.UpdateRestaurant;

public class UpdateRestaurantCommand : IRequest
{
    public int Id { get; set; }
    public RestaurantForUpdateDto RestaurantForUpdate { get; set; } = null!;
}
