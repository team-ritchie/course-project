using FluentValidation;

namespace RitchieBoardGames.Application.Features.Restaurants.Commands.UpdateRestaurant;

public class UpdateRestaurantCommandValidator : AbstractValidator<RestaurantForUpdateDto>
{
    public UpdateRestaurantCommandValidator()
    {
    }
}
