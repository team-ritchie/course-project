namespace RitchieBoardGames.Application.Features.Restaurants.Queries.GetRestaurantDetail
{
    public class RestaurantsAddressDto
    {
        public int Id { get; set; }
        public string FullAddress { get; set; } = String.Empty;
        public string CityName { get; set; } = null!;
        public int CityId { get; set; }
    }
}
