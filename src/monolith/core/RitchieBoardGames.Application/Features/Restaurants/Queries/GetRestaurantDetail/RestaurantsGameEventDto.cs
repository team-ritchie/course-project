namespace RitchieBoardGames.Application.Features.Restaurants.Queries.GetRestaurantDetail
{
    public class RestaurantsGameEventDto
    {
        public int GameEventId { get; set; }
        public string Name { get; set; } = String.Empty;
        public DateTime TimeStart { get; set; }
        public int MaxPlayersCount { get; set; }
        public int MinPlayersCount { get; set; }
        public int MaxAge { get; set; }
        public int MinAge { get; set; }
        public string Note { get; set; } = string.Empty;
        public int BoardGameId { get; set; }
        public string BoardGameName { get; set; } = null!;
        public int NumberOfPersons { get; set; }
    }
}
