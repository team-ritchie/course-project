namespace RitchieBoardGames.Application.Features.Restaurants.Queries.GetRestaurantDetail
{
    /// <summary>
    /// Человек, который отображается в списке людей, представляющих ресторан
    /// </summary>
    public class RestaurantsPersonDto
    {
        /// <summary>
        /// Id человека, представляющего ресторан
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; } = string.Empty;
        
        /// <summary>
        /// День рождения для вычисления возраста
        /// </summary>
        public DateTime Birthday { get; set; }
        
        /// <summary>
        /// Ссылка на фото (аватар)
        /// </summary>
        public string ImageUri { get; set; } = string.Empty;
    }
}
