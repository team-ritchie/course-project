namespace RitchieBoardGames.Application.Features.Restaurants.Queries.GetRestaurantDetail;

/// <summary>
/// Полная информация о ресторане (ответ на запрос)
/// </summary>
public class RestaurantDetailVm
{
    
    /// <summary>
    /// Id ресторана
    /// </summary>
    public int Id { get; set; }
    
    /// <summary>
    /// Название ресторана
    /// </summary>
    public string Name { get; set; } = string.Empty;
    
    /// <summary>
    /// Описание ресторана
    /// </summary>
    public string Description { get; set; } = string.Empty;
    
    /// <summary>
    /// Адрес ресторана
    /// </summary>
    public RestaurantsAddressDto Address { get; set; } = null!;
    
    /// <summary>
    /// Id адреса ресторана
    /// </summary>
    public int AddressId { get; set; }
    
    /// <summary>
    /// Телефон ресторана
    /// </summary>
    public string Phone { get; set; } = null!;

    /// <summary>
    /// Час, с которого открывается ресторан. Если работате с 8, значит, OpenHour = 8
    /// </summary>
    public short OpenHour { get; set; }

    /// <summary>
    /// Час, когда закрывается ресторан. Если закрывается в 22, то ClosingHour 22
    /// </summary>
    public short ClosingHour { get; set; }
    
    /// <summary>
    /// Фото ресторана
    /// </summary>
    public string ImageUri { get; set; } = null!;
    
    /// <summary>
    /// Люди, которые являются представителями ресторана
    /// </summary>
    public List<RestaurantsPersonDto> Persons { get; set; } = new();
    
    /// <summary>
    /// Мероприятия, которые проходят, проходили или будут проходить в ресторане
    /// </summary>
    public List<RestaurantsGameEventDto> GameEvents { get; set; } = new();
}
