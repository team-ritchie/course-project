namespace RitchieBoardGames.Application.Features.Restaurants.Queries.GetRestaurantList;

/// <summary>
/// Набор полей ресторана для запрашиваемого списка ресторанов
/// </summary>
public class RestaurantListVm
{
    
    /// <summary>
    /// Id ресторана
    /// </summary>
    public int Id { get; set; }
    
    /// <summary>
    /// Название ресторана
    /// </summary>
    public string Name { get; set; } = string.Empty;
    
    /// <summary>
    /// Строковое представление адреса ресторана
    /// </summary>
    public string Address { get; set; } = null!;
    
    /// <summary>
    /// Id адреса
    /// </summary>
    public int AddressId { get; set; }
    
    /// <summary>
    /// Телефон ресторана
    /// </summary>
    public string Phone { get; set; } = null!;
    
    /// <summary>
    /// Час открытия ресторана
    /// </summary>
    public short OpenHour { get; set; }
    
    /// <summary>
    /// Час закрытия ресторана
    /// </summary>
    public short ClosingHour { get; set; }
    
    /// <summary>
    /// Ссылка на фото ресторана
    /// </summary>
    public string ImageUri { get; set; } = null!;
}
