namespace RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressesList;

public class AddressListVm
{
    public int Id { get; set; }
    public string FullAddress { get; set; } = null!;
    public string RestaurantName { get; set; } = null!;
    public string City { get; set; } = null!;
}
