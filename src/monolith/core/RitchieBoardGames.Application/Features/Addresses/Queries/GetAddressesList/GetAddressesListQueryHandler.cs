using MediatR;

namespace RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressesList;

public class GetAddressesListQueryHandler : IRequestHandler<GetAddressesListQuery, List<AddressListVm>>
{
    public Task<List<AddressListVm>> Handle(GetAddressesListQuery request, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}
