using MediatR;

namespace RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressesList;

public class GetAddressesListQuery : IRequest<List<AddressListVm>>
{

}
