namespace RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressesListByCity;

public class AddressListByCityVm
{
    public int Id { get; set; }
    public string FullAddress { get; set; } = null!;
    public string RestaurantName { get; set; } = null!;
}
