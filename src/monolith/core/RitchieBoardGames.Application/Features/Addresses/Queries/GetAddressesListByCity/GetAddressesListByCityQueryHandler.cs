using MediatR;

namespace RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressesListByCity;

public class GetAddressesListByCityQueryHandler : IRequestHandler<GetAddressesListByCityQuery, List<AddressListByCityVm>>
{
    public Task<List<AddressListByCityVm>> Handle(GetAddressesListByCityQuery request, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}
