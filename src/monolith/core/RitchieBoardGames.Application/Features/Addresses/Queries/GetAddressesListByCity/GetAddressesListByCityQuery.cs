using MediatR;

namespace RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressesListByCity;

public class GetAddressesListByCityQuery : IRequest<List<AddressListByCityVm>>
{

}
