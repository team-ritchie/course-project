using MediatR;

namespace RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressDetail;

public class GetAddressDetailQuery : IRequest<AddressDetailVm>
{
    public int Id { get; set; }
}
