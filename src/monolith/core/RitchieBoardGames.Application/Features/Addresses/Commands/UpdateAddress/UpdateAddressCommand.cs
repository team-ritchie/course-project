using MediatR;

namespace RitchieBoardGames.Application.Features.Addresses.Commands.UpdateAddress;

public class UpdateAddressCommand : IRequest
{
    public int Id { get; set; }
    public AddressForUpdateDto AddressForUpdate { get; set; } = null!;
}
