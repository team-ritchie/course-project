namespace RitchieBoardGames.Application.Features.Addresses.Commands.UpdateAddress;

/// <summary>
/// Поля для полного обновления адреса
/// </summary>
public class AddressForUpdateDto
{
    /// <summary>
    /// Полный новый адрес (без города)
    /// </summary>
    public string FullAddress { get; set; } = null!;

    /// <summary>
    /// Новое название города
    /// </summary>
    public string CityName { get; set; } = null!;
}
