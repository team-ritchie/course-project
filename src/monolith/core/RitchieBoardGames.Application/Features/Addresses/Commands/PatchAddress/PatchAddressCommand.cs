using MediatR;
using Microsoft.AspNetCore.JsonPatch;

namespace RitchieBoardGames.Application.Features.Addresses.Commands.PatchAddress;

public class PatchAddressCommand : IRequest
{
    public int Id { get; set; }
    public JsonPatchDocument<AddressForPatchDto> PatchDocument { get; set; } = null!;
}
