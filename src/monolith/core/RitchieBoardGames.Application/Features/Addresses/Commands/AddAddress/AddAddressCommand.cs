using MediatR;

namespace RitchieBoardGames.Application.Features.Addresses.Commands.AddAddress;

public class AddAddressCommand : IRequest<int>
{
    public string FullAddress { get; set; } = null!;
    public int RestaurandId { get; set; }
    public string CityName { get; set; } = null!;
}
