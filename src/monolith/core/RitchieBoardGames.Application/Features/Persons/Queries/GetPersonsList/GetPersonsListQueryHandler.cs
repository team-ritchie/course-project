using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.Persons.Queries.GetPersonsList;

public class GetPersonsListQueryHandler : IRequestHandler<GetPersonsListQuery, List<PersonListVm>>
{
    private readonly IMapper _mapper;
    private readonly IAsyncRepository<Person> _personRepository;

    public GetPersonsListQueryHandler(IMapper mapper, IAsyncRepository<Person> personRepository)
    {
        _mapper = mapper;
        _personRepository = personRepository;
    }
    public async  Task<List<PersonListVm>> Handle(GetPersonsListQuery request, CancellationToken cancellationToken)
    {
        var persons = await  _personRepository.ListAllAsync();
        return _mapper.Map<List<PersonListVm>>(persons);
    }
}
