using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RitchieBoardGames.Application.Features.Persons.Queries.GetPersonsList
{
    public class PersonListVm
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Phone { get; set; } = string.Empty;
        public DateTime Birthday { get; set; }
        public string ImageUri { get; set; } = string.Empty;
        public string Email { get; set; } = null!;
       
    }
}
