using MediatR;

namespace RitchieBoardGames.Application.Features.Persons.Queries.GetPersonsList;

public class GetPersonsListQuery : IRequest<List<PersonListVm>>
{

}
