using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;

namespace RitchieBoardGames.Application.Features.Persons.Queries.GetPersonDetail;

public class GetPersonDetailQueryHandler : IRequestHandler<GetPersonDetailQuery, PersonDetailVm>
{
    private readonly IMapper _mapper;
    private readonly IPersonRepository _personRepository;

    public GetPersonDetailQueryHandler(IMapper mapper, IPersonRepository personRepository)
    {
        _mapper = mapper;
        _personRepository = personRepository;
    }
    public async Task<PersonDetailVm> Handle(GetPersonDetailQuery request, CancellationToken cancellationToken)
    {
        var person = await _personRepository.GetFullInfoAsync(request.Id);
        var personDto = _mapper.Map<PersonDetailVm>(person);        
        personDto.Restaurants = _mapper.Map<List<RestaurantDto>>(person.Restaurants);
        personDto.GameEvents = _mapper.Map<List<GameEventDto>>(person.GameEvents);

        return personDto;
    }
}
