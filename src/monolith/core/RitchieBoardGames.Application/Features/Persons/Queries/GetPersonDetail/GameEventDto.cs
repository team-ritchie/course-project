namespace RitchieBoardGames.Application.Features.Persons.Queries.GetPersonDetail;

public class GameEventDto
{
    public int Id { get; set; }
    public DateTime TimeStart { get; set; }
    public int MaxPlayersCount { get; set; }
    public int MinPlayersCount { get; set; }
    public int MaxAge { get; set; }
    public int MinAge { get; set; }
    public string Note { get; set; } = string.Empty;
}
