using MediatR;

namespace RitchieBoardGames.Application.Features.Persons.Queries.GetPersonDetail;

public class GetPersonDetailQuery:IRequest<PersonDetailVm>
{
    public int Id { get; set; }
}
