namespace RitchieBoardGames.Application.Features.Persons.Queries.GetPersonDetail;

public class PersonDetailVm
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Phone { get; set; } = string.Empty;
    public DateTime Birthday { get; set; }
    public string ImageUri { get; set; } = string.Empty;
    public string Email { get; set; } = null!;
    public List<RestaurantDto> Restaurants { get; set; } = new();
    public List<GameEventDto> GameEvents { get; set; } = new();
}
