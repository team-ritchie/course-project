namespace RitchieBoardGames.Application.Features.Persons.Queries.GetPersonDetail;

public class RestaurantDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string Phone { get; set; } = null!;
    public short OpenHour { get; set; }
    public short ClosingHour { get; set; }

}
