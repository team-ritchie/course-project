namespace RitchieBoardGames.Application.Features.Persons.Commands.UpdatePerson;

public class UpdatePersonDto
{    
    public string Name { get; set; } = string.Empty;
    public string Phone { get; set; } = string.Empty;
    public DateTime Birthday { get; set; }
    public string ImageUri { get; set; } = string.Empty;
    public string Email { get; set; } = null!;
   
}
