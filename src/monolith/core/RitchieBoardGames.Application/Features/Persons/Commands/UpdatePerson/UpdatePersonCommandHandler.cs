using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RitchieBoardGames.Application.Features.Persons.Commands.UpdatePerson
{
    public class UpdatePersonCommandHandler : IRequestHandler<UpdatePersonCommand>
    {
        private readonly IMapper _mapper;
        private readonly IPersonRepository _personRepository;

        public UpdatePersonCommandHandler(IMapper mapper, IPersonRepository personRepository)
        {
            _mapper = mapper;
            _personRepository = personRepository;
        }
        public async Task<Unit> Handle(UpdatePersonCommand request, CancellationToken cancellationToken)
        {
            var person = await _personRepository.GetByIdAsync(request.Id);
            if(person==null)
                throw new NotFoundException("Игрок с id", request.Id);
            _mapper.Map(request.UpdatePerson, person);

            // TODO: validation

            await _personRepository.UpdateAsync(person);
            return Unit.Value;

        }
    }
}
