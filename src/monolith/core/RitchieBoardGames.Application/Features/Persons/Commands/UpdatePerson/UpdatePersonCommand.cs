using MediatR;

namespace RitchieBoardGames.Application.Features.Persons.Commands.UpdatePerson;

public class UpdatePersonCommand:IRequest
{
    public int Id { get; set; }
    public UpdatePersonDto UpdatePerson { get; set; }

    public UpdatePersonCommand(UpdatePersonDto updatePerson, int id)
    {
        UpdatePerson = updatePerson;
        Id = id;
    }
}
