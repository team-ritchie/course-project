using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Exceptions;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.Persons.Commands.DeletePerson;

public class DeletePersonComandHandler : IRequestHandler<DeletePersonCommand>
{
    private readonly IAsyncRepository<Person> _personRepository;

    public DeletePersonComandHandler(IAsyncRepository<Person> personRepository)
    {
        _personRepository = personRepository;
    }
    public async Task<Unit> Handle(DeletePersonCommand request, CancellationToken cancellationToken)
    {
        var person = await _personRepository.GetByIdAsync(request.Id);
        if (person is null) throw new NotFoundException("personId", request.Id);
        await _personRepository.DeleteAsync(person);
        return Unit.Value;
    }
}
