using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Common;
using RitchieBoardGames.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RitchieBoardGames.Application.Features.Persons.Commands.AddPerson
{
    public class AddPersonCommandHandler : IRequestHandler<AddPersonCommand, int >
    {
        private readonly IPersonRepository _personRepository;
        private readonly IMapper _mapper;

        public AddPersonCommandHandler(IPersonRepository personRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _mapper = mapper;
        }
        public async Task<int> Handle(AddPersonCommand request, CancellationToken cancellationToken)
        {
            var person = _mapper.Map<Person>(request);
            var validator = new AddPersonCommandValidator();
            var validationResult =  await validator.ValidateAsync(request);

            if (validationResult.Errors.Count > 0)
                throw new Exceptions.ValidationException(validationResult);

            var result = await _personRepository.AddAsync(person);
            return result.Id;
        }
    }
}
