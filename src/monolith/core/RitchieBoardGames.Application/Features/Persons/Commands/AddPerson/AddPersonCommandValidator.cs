using FluentValidation;

namespace RitchieBoardGames.Application.Features.Persons.Commands.AddPerson;

internal class AddPersonCommandValidator : AbstractValidator<AddPersonCommand>
{
    public AddPersonCommandValidator()
    {
        RuleFor(p => p.Name)
            .NotEmpty()
            .WithErrorCode("Имя игрока не должно быть пустым");

        RuleFor(p => p.Phone)
            .MinimumLength(7)
            .WithErrorCode("Некорректный номер телефона");

        RuleFor(p => p.Email)
             .NotEmpty()
             .WithErrorCode("Email игрока не должно быть пустым");

        RuleFor(p => p.Birthday)
             .LessThanOrEqualTo(DateTime.Now)
             .WithErrorCode("Некорретная дата рождения игрока");

    }
}
