using FluentValidation;
using RitchieBoardGames.Application.Contracts.Persistence;

namespace RitchieBoardGames.Application.Features.GameEvents.Commands.CreateGameEvent;

public class CreateGameEventValidator : AbstractValidator<CreateGameEventCommand>
{
    private readonly IGameEventRepository _gameEventRepository;

    public CreateGameEventValidator(IGameEventRepository gameEventRepository)
    {
        _gameEventRepository = gameEventRepository;

        RuleFor(p => p.GameId).GreaterThan(0).WithMessage("Игра не выбрана");

        RuleFor(p => p.RestaurantId).GreaterThan(0).WithMessage("Место проведения мероприятия обязательно");

        RuleFor(p => p.TimeStart).NotNull().WithMessage("Дата и время начала мероприятия обязательны");

        RuleFor(p => p.MinPlayersCount).LessThanOrEqualTo(p => p.MaxPlayersCount).WithMessage("Минимальное количество участников должно быть меньше максимального");

        RuleFor(p => p.MinAge).LessThanOrEqualTo(p => p.MaxAge).WithMessage("Минимальный возраст должен быть меньше максимального");

        RuleFor(p => p.Note).NotNull().MaximumLength(3000).WithMessage("Описание события слишком длинное.");

        RuleFor(p => p.Name).NotEmpty().WithMessage("Название мероприятия обязательно")
            .NotNull()
            .MaximumLength(500).WithMessage("Название мероприятия слишком длинное, должно быть не более 500 символов.");

        RuleFor(e => e).MustAsync(GameEventNameUnique).WithMessage("Название мероприятия должно быть уникально");
    }

    private async Task<bool> GameEventNameUnique(CreateGameEventCommand e, CancellationToken token)
    {
        return !(await _gameEventRepository.IsGameEventNameUnique(0, e.Name));
    }
}
