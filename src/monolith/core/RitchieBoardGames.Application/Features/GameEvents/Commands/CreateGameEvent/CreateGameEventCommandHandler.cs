using AutoMapper;
using MediatR;
using System.Globalization;
// using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Models;
using RitchieBoardGames.Domain.Entities;


namespace RitchieBoardGames.Application.Features.GameEvents.Commands.CreateGameEvent;

public class CreateGameEventCommandHandler : IRequestHandler<CreateGameEventCommand, int>
{
    private readonly IMapper _mapper;
    private readonly IGameEventRepository _gameEventRepository;
    private readonly ICityRepository _cityRepository;
    private readonly IBoardGameRepository _gameRepository;
    private readonly IRestaurantRepository _restaurantRepository;
    private readonly IEmailService _emailService;

    public CreateGameEventCommandHandler(IMapper mapper, IGameEventRepository gameEventRepository, ICityRepository cityRepository, IBoardGameRepository gameRepository, IRestaurantRepository restaurantRepository, IEmailService emailService)
    {
        _mapper = mapper;
        _gameEventRepository = gameEventRepository;
        _cityRepository = cityRepository;
        _gameRepository = gameRepository;
        _restaurantRepository = restaurantRepository;
        _emailService = emailService;
    }

    public async Task<int> Handle(CreateGameEventCommand request, CancellationToken cancellationToken)
    {
        var validator = new CreateGameEventValidator(_gameEventRepository);
        var validationResult = await validator.ValidateAsync(request);

        if (validationResult != null && validationResult.Errors.Count > 0)
            throw new Exceptions.ValidationException(validationResult);

        var gameEvent = _mapper.Map<GameEvent>(request);

        gameEvent = await LoadFKData(gameEvent, request.GameId, request.RestaurantId);

        gameEvent = await _gameEventRepository.AddAsync(gameEvent);

        // await SentInfoAboutEvent(gameEvent);

        return gameEvent.Id;
    }

    private async Task<GameEvent> LoadFKData(GameEvent row, int gameId, int restaurantId)
    {
        if (gameId > 0)
        {
            var res = await _gameRepository.GetByIdAsync(gameId);
            row.BoardGame = res; // await _gameRepository.GetByIdAsync(gameId);
            if (row.BoardGame == null)
                throw new ArgumentException("Игра не найдена.");
        }

        if (restaurantId > 0)
        {
            row.Restaurant = await _restaurantRepository.GetByIdAsync(restaurantId);
            if (row.Restaurant == null)
                throw new ArgumentException("Ресторан не найден.");
        }

        return row;
    }

}
