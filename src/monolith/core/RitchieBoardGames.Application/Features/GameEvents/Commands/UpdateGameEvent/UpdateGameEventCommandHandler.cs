using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;
using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RitchieBoardGames.Application.Features.GameEvents.Commands.UpdateGameEvent
{
    public class UpdateGameEventCommandHandler : IRequestHandler<UpdateGameEventCommand>
    {
        private readonly IMapper _mapper;
        private readonly IGameEventRepository _gameEventRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IBoardGameRepository _gameRepository;
        private readonly IRestaurantRepository _restaurantRepository;
        private readonly IEmailService _emailService;

        public UpdateGameEventCommandHandler(IMapper mapper, IGameEventRepository gameEventRepository, ICityRepository cityRepository, IBoardGameRepository gameRepository, IRestaurantRepository restaurantRepository, IEmailService emailService)
        {
            _mapper = mapper;
            _gameEventRepository = gameEventRepository;
            _cityRepository = cityRepository;
            _gameRepository = gameRepository;
            _restaurantRepository = restaurantRepository;
            _emailService = emailService;

        }
        public async Task<Unit> Handle(UpdateGameEventCommand request, CancellationToken cancellationToken)
        {
            var gameEventToUpdate = await _gameEventRepository.GetByIdAsync(request.Id);

            var validator = new UpdateGameEventValidator(_gameEventRepository);
            var validationResult = await validator.ValidateAsync(request);

            if (validationResult.Errors.Count > 0)
                throw new Exceptions.ValidationException(validationResult);

            gameEventToUpdate = (GameEvent)_mapper.Map(request.GameEvent, gameEventToUpdate, typeof(GameEventForUpdateDto), typeof(GameEvent));

            gameEventToUpdate = await LoadFKData(gameEventToUpdate, (int)request.GameEvent.GameId, (int)request.GameEvent.RestaurantId);

            await _gameEventRepository.UpdateAsync(gameEventToUpdate);

            return Unit.Value;
        }

        private async Task<GameEvent> LoadFKData(GameEvent row, int gameId, int restaurantId)
        {
            if (gameId > 0)
            {
                row.BoardGame = await _gameRepository.GetByIdAsync((int)gameId);
                if (row.BoardGame == null)
                    throw new ArgumentException("Игра не найдена.");
            }

            if (restaurantId > 0)
            {
                row.Restaurant = await _restaurantRepository.GetByIdAsync(restaurantId);
                if (row.Restaurant == null)
                    throw new ArgumentException("Ресторан не найден.");
            }
            
            return row;
        }



    }
}
