using MediatR;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.GameEvents.Commands.UpdateGameEvent;

public class GameEventForUpdateDto
{
    public string Name { get; set; } = null!;
    public int GameId { get; set; }
    public int RestaurantId { get; set; }
    public DateTime TimeStart { get; set; } = DateTime.Now;
    public int MinPlayersCount { get; set; }
    public int MaxPlayersCount { get; set; }
    public int MinAge { get; set; }
    public int MaxAge { get; set; }
    public string? Note { get; set; }
}
