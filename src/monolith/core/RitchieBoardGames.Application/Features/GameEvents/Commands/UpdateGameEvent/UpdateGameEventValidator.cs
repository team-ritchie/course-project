using FluentValidation;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.GameEvents.Commands.UpdateGameEvent;

public class UpdateGameEventValidator : AbstractValidator<UpdateGameEventCommand>
{
    private readonly IGameEventRepository _gameEventRepository =null!;

    public UpdateGameEventValidator(IGameEventRepository gameEventRepository)
    {
        _gameEventRepository = gameEventRepository;

        RuleFor(p => p.GameEvent.GameId).GreaterThan(0).WithMessage("Игра не выбрана");

        RuleFor(p => p.GameEvent.RestaurantId).GreaterThan(0).WithMessage("Место проведения мероприятия обязательно");

        RuleFor(p => p.GameEvent.TimeStart).NotNull().WithMessage("Дата и время начала мероприятия обязательны");

        RuleFor(p => p.GameEvent.MinPlayersCount).LessThanOrEqualTo(p => p.GameEvent.MaxPlayersCount).WithMessage("Минимальное количество участников должно быть меньше максимального");

        RuleFor(p => p.GameEvent.MinAge).LessThanOrEqualTo(p => p.GameEvent.MaxAge).WithMessage("Минимальный возраст должен быть меньше максимального");

        RuleFor(p => p.GameEvent.Note).NotNull().MaximumLength(3000).WithMessage("Описание события слишком длинное.");

        RuleFor(p => p.GameEvent.Name).NotEmpty().WithMessage("Название мероприятия обязательно")
            .NotNull()
            .MaximumLength(500).WithMessage("Название мероприятия слишком длинное, должно быть не более 500 символов.");

        RuleFor(e => e).MustAsync(GameEventNameUnique).WithMessage("Название мероприятия должно быть уникально");
    }

    private async Task<bool> GameEventNameUnique(UpdateGameEventCommand e, CancellationToken token)
    {
        return !(await _gameEventRepository.IsGameEventNameUnique(e.Id, e.GameEvent.Name));
    }

}
