using MediatR;

namespace RitchieBoardGames.Application.Features.GameEvents.Commands.UpdateGameEvent;

public class UpdateGameEventCommand : IRequest
{
    public int Id { get; set; }
    public GameEventForUpdateDto GameEvent { get; set; } = null!;

}
