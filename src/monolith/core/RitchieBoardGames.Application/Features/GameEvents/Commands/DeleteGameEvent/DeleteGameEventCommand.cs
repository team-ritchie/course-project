using MediatR;

namespace RitchieBoardGames.Application.Features.GameEvents.Commands.DeleteGameEvent
{
    public class DeleteGameEventCommand : IRequest
    {
        public int Id { get; set; }
    }
}
