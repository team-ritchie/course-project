using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.GameEvents.Commands.DeleteGameEvent;

public class DeleteGameEventCommandHandler : IRequestHandler<DeleteGameEventCommand>
{
    private readonly IMapper _mapper;
    private readonly IGameEventRepository _gameEventRepository;

    public DeleteGameEventCommandHandler(IMapper mapper, IGameEventRepository gameEventRepository)
    {
        _mapper = mapper;
        _gameEventRepository = gameEventRepository;
    }

    public async Task<Unit> Handle(DeleteGameEventCommand request, CancellationToken cancellationToken)
    {
        var gameEventToDelete = await _gameEventRepository.GetByIdAsync(request.Id);
        
        // check permissions
        
        // delete gameEvent
        await _gameEventRepository.DeleteAsync(gameEventToDelete);
    
        return Unit.Value;
    }
}
