using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsByFilters;

public class GetGameEventsByFiltersQueryHandler : IRequestHandler<GetGameEventsByFiltersQuery, List<GameEventsByFiltersVm>>
{
    private readonly IMapper _mapper;
    private readonly IGameEventRepository _gameEventRepository;
    private readonly ICityRepository _cityRepository;
    private readonly IBoardGameRepository _gameRepository;
    private readonly IRestaurantRepository _restaurantRepository;

    public GetGameEventsByFiltersQueryHandler(IMapper mapper, IGameEventRepository gameEventRepository, ICityRepository cityRepository, IBoardGameRepository gameRepository, IRestaurantRepository restaurantRepository)
    {
        _mapper = mapper;
        _gameEventRepository = gameEventRepository;
        _cityRepository = cityRepository;
        _gameRepository = gameRepository;
        _restaurantRepository = restaurantRepository;
    }
    public async Task<List<GameEventsByFiltersVm>> Handle(GetGameEventsByFiltersQuery request, CancellationToken cancellationToken)
    {
        var nameEvents = await _gameEventRepository.GetGameEventsByFilters(request);
        
        var result = _mapper.Map<List<GameEventsByFiltersVm>>(nameEvents);

        return result;
    }

}
