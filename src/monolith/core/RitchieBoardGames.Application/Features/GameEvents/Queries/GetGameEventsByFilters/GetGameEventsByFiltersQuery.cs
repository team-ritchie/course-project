using MediatR;

namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsByFilters;

public class GetGameEventsByFiltersQuery : IRequest<List<GameEventsByFiltersVm>>
{
    public int? CityId { get; set; }
    public string? Date { get; set; }
    public int? GameId { get; set; }
    public int? RestaurantId { get; set; }
}

