namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsByFilters;

public class GameEventsByFiltersVm
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
}
