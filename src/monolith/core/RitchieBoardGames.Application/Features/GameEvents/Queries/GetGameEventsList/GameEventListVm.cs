namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsList;

public class GameEventListVm
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
}
