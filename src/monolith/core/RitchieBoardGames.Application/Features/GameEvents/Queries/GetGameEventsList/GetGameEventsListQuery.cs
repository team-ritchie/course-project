using MediatR;

namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsList;

public class GetGameEventsListQuery : IRequest<List<GameEventListVm>>
{

}
