using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsList;

public class GetGameEventsListQueryHandler : IRequestHandler<GetGameEventsListQuery, List<GameEventListVm>>
{
    private readonly IMapper _mapper;
    private readonly IGameEventRepository _gameEventRepository;

    public GetGameEventsListQueryHandler(IMapper mapper, IGameEventRepository gameEventRepository)
    {
        _mapper = mapper;
        _gameEventRepository = gameEventRepository;
    }
    public async Task<List<GameEventListVm>> Handle(GetGameEventsListQuery request, CancellationToken cancellationToken)
    {
        var allGameEvents = (await _gameEventRepository.ListAllAsync()).OrderByDescending(t => t.TimeStart);
        
        var result = _mapper.Map<List<GameEventListVm>>(allGameEvents);

        return result;
    }
}
