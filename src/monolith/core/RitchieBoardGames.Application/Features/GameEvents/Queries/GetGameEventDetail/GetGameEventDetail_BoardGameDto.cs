namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventDetail;

public class GetGameEventDetail_BoardGameDto
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
}
