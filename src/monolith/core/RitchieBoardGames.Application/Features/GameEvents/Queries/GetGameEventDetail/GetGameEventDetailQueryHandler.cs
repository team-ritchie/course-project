using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventDetail;

public class GetGameEventDetailQueryHandler : IRequestHandler<GetGameEventDetailQuery, GameEventDetailVm>
{
    private readonly IMapper _mapper;
    private readonly IGameEventRepository _gameEventRepository;
    private readonly IRestaurantRepository _restaurantRepository;
    private readonly IBoardGameRepository _gameRepository;

    public GetGameEventDetailQueryHandler(IMapper mapper, IGameEventRepository gameEventRepository,
        IRestaurantRepository restaurantRepository,
        IBoardGameRepository gameRepository)
    {
        _mapper = mapper;
        _gameEventRepository = gameEventRepository;
        _restaurantRepository = restaurantRepository;
        _gameRepository = gameRepository;
    }

    public async Task<GameEventDetailVm> Handle(GetGameEventDetailQuery request, CancellationToken cancellationToken)
    {
        var gameEvent = await _gameEventRepository.GetByIdAsync(request.Id);
        gameEvent = await LoadFKData(gameEvent);
        var gameDetailDto = _mapper.Map<GameEventDetailVm>(gameEvent);
        return gameDetailDto;
    }

    private async Task<GameEvent?> LoadFKData(GameEvent? row)
    {
        if (row != null) 
        {
            if (row.BoardGameId > 0 && row.BoardGame == null)
                row.BoardGame = await _gameRepository.GetByIdAsync((int)row.BoardGameId);

            if (row.RestaurantId > 0 && row.Restaurant == null)
                row.Restaurant = await _restaurantRepository.GetByIdAsync(row.RestaurantId);
        }

        return row; 
    } 
}
