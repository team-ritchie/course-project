
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventDetail;

public class GetGameEventDetail_RestaurantDto
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public City City { get; set; } = null!;
    public string FullAddress { get; set; } = null!;
    public string Phone { get; set; } = null!;
    public short OpenHour { get; set; }
    public short ClosingHour { get; set; }
}
