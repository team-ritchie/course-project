namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventDetail;

public class GetGameEventDetail_PersonDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Phone { get; set; } = string.Empty;
    public string Email { get; set; } = null!;
    public DateTime Birthday { get; set; }

}
