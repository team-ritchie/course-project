using MediatR;

namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventDetail;

public class GetGameEventDetailQuery : IRequest<GameEventDetailVm>
{
    public int Id { get; set; }
}
