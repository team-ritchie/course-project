namespace RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventDetail;

public class GameEventDetailVm
{
    public int Id { get; set; }
    public string Name { get; set; } = String.Empty;
    public DateTime TimeStart { get; set; }
    public int MaxPlayersCount { get; set; }
    public int MinPlayersCount { get; set; }
    public int MaxAge { get; set; }
    public int MinAge { get; set; }
    public string Note { get; set; } = string.Empty;
    public GetGameEventDetail_BoardGameDto Game { get; set; } = null!;
    public GetGameEventDetail_RestaurantDto Restaurant { get; set; } = null!;
    public List<GetGameEventDetail_PersonDto> Persons { get; set; } = new();
    public int NumberOfPersons => Persons.Count;
}
