using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGamesList;

public class GetBoardGamesListQueryHandler : IRequestHandler<GetBoardGamesListQuery, List<BoardGameListVm>>
{
    private readonly IMapper _mapper;
    private readonly IAsyncRepository<BoardGame> _gameRepository;

    public GetBoardGamesListQueryHandler(IMapper mapper, IAsyncRepository<BoardGame> gameRepository)
    {
        _mapper = mapper;
        _gameRepository = gameRepository;
    }
    public async Task<List<BoardGameListVm>> Handle(GetBoardGamesListQuery request, CancellationToken cancellationToken)
    {
        var allGames = (await _gameRepository.ListAllAsync()).OrderBy(g => g.Name);
        return _mapper.Map<List<BoardGameListVm>>(allGames);
    }
}
