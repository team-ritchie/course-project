using MediatR;

namespace RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGamesList;

public class GetBoardGamesListQuery : IRequest<List<BoardGameListVm>>
{

}
