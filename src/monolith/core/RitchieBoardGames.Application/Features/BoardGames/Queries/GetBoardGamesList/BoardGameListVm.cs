namespace RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGamesList;

/// <summary>
/// Настольная игра, элемент возвращаемого списка
/// </summary>
public class BoardGameListVm
{
    /// <summary>
    /// Id настольной игры
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Название настольной игры
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    /// Минимальное количество игроков
    /// </summary>
    public int? MinPersons { get; set; }

    /// <summary>
    /// Максимальное количество игроков
    /// </summary>
    public int? MaxPersons { get; set; }

    /// <summary>
    /// Минимальная продолжительность игры
    /// </summary>
    public int? MinDuration { get; set; }

    /// <summary>
    /// Максимальная продолжительность игры
    /// </summary>
    public int? MaxDuration { get; set; }

    /// <summary>
    /// Ссылка на изображение игры
    /// </summary>
    public string ImageUri { get; set; } = string.Empty;
}
