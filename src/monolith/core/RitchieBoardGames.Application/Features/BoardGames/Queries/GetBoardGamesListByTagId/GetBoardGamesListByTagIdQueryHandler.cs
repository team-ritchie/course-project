using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGamesList;

namespace RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGamesListByTagId;

public class GetBoardGamesListByTagIdQueryHandler
    : IRequestHandler<GetBoardGamesListByTagIdQuery, List<BoardGameListVm>>
{
    private IMapper _mapper;
    private IBoardGameRepository _boardGameRepository;

    public async Task<List<BoardGameListVm>> Handle(GetBoardGamesListByTagIdQuery request, CancellationToken cancellationToken)
    {
        var boardGamesByTagId = (await _boardGameRepository.ListAllByTagIdAsync(request.TagId));
        return _mapper.Map<List<BoardGameListVm>>(boardGamesByTagId);
    }

    public GetBoardGamesListByTagIdQueryHandler(IMapper mapper, IBoardGameRepository boardGameRepository)
    {
        _mapper = mapper;
        _boardGameRepository = boardGameRepository;
    }
}
