using MediatR;
using RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGamesList;

namespace RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGamesListByTagId;

public class GetBoardGamesListByTagIdQuery : IRequest<List<BoardGameListVm>>
{
    public int TagId { get; set; }
}
