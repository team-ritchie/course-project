using MediatR;

namespace RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGameDetail;

public class GetBoardGameDetailQuery : IRequest<BoardGameDetailVm>
{
    public int Id { get; set; }
}
