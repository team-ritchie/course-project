namespace RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGameDetail;

public class TagDto
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
}
