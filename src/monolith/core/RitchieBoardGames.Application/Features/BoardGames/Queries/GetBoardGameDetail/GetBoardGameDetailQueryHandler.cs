using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGameDetail;

public class GetBoardGameDetailQueryHandler : IRequestHandler<GetBoardGameDetailQuery, BoardGameDetailVm>
{
    private readonly IMapper _mapper;
    private readonly IAsyncRepository<BoardGame> _gameRepository;
    private readonly IAsyncRepository<Tag> _tagRepository;

    public GetBoardGameDetailQueryHandler(IMapper mapper, IAsyncRepository<BoardGame> gameRepository,
        IAsyncRepository<Tag> tagRepository)
    {
        _mapper = mapper;
        _gameRepository = gameRepository;
        _tagRepository = tagRepository;
    }

    public async Task<BoardGameDetailVm> Handle(GetBoardGameDetailQuery request, CancellationToken cancellationToken)
    {
        var game = await _gameRepository.GetByIdAsync(request.Id);
        var gameDetailDto = _mapper.Map<BoardGameDetailVm>(game);

        // TODO: add events and tags

        return gameDetailDto;

    }
}
