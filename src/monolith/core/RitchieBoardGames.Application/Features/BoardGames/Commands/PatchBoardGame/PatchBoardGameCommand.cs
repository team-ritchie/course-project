using MediatR;
using Microsoft.AspNetCore.JsonPatch;

namespace RitchieBoardGames.Application.Features.BoardGames.Commands.PatchBoardGame;

public class PatchBoardGameCommand : IRequest
{
    public int Id { get; set; }
    public JsonPatchDocument<BoardGameForPatchDto> PatchDocument { get; set; } = null!;
}
