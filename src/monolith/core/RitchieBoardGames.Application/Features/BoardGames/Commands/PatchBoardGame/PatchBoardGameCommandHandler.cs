using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Exceptions;

namespace RitchieBoardGames.Application.Features.BoardGames.Commands.PatchBoardGame;

public class PatchBoardGameCommandHandler : IRequestHandler<PatchBoardGameCommand>
{
    private readonly IMapper _mapper;
    private readonly IBoardGameRepository _gameRepository;

    public PatchBoardGameCommandHandler(IMapper mapper, IBoardGameRepository gameRepository)
    {
        _mapper = mapper;
        _gameRepository = gameRepository;
    }

    public async Task<Unit> Handle(PatchBoardGameCommand request, CancellationToken cancellationToken)
    {
        var gameInRepository = await _gameRepository.GetByIdAsync(request.Id);

        if (gameInRepository is null)
            throw new NotFoundException("Игра с id", request.Id);

        var gameToPatch = _mapper.Map<BoardGameForPatchDto>(gameInRepository);

        request.PatchDocument.ApplyTo(gameToPatch);

        // TODO: Add validator
        //if (false) // Validation Fails
        //{
        //    throw new BadRequestException("Некоторые из новых значений не проходят валидацию");
        //}

        _mapper.Map(gameToPatch, gameInRepository);

        await _gameRepository.UpdateAsync(gameInRepository);

        return Unit.Value;
    }
}
