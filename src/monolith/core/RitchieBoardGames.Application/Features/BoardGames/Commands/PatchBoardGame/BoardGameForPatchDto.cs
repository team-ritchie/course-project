namespace RitchieBoardGames.Application.Features.BoardGames.Commands.PatchBoardGame;

public class BoardGameForPatchDto
{
    public string? Name { get; set; }
    public string? Description { get; set; }
    public int? MinPersons { get; set; }
    public int? MaxPersons { get; set; }
    public int? MinAge { get; set; }
    public int? MaxAge { get; set; }
    public int? MinDuration { get; set; }
    public int? MaxDuration { get; set; }
}
