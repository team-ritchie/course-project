using FluentValidation;
using RitchieBoardGames.Application.Contracts.Persistence;

namespace RitchieBoardGames.Application.Features.BoardGames.Commands.CreateBoardGame;

public class CreateBoardGameCommandValidator : AbstractValidator<CreateBoardGameCommand>
{
    private readonly IBoardGameRepository _gameRepository;

    public CreateBoardGameCommandValidator(IBoardGameRepository boardGameRepository)
    {

        _gameRepository = boardGameRepository;

        RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage("Название игры не должно быть пустым");

        RuleFor(x => x.MinPersons)
            .NotEmpty()
            .WithMessage("Должно быть указано минимальное число игроков");

        RuleFor(x => x.MaxPersons)
            .NotEmpty()
            .WithMessage("Должно быть указано максимальное количество игроков");

        RuleFor(x => x.MinAge)
            .NotEmpty()
            .WithMessage("Должен быть указан возраст, с которого можно играть в данную игру");

        RuleFor(g => g).MustAsync(GameNameUnique).WithMessage("Название игры должно быть уникальным");
    }

    private async Task<bool> GameNameUnique(CreateBoardGameCommand request, CancellationToken token)
    {
        return !await _gameRepository.IsNameExist(request.Name);
    }
}
