using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.BoardGames.Commands.CreateBoardGame;

public class CreateBoardGameCommandHandler : IRequestHandler<CreateBoardGameCommand, int>
{
    private readonly IMapper _mapper;
    private readonly IBoardGameRepository _gameRepository;

    public CreateBoardGameCommandHandler(IMapper mapper, IBoardGameRepository gameRepository)
    {
        _mapper = mapper;
        _gameRepository = gameRepository;
    }

    public async Task<int> Handle(CreateBoardGameCommand request, CancellationToken cancellationToken)
    {
        var validator = new CreateBoardGameCommandValidator(_gameRepository);
        var validationResult = await validator.ValidateAsync(request);

        if (validationResult.Errors.Count > 0)
            throw new Exceptions.ValidationException(validationResult);

        var game = _mapper.Map<BoardGame>(request);

        game = await _gameRepository.AddAsync(game);

        return game.Id;
    }
}
