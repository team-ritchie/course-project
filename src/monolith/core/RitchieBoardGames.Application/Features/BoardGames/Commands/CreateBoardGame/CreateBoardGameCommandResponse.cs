using RitchieBoardGames.Application.Responses;

namespace RitchieBoardGames.Application.Features.BoardGames.Commands.CreateBoardGame;

public class CreateBoardGameCommandResponse : BaseResponse
{
    public CreateBoardGameCommandResponse(CreateBoardGameDto game) : base()
    {
        Game = game;
    }

    public CreateBoardGameDto Game { get; set; }
}
