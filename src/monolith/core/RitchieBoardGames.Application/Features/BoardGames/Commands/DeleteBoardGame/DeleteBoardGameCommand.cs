using MediatR;

namespace RitchieBoardGames.Application.Features.BoardGames.Commands.DeleteBoardGame;

public class DeleteBoardGameCommand : IRequest
{
    public int Id { get; set; }
}
