using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;

namespace RitchieBoardGames.Application.Features.BoardGames.Commands.DeleteBoardGame;

public class DeleteBoardGameCommandHandler : IRequestHandler<DeleteBoardGameCommand>
{
    private readonly IBoardGameRepository _gameRepository;

    public DeleteBoardGameCommandHandler(IBoardGameRepository gameRepository)
    {
        _gameRepository = gameRepository;
    }

    public async Task<Unit> Handle(DeleteBoardGameCommand request, CancellationToken cancellationToken)
    {
        var gameToDelete = await _gameRepository.GetByIdAsync(request.Id);

        await _gameRepository.DeleteAsync(gameToDelete);

        return Unit.Value;
    }
}
