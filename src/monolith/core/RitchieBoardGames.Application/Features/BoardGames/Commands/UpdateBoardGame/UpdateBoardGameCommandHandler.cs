using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Exceptions;

namespace RitchieBoardGames.Application.Features.BoardGames.Commands.UpdateBoardGame;

public class UpdateBoardGameCommandHandler : IRequestHandler<UpdateBoardGameCommand>
{
    private readonly IMapper _mapper;
    private readonly IBoardGameRepository _gameRepository;

    public UpdateBoardGameCommandHandler(IMapper mapper, IBoardGameRepository gameRepository)
    {
        _mapper = mapper;
        _gameRepository = gameRepository;
    }

    public async Task<Unit> Handle(UpdateBoardGameCommand request, CancellationToken cancellationToken)
    {
        var gameToUpdate = await _gameRepository.GetByIdAsync(request.Id);

        if (gameToUpdate is null)
            throw new NotFoundException("Игра с id", request.Id);

        _mapper.Map(request.BoardGame, gameToUpdate);

        // TODO: validation

        await _gameRepository.UpdateAsync(gameToUpdate);

        return Unit.Value;
    }
}
