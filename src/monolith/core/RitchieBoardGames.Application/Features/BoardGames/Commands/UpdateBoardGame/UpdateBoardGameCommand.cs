using MediatR;

namespace RitchieBoardGames.Application.Features.BoardGames.Commands.UpdateBoardGame;

public class UpdateBoardGameCommand : IRequest
{
    public int Id { get; set; }
    public BoardGameForUpdateDto BoardGame { get; set; } = null!;

}
