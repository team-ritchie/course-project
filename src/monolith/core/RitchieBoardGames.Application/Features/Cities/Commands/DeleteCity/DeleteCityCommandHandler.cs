using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.Cities.Commands.DeleteCity;

public class DeleteCityCommandHandler : IRequestHandler<DeleteCityCommand>
{
    private readonly IAsyncRepository<City> _cityRepository;

    public DeleteCityCommandHandler(IAsyncRepository<City> cityRepository)
    {
        _cityRepository = cityRepository;
    }

    public async Task<Unit> Handle(DeleteCityCommand request, CancellationToken cancellationToken)
    {
        var cityToDelete = await _cityRepository.GetByIdAsync(request.Id);
        
        await _cityRepository.DeleteAsync(cityToDelete);
        
        return Unit.Value;
    }
}
