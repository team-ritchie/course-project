using MediatR;

namespace RitchieBoardGames.Application.Features.Cities.Commands.AddCity;

public class AddCityCommand : IRequest<int>
{
    public string Name { get; set; } = null!;
}
