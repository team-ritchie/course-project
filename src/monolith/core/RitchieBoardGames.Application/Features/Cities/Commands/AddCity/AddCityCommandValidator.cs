using FluentValidation;
using RitchieBoardGames.Application.Contracts.Persistence;

namespace RitchieBoardGames.Application.Features.Cities.Commands.AddCity;

internal class AddCityCommandValidator : AbstractValidator<AddCityCommand>
{
    private readonly ICityRepository _cityRepository;

    public AddCityCommandValidator(ICityRepository cityRepository)
    {
        _cityRepository = cityRepository;

        RuleFor(c => c.Name).NotEmpty().WithMessage("Имя города не должно быть пустым");

        RuleFor(e => e).MustAsync(CityNameUnique).WithMessage("Название города должно быть уникальным");
    }

    private async Task<bool> CityNameUnique(AddCityCommand e, CancellationToken token)
    {
        return !(await _cityRepository.IsNameUnique(e.Name));
    }

}
