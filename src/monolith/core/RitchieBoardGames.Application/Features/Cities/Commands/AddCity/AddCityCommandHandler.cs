using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Exceptions;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.Cities.Commands.AddCity;

public class AddCityCommandHandler : IRequestHandler<AddCityCommand, int>
{
    private readonly ICityRepository _cityRepository;
    private readonly IMapper _mapper;

    public AddCityCommandHandler(ICityRepository cityRepository, IMapper mapper)
    {
        _cityRepository = cityRepository;
        _mapper = mapper;
    }
    public async Task<int> Handle(AddCityCommand request, CancellationToken cancellationToken)
    {
        var city = _mapper.Map<City>(request);

        var validator = new AddCityCommandValidator(_cityRepository);
        var validationResult = await validator.ValidateAsync(request);

        if (validationResult.Errors.Count > 0)
            throw new ValidationException(validationResult);

        var result = await _cityRepository.AddAsync(city);

        return result.Id;
    }
}
