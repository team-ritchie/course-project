using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.Cities.Queries.GetCitiesList;

public class GetCitiesListQueryHandler : IRequestHandler<GetCitiesListQuery, List<CityListVm>>
{
    private readonly IMapper _mapper;
    private readonly IAsyncRepository<City> _cityRepository;

    public GetCitiesListQueryHandler(IMapper mapper, IAsyncRepository<City> cityRepository)
    {
        _mapper = mapper;
        _cityRepository = cityRepository;
    }

    public async Task<List<CityListVm>> Handle(GetCitiesListQuery request, CancellationToken cancellationToken)
    {
        var allCities = (await _cityRepository.ListAllAsync()).OrderBy(c => c.Name);
        return _mapper.Map<List<CityListVm>>(allCities);
    }
}
