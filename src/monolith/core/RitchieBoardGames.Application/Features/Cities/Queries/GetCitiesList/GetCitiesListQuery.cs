using MediatR;

namespace RitchieBoardGames.Application.Features.Cities.Queries.GetCitiesList;

public class GetCitiesListQuery : IRequest<List<CityListVm>>
{

}
