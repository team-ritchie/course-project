namespace RitchieBoardGames.Application.Features.Cities.Queries.GetCitiesList;

public class CityListVm
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
}
