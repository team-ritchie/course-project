using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.Tags.Queries.GetTagsList;

public class GetTagsListQueryHandler : IRequestHandler<GetTagsListQuery, List<TagListVm>>
{
    private readonly IMapper _mapper;
    private readonly IAsyncRepository<Tag> _tagRepository;

    public GetTagsListQueryHandler(IMapper mapper, IAsyncRepository<Tag> tagRepository)
    {
        _mapper = mapper;
        _tagRepository = tagRepository;
    }
    public async Task<List<TagListVm>> Handle(GetTagsListQuery request, CancellationToken cancellationToken)
    {
        var allTags = (await _tagRepository.ListAllAsync()).OrderBy(t => t.Name);
        return _mapper.Map<List<TagListVm>>(allTags);
    }
}
