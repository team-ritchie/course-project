namespace RitchieBoardGames.Application.Features.Tags.Queries.GetTagsList;

public class TagListVm
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
}
