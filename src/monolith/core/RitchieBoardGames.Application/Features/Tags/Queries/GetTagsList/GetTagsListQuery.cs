using MediatR;

namespace RitchieBoardGames.Application.Features.Tags.Queries.GetTagsList;

public class GetTagsListQuery : IRequest<List<TagListVm>>
{

}
