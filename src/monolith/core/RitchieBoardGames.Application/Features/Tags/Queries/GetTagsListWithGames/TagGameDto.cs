namespace RitchieBoardGames.Application.Features.Tags.Queries.GetTagsListWithGames;

public class TagGameDto
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string? Description { get; set; }
    public int? MinPersons { get; set; }
    public int? MaxPersons { get; set; }
    public int? MinAge { get; set; }
    public int? MaxAge { get; set; }
    public int? MinDuration { get; set; }
    public int? MaxDuration { get; set; }
    public string ImageUri { get; set; } = string.Empty;
}
