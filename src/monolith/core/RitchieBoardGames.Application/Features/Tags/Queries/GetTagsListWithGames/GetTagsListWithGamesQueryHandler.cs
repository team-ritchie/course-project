using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;

namespace RitchieBoardGames.Application.Features.Tags.Queries.GetTagsListWithGames;

public class GetTagsListWithGamesQueryHandler : IRequestHandler<GetTagsListWithGamesQuery, List<TagGameListVm>>
{
    private readonly IMapper _mapper;
    private readonly ITagRepository _tagRepository;

    public GetTagsListWithGamesQueryHandler(IMapper mapper, ITagRepository tagRepository)
    {
        _mapper = mapper;
        _tagRepository = tagRepository;
    }
    public async Task<List<TagGameListVm>> Handle(GetTagsListWithGamesQuery request, CancellationToken cancellationToken)
    {
        var list = await _tagRepository.GetTagsWithGames(request.IncludeTrendingInfo);
        return _mapper.Map<List<TagGameListVm>>(list);
    }
}
