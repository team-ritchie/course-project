namespace RitchieBoardGames.Application.Features.Tags.Queries.GetTagsListWithGames;

public class TagGameListVm
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public ICollection<TagGameDto> Games { get; set; } = default!;
}
