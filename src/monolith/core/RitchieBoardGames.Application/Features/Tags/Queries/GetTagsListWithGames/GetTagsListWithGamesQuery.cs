using MediatR;

namespace RitchieBoardGames.Application.Features.Tags.Queries.GetTagsListWithGames;

public class GetTagsListWithGamesQuery : IRequest<List<TagGameListVm>>
{
    public bool IncludeTrendingInfo { get; set; }
}
