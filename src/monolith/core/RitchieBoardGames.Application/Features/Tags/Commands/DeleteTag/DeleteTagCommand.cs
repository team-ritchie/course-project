using MediatR;

namespace RitchieBoardGames.Application.Features.Tags.Commands.DeleteTag
{
    public class DeleteTagCommand : IRequest
    {
        public int Id { get; set; }
    }
}
