using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.Tags.Commands.DeleteTag;

public class DeleteTagCommandHandler : IRequestHandler<DeleteTagCommand>
{
    private readonly IMapper _mapper;
    private readonly IAsyncRepository<Tag> _tagRepository;

    public DeleteTagCommandHandler(IMapper mapper, IAsyncRepository<Tag> tagRepository)
    {
        _mapper = mapper;
        _tagRepository = tagRepository;
    }

    public async Task<Unit> Handle(DeleteTagCommand request, CancellationToken cancellationToken)
    {
        var tagToDelete = await _tagRepository.GetByIdAsync(request.Id);

        await _tagRepository.DeleteAsync(tagToDelete);

        return Unit.Value;
    }
}
