using MediatR;

namespace RitchieBoardGames.Application.Features.Tags.Commands.UpdateTag;

public class UpdateTagCommand : IRequest
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
}
