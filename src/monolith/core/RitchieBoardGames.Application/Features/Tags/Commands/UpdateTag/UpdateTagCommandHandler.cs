using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RitchieBoardGames.Application.Features.Tags.Commands.UpdateTag
{
    public class UpdateTagCommandHandler : IRequestHandler<UpdateTagCommand>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Tag> _tagRepository;

        public UpdateTagCommandHandler(IMapper mapper, IAsyncRepository<Tag> tagRepository)
        {
            _mapper = mapper;
            _tagRepository = tagRepository;
        }
        public async Task<Unit> Handle(UpdateTagCommand request, CancellationToken cancellationToken)
        {
            var tagToUpdate = await _tagRepository.GetByIdAsync(request.Id);

            _mapper.Map(request, tagToUpdate, typeof(UpdateTagCommand), typeof(Tag));

            return Unit.Value;
        }
    }
}
