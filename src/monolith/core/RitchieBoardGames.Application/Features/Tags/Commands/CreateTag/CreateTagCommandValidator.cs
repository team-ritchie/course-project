using FluentValidation;
using RitchieBoardGames.Application.Contracts.Persistence;

namespace RitchieBoardGames.Application.Features.Tags.Commands.CreateTag;

public class CreateTagCommandValidator : AbstractValidator<CreateTagCommand>
{
    private readonly ITagRepository _tagRepository;

    public CreateTagCommandValidator(ITagRepository tagRepository)
    {
        _tagRepository = tagRepository;

        RuleFor(p=>p.Name).NotEmpty().WithMessage("Имя тега обязательно")
            .NotNull()
            .MaximumLength(50).WithMessage("Имя тега слишком длинное, должно быть не более 50 символов.");

        RuleFor(e=>e).MustAsync(TagNameUnique);
    }

    private async Task<bool> TagNameUnique(CreateTagCommand e, CancellationToken token)
    {
        return !(await _tagRepository.IsNameUnique(e.Name));
    }
}
