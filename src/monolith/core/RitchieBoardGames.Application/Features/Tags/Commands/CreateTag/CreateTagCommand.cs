using MediatR;

namespace RitchieBoardGames.Application.Features.Tags.Commands.CreateTag;

public class CreateTagCommand : IRequest<int>
{
    public string Name { get; set; } = null!;
}
