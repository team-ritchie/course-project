using AutoMapper;
using MediatR;
using RitchieBoardGames.Application.Contracts.Infrastructure;
using RitchieBoardGames.Application.Contracts.Persistence;
using RitchieBoardGames.Application.Models;
using RitchieBoardGames.Application.Models.Mail;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Features.Tags.Commands.CreateTag;

public class CreateTagCommandHandler : IRequestHandler<CreateTagCommand, int>
{
    private readonly IMapper _mapper;
    private readonly ITagRepository _tagRepository;
    private readonly IEmailService _emailService;

    public CreateTagCommandHandler(IMapper mapper, ITagRepository tagRepository, IEmailService emailService)
    {
        _mapper = mapper;
        _tagRepository = tagRepository;
        _emailService = emailService;
    }

    public async Task<int> Handle(CreateTagCommand request, CancellationToken cancellationToken)
    {
        var validator = new CreateTagCommandValidator(_tagRepository);
        var validationResult = await validator.ValidateAsync(request);

        if (validationResult.Errors.Count > 0)
            throw new Exceptions.ValidationException(validationResult);

        var tag = _mapper.Map<Tag>(request);

        tag = await _tagRepository.AddAsync(tag);

        var email = new Email
        {
            To = "arigatory@gmail.com",
            Body = $"A new tag was created: {request}",
            Subject = "A new tag was created"
        };

        try
        {
            await _emailService.SendEmailAsync(email);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            //TODO: ошибка отправки не должна останавливать работу API, нужно просто отправить в лог
        }

        return tag.Id;
    }
}
