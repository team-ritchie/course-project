using AutoMapper;
using RitchieBoardGames.Application.Features.BoardGames.Commands.CreateBoardGame;
using RitchieBoardGames.Application.Features.BoardGames.Commands.PatchBoardGame;
using RitchieBoardGames.Application.Features.BoardGames.Commands.UpdateBoardGame;
using RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGameDetail;
using RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGamesList;
using RitchieBoardGames.Application.Features.Cities.Commands.AddCity;
using RitchieBoardGames.Application.Features.Cities.Queries.GetCitiesList;
using RitchieBoardGames.Application.Features.Persons.Commands.AddPerson;
using RitchieBoardGames.Application.Features.Persons.Commands.UpdatePerson;
using RitchieBoardGames.Application.Features.Persons.Queries.GetPersonDetail;
using RitchieBoardGames.Application.Features.Persons.Queries.GetPersonsList;
using RitchieBoardGames.Application.Features.Tags.Commands.CreateTag;
using RitchieBoardGames.Application.Features.Tags.Commands.UpdateTag;
using RitchieBoardGames.Application.Features.Tags.Queries.GetTagsList;
using RitchieBoardGames.Application.Features.Tags.Queries.GetTagsListWithGames;
using RitchieBoardGames.Application.Features.GameEvents.Commands.CreateGameEvent;
using RitchieBoardGames.Application.Features.GameEvents.Commands.UpdateGameEvent;
using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsList;
using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventDetail;
using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsByFilters;
using RitchieBoardGames.Domain.Entities;

namespace RitchieBoardGames.Application.Profiles;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<City, CityListVm>().ReverseMap();
        CreateMap<City, AddCityCommand>().ReverseMap();

        CreateMap<BoardGame, BoardGameListVm>().ReverseMap();
        CreateMap<BoardGame, BoardGameDetailVm>().ReverseMap();
        CreateMap<BoardGame, CreateBoardGameCommand>().ReverseMap();
        CreateMap<BoardGame, BoardGameForUpdateDto>().ReverseMap();
        CreateMap<BoardGame, BoardGameForPatchDto>().ReverseMap();
        CreateMap<BoardGame, GetGameEventDetail_BoardGameDto>().ReverseMap();

        CreateMap<Tag, TagDto>().ReverseMap();
        CreateMap<Tag, TagListVm>().ReverseMap();
        CreateMap<Tag, TagGameListVm>().ReverseMap();
        CreateMap<Tag, TagGameDto>().ReverseMap();
        CreateMap<Tag, CreateTagCommand>().ReverseMap();
        CreateMap<Tag, UpdateTagCommand>().ReverseMap();

        CreateMap<Person, AddPersonCommand>().ReverseMap();
        CreateMap<Person, PersonListVm>().ReverseMap();
        CreateMap<Person, PersonDetailVm>().ReverseMap();
        CreateMap<Person, UpdatePersonDto>().ReverseMap();

        CreateMap<Restaurant, RestaurantDto>().ReverseMap();

        CreateMap<GameEvent, Features.Persons.Queries.GetPersonDetail.GameEventDto>().ReverseMap();
        CreateMap<GameEvent, GameEventListVm>().ReverseMap();
        CreateMap<GameEvent, GameEventDetailVm>().ReverseMap();
        CreateMap<GameEvent, GameEventsByFiltersVm>().ReverseMap();
        CreateMap<GameEvent, GameEventsByFiltersVm>().ReverseMap();
        CreateMap<GameEvent, CreateGameEventCommand>().ReverseMap();
        CreateMap<GameEvent, GameEventForUpdateDto>().ReverseMap();
        
        CreateMap<Person, GetGameEventDetail_PersonDto>().ReverseMap();
        CreateMap<Restaurant, GetGameEventDetail_RestaurantDto>().ReverseMap();
      
    }
}
