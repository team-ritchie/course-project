using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Application.Features.Cities.Commands.AddCity;
using RitchieBoardGames.Application.Features.Cities.Commands.DeleteCity;
using RitchieBoardGames.Application.Features.Cities.Queries.GetCitiesList;

namespace RitchieBoardGames.Api.Controllers;

[Authorize]
[Route("api/cities")]
[ApiController]
public class CitiesController : ControllerBase
{
    private readonly IMediator _mediator;

    public CitiesController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<List<CityListVm>>> GetAllCities()
    {
        return await _mediator.Send(new GetCitiesListQuery());
    }

    [HttpPost]
    public async Task<ActionResult> AddCity(AddCityCommand addCityCommand)
    {
        var assignedId = await _mediator.Send(addCityCommand);
        return NoContent();

        //return CreatedAtRoute("Имя метода", assignedId);

    }

    [HttpDelete("{cityId}")]
    public async Task<ActionResult> DeleteCity(int cityId)
    {
        await _mediator.Send(new DeleteCityCommand { Id = cityId });
        return NoContent();
    }
}
