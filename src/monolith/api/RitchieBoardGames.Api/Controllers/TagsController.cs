using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Application.Features.Tags.Commands.CreateTag;
using RitchieBoardGames.Application.Features.Tags.Queries.GetTagsList;

namespace RitchieBoardGames.Api.Controllers;

[Authorize]
[Route("api/tags")]
[ApiController]
public class TagsController : ControllerBase
{
    private readonly IMediator _mediator;

    public TagsController(IMediator mediatR)
    {
        _mediator = mediatR;
    }

    [HttpGet]
    public async Task<ActionResult<List<TagListVm>>> GetAllTags()
    {
        var dtos = await _mediator.Send(new GetTagsListQuery());
        return Ok(dtos);
    }

    [HttpPost]
    public async Task<ActionResult> AddTag(CreateTagCommand createTagCommand)
    {
        var response = await _mediator.Send(createTagCommand);
        return NoContent();
    }
}
