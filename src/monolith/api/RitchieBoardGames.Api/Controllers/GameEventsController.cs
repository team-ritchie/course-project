using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Application.Features.GameEvents.Commands.CreateGameEvent;
using RitchieBoardGames.Application.Features.GameEvents.Commands.DeleteGameEvent;
using RitchieBoardGames.Application.Features.GameEvents.Commands.UpdateGameEvent;
using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventDetail;
using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsByFilters;
using RitchieBoardGames.Application.Features.GameEvents.Queries.GetGameEventsList;

namespace RitchieBoardGames.Api.Controllers;


[Authorize]
[Route("api/game_events")]
[ApiController]
public class GameEventsController : ControllerBase
{
    private readonly IMediator _mediatR;

    public GameEventsController(IMediator mediatR)
    {
        _mediatR = mediatR;
    }

    [HttpGet]
    public async Task<ActionResult<List<GameEventListVm>>> GetAllGameEvents()
    {
        var dtos = await _mediatR.Send(new GetGameEventsListQuery());
        return Ok(dtos);
    }

    [HttpGet("by_filters")]
    public async Task<ActionResult<List<GameEventsByFiltersVm>>> GetGameEventsByFilters([FromQuery] GetGameEventsByFiltersQuery request)
    {
        var dtos = await _mediatR.Send(request);
        return Ok(dtos);
    }

    [HttpGet("{gameEventId}")]
    public async Task<ActionResult<GameEventDetailVm>> GetGameEventDetail(int gameEventId)
    {
        var gameEventDetail = await _mediatR.Send(new GetGameEventDetailQuery { Id = gameEventId });
        return Ok(gameEventDetail);
    }

    [HttpPost]
    public async Task<ActionResult> CreateGameEvent([FromBody] CreateGameEventCommand createGameEventCommand)
    {
        var response = await _mediatR.Send(createGameEventCommand);
        return Ok(response);
    }

    [HttpPut("{gameEventId}")]
    public async Task<ActionResult> UpdateGameEvent(int gameEventId, [FromBody] GameEventForUpdateDto gameEventFroUpdate)
    {
        var response = await _mediatR.Send(new UpdateGameEventCommand
        {
            Id = gameEventId,
            GameEvent = gameEventFroUpdate
        });

        return Ok(response);
    }

    [HttpDelete("{gameEventId}")]
    public async Task<ActionResult> DeleteGameEvent(int gameEventId)
    {
        var response = await _mediatR.Send(new DeleteGameEventCommand { Id = gameEventId });
        return NoContent();
    }

}
