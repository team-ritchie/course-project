using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Application.Features.BoardGames.Commands.CreateBoardGame;
using RitchieBoardGames.Application.Features.BoardGames.Commands.DeleteBoardGame;
using RitchieBoardGames.Application.Features.BoardGames.Commands.PatchBoardGame;
using RitchieBoardGames.Application.Features.BoardGames.Commands.UpdateBoardGame;
using RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGameDetail;
using RitchieBoardGames.Application.Features.BoardGames.Queries.GetBoardGamesList;

namespace RitchieBoardGames.Api.Controllers;

[Authorize]
[Route("api/boardgames")]
[ApiController]
public class BoardGamesController : ControllerBase
{
    private readonly IMediator _mediator;

    public BoardGamesController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Возвращает список всех имеющихся настольных игр
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<BoardGameListVm>>> GetAllBoardGames()
    {
        var dtos = await _mediator.Send(new GetBoardGamesListQuery());
        return Ok(dtos);
    }

    /// <summary>
    /// Возвращает информацию о настольной игре с заданным id
    /// </summary>
    /// <param name="boardGameId">Id настольной игры</param>
    /// <returns>ActionResult типа BoardGameDetailVm</returns>
    /// <response code="200">Возвращает запрашиваемую настольную игру</response>
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesDefaultResponseType]
    [HttpGet("{boardGameId}")]
    public async Task<ActionResult<BoardGameDetailVm>> GetBoardGameDetail(int boardGameId)
    {
        var boardGameDetail = await _mediator.Send(new GetBoardGameDetailQuery { Id = boardGameId });
        return Ok(boardGameDetail);
    }

    /// <summary>
    /// Создает настольную игру
    /// </summary>
    /// <param name="createBoardGameCommand"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult> CreateBoardGame(CreateBoardGameCommand createBoardGameCommand)
    {
        await _mediator.Send(createBoardGameCommand);
        // TODO: redirect info
        return NoContent();
    }

    /// <summary>
    /// Полностью заменяет настольную игру с заданным id
    /// </summary>
    /// <param name="boardGameId"></param>
    /// <param name="boardGameForUpdate"></param>
    /// <returns></returns>
    [HttpPut("{boardGameId}")]
    public async Task<ActionResult> UpdateBoardGame(int boardGameId, BoardGameForUpdateDto boardGameForUpdate)
    {
        await _mediator.Send(new UpdateBoardGameCommand
        {
            Id = boardGameId,
            BoardGame = boardGameForUpdate
        });

        return NoContent();
    }

    /// <summary>
    /// Обновляет отдельные поля настольной игры, используя JsonPatchDocument
    /// </summary>
    /// <param name="boardGameId">Id наскольной игры, которую нужно обновить</param>
    /// <param name="patchDocument">Набор комманд, которые нужно применить к
    /// настольной игре</param>
    /// <returns>ActionResult</returns>
    /// <remarks>
    /// Пример запроса (данный запрос изменяет название настольной игры **Name**):   
    /// 
    ///     PATCH /boardgames/id
    ///     [
    ///         {
    ///             "op": "replace",
    ///             "path": "/name",
    ///             "value": "New Monopoly"
    ///         }
    ///     ]
    /// </remarks>
    [HttpPatch("{boardGameId}")]
    public async Task<ActionResult> PartiallyUpdateBoardGame(int boardGameId,
        JsonPatchDocument<BoardGameForPatchDto> patchDocument)
    {
        await _mediator.Send(new PatchBoardGameCommand { Id = boardGameId, PatchDocument = patchDocument });
        return NoContent();
    }

    /// <summary>
    /// Удаляет настольную игру по id
    /// </summary>
    /// <param name="boardGameId"></param>
    /// <returns></returns>
    [HttpDelete("{boardGameId}")]
    public async Task<ActionResult> DeleteBoardGame(int boardGameId)
    {
        await _mediator.Send(new DeleteBoardGameCommand { Id = boardGameId });
        return NoContent();
    }


}
