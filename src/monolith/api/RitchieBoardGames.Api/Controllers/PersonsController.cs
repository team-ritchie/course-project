using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Application.Features.Persons.Commands.AddPerson;
using RitchieBoardGames.Application.Features.Persons.Commands.DeletePerson;
using RitchieBoardGames.Application.Features.Persons.Commands.UpdatePerson;
using RitchieBoardGames.Application.Features.Persons.Queries.GetPersonDetail;
using RitchieBoardGames.Application.Features.Persons.Queries.GetPersonsList;

namespace RitchieBoardGames.Api.Controllers;

[Authorize]
[Route("api/persons")]
[ApiController]
public class PersonsController : ControllerBase
{
    private readonly IMediator _mediator;

    public PersonsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<List<PersonListVm>>> GetAllPersons()
    {
        return await _mediator.Send(new GetPersonsListQuery());
    }
    /// <summary>
    /// Возвращает информацию о игроке по Id
    /// </summary>
    /// <param name="personId"></param>
    /// <returns></returns>
    [HttpGet("{personId}")]
    public async Task<ActionResult<PersonDetailVm>> GetPersonDetail(int personId)
    {
        var personDetail = await _mediator.Send(new GetPersonDetailQuery { Id = personId });
        return Ok(personDetail);
    }

    [HttpPost]
    public async Task<ActionResult> AddPerson(AddPersonCommand addPersonCommand)
    {
        var id = await _mediator.Send(addPersonCommand);
        return NoContent();
    }

    [HttpPut(("{personId}"))]
    public async Task<ActionResult> UpdatePerson(UpdatePersonDto personDto, int personId)
    {
        await _mediator.Send(new UpdatePersonCommand(personDto, personId));
        return NoContent();
    }

    [HttpDelete("{personId}")]
    public async Task<ActionResult> DeletePerson(int personId)
    {
        await _mediator.Send(new DeletePersonCommand { Id = personId });
        return NoContent();
    }
}
