using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Application.Features.Restaurants.Commands.AddRestaurant;
using RitchieBoardGames.Application.Features.Restaurants.Commands.DeleteRestaurant;
using RitchieBoardGames.Application.Features.Restaurants.Commands.PatchRestaurant;
using RitchieBoardGames.Application.Features.Restaurants.Commands.UpdateRestaurant;
using RitchieBoardGames.Application.Features.Restaurants.Queries.GetRestaurantDetail;
using RitchieBoardGames.Application.Features.Restaurants.Queries.GetRestaurantList;

namespace RitchieBoardGames.Api.Controllers;

[Authorize]
[Route("api/restaurants")]
[ApiController]
public class RestaurantsController : ControllerBase
{
    private readonly IMediator _mediator;

    public RestaurantsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Возвращает список всех имеющихся ресторанов
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<RestaurantListVm>>> GetAllRestaurants()
    {
        var dtos = await _mediator.Send(new GetRestaurantsListQuery());
        return Ok(dtos);
    }

    /// <summary>
    /// Возвращает информацию о ресторане с заданным id
    /// </summary>
    /// <param name="restaurantId">Id ресторана</param>
    /// <returns>ActionResult типа RestaurantDetailVm</returns>
    /// <response code="200">Возвращает запрашиваемый ресторан</response>
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesDefaultResponseType]
    [HttpGet("{restaurantId}")]
    public async Task<ActionResult<RestaurantDetailVm>> GetRestaurantDetail(int restaurantId)
    {
        var restaurantDetail = await _mediator.Send(new GetRestaurantDetailQuery { Id = restaurantId });
        return Ok(restaurantDetail);
    }

    /// <summary>
    /// Добавляет ресторан
    /// </summary>
    /// <param name="addRestaurantCommand">Поля добавляемого ресторана</param>
    /// <returns>ActionResult</returns>
    /// <response code="204">Возвращает No Content</response>
    [HttpPost]
    public async Task<ActionResult> AddRestaurant(AddRestaurantCommand addRestaurantCommand)
    {
        await _mediator.Send(addRestaurantCommand);
        // TODO: redirect info
        return NoContent();
    }

    /// <summary>
    /// Полностью заменяет рестаран с заданным id
    /// </summary>
    /// <param name="restaurantId"></param>
    /// <param name="restaurantForUpdateDto"></param>
    /// <returns>ActionResult</returns>
    /// <response code="204">Возвращает No Content</response>
    [HttpPut("{restaurantId}")]
    public async Task<ActionResult> UpdateRestaurant(int restaurantId, RestaurantForUpdateDto restaurantForUpdateDto)
    {
        await _mediator.Send(new UpdateRestaurantCommand
        {
            Id = restaurantId,
            RestaurantForUpdate = restaurantForUpdateDto
        });

        return NoContent();
    }

    /// <summary>
    /// Обновляет отдельные поля ресторана, используя JsonPatchDocument
    /// </summary>
    /// <param name="restaurantId">Id ресторана, который нужно обновить</param>
    /// <param name="patchDocument">Набор комманд, которые нужно применить к
    /// ресторану</param>
    /// <returns>ActionResult</returns>
    /// <remarks>
    /// Пример запроса (данный запрос изменяет название ресторана **Name**):   
    /// 
    ///     PATCH /restaurants/id
    ///     [
    ///         {
    ///             "op": "replace",
    ///             "path": "/name",
    ///             "value": "Новый ресторан"
    ///         }
    ///     ]
    /// </remarks>
    /// <response code="204">Возвращает No Content</response>
    [HttpPatch("{restaurantId}")]
    public async Task<ActionResult> PartiallyUpdateBoardGame(int restaurantId,
        JsonPatchDocument<RestaurantForPatch> patchDocument)
    {
        await _mediator.Send(new PatchRestaurantCommand { Id = restaurantId, PatchDocument = patchDocument });
        return NoContent();
    }

    /// <summary>
    /// Удаляет ресторан по id
    /// </summary>
    /// <param name="restaurantId"></param>
    /// <returns>ActionResult</returns>
    /// <response code="204">Возвращает No Content</response>
    [HttpDelete("{restaurantId}")]
    public async Task<ActionResult> DeleteRestaurant(int restaurantId)
    {
        await _mediator.Send(new DeleteRestaurantCommand { Id = restaurantId });
        return NoContent();
    }
}
