using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using RitchieBoardGames.Application.Features.Addresses.Commands.AddAddress;
using RitchieBoardGames.Application.Features.Addresses.Commands.DeleteAddress;
using RitchieBoardGames.Application.Features.Addresses.Commands.PatchAddress;
using RitchieBoardGames.Application.Features.Addresses.Commands.UpdateAddress;
using RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressDetail;
using RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressesList;
using RitchieBoardGames.Application.Features.Addresses.Queries.GetAddressesListByCity;

namespace RitchieBoardGames.Api.Controllers;

[Authorize]
[Route("api/addresses")]
[ApiController]
public class AddressesController : ControllerBase
{
    private readonly IMediator _mediator;

    public AddressesController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Возвращает список всех имеющихся адресов
    /// </summary>
    /// <returns>Возвращает ActionResult List из AddressListVm </returns>
    [HttpGet]
    public async Task<ActionResult<List<AddressListByCityVm>>> GetAllAddresses()
    {
        var dtos = await _mediator.Send(new GetAddressesListQuery());
        return Ok(dtos);
    }

    /// <summary>
    /// Возвращает информацию об адресе с заданным id
    /// </summary>
    /// <param name="addressId">Id адреса</param>
    /// <returns>ActionResult типа AddressDetailVm</returns>
    /// <response code="200">Возвращает запрашиваемый адрес</response>
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesDefaultResponseType]
    [HttpGet("{AddressId}")]
    public async Task<ActionResult<AddressDetailVm>> GetAddressDetail(int addressId)
    {
        var AddressDetail = await _mediator.Send(new GetAddressDetailQuery { Id = addressId });
        return Ok(AddressDetail);
    }

    /// <summary>
    /// Добавляет адрес
    /// </summary>
    /// <param name="addAddressCommand">Поля добавляемого адреса</param>
    /// <returns>ActionResult</returns>
    /// <response code="204">Возвращает No Content</response>
    [HttpPost]
    public async Task<ActionResult> AddAddress(AddAddressCommand addAddressCommand)
    {
        await _mediator.Send(addAddressCommand);
        // TODO: redirect info
        return NoContent();
    }

    /// <summary>
    /// Полностью заменяет адрес с заданным id
    /// </summary>
    /// <param name="addressId"></param>
    /// <param name="addressForUpdateDto"></param>
    /// <returns>ActionResult</returns>
    /// <response code="204">Возвращает No Content</response>
    [HttpPut("{addressId}")]
    public async Task<ActionResult> UpdateAddress(int addressId, AddressForUpdateDto addressForUpdateDto)
    {
        await _mediator.Send(new UpdateAddressCommand
        {
            Id = addressId,
            AddressForUpdate = addressForUpdateDto
        });

        return NoContent();
    }

    /// <summary>
    /// Обновляет отдельные поля адреса, используя JsonPatchDocument
    /// </summary>
    /// <param name="addressId">Id адреса, который нужно обновить</param>
    /// <param name="patchDocument">Набор комманд, которые нужно применить к
    /// адресу</param>
    /// <returns>ActionResult</returns>
    /// <remarks>
    /// Пример запроса (данный запрос изменяет полный адрес **FullAddress**):   
    /// 
    ///     PATCH /Addresses/id
    ///     [
    ///         {
    ///             "op": "replace",
    ///             "path": "/FullAddress",
    ///             "value": "Новый адрес"
    ///         }
    ///     ]
    /// </remarks>
    /// <response code="204">Возвращает No Content</response>
    [HttpPatch("{addressId}")]
    public async Task<ActionResult> PartiallyUpdateBoardGame(int addressId,
        JsonPatchDocument<AddressForPatchDto> patchDocument)
    {
        await _mediator.Send(new PatchAddressCommand { Id = addressId, PatchDocument = patchDocument });
        return NoContent();
    }

    /// <summary>
    /// Удаляет адрес по id
    /// </summary>
    /// <param name="addressId"></param>
    /// <returns>ActionResult</returns>
    /// <response code="204">Возвращает No Content</response>
    [HttpDelete("{addressId}")]
    public async Task<ActionResult> DeleteAddress(int addressId)
    {
        await _mediator.Send(new DeleteAddressCommand { Id = addressId });
        return NoContent();
    }
}
