using Microsoft.OpenApi.Models;
using System.Reflection;

namespace RitchieBoardGames.Api.Middleware;

public static class MiddlewareExtensions
{
    public static IApplicationBuilder UseCustomExceptionHandler(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<ExceptionHandlerMiddleware>();
    }

    public static void AddCustomSwaggerGen(this IServiceCollection services)
    {
        services.AddSwaggerGen(setupAction =>
        {
            setupAction.SwaggerDoc("RitchieBoardGamesSpecification", new()
            {
                Title = "Board Games API",
                Version = "1",
                Description = "С помощью данного API можно управлять организацией настольных" +
                " игр в ресторане в выбранном городе",
                Contact = new()
                {
                    Name = "Сайт разработчиков",
                    Url = new Uri("https://gitlab.com/team-ritchie/course-project/-/tree/dev")
                },
                License = new()
                {
                    Name = "Лицензия MIT",
                    Url = new Uri("https://opensource.org/licenses/MIT")
                }
            });
            var xmlCommentsFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlCommentFullPath = Path.Combine(
                AppContext.BaseDirectory, xmlCommentsFile);
            setupAction.IncludeXmlComments(xmlCommentFullPath);
            setupAction.IncludeXmlComments(Path.Combine(
                AppContext.BaseDirectory, "RitchieBoardGames.Application.xml"));

            setupAction.AddSecurityDefinition("RitchieBoardGamesAuth", new OpenApiSecurityScheme()
            {
                Name = "Bearer",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.Http,
                Scheme = "Bearer",
                Description = "Введите валидный токен, чтобы получить доступ к API"
            });

            setupAction.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "RitchieBoardGamesAuth"
                        },
                        Name = "Bearer",
                        In = ParameterLocation.Header
                    }, new List<string>()
                }
            });
        });
    }

}
