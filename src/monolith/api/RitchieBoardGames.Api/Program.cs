using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using RitchieBoardGames.Api.Middleware;
using RitchieBoardGames.Application;
using RitchieBoardGames.Identity;
using RitchieBoardGames.Infrastructure;
using RitchieBoardGames.Persistence;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddApplicationServices();
builder.Services.AddInfrastructureServices(builder.Configuration);
builder.Services.AddPersistenceServices(builder.Configuration);
builder.Services.AddIdentityServices(builder.Configuration);

builder.Services.AddControllers(configure =>
{
    // чтобы возвращать ошибку 406, если подается плохой формат (не json)
    configure.ReturnHttpNotAcceptable = true;

    configure.Filters.Add(
        new ProducesAttribute("application/json"));

    configure.Filters.Add(
        new ConsumesAttribute("application/json"));

    configure.Filters.Add(
        new ProducesResponseTypeAttribute(StatusCodes.Status400BadRequest));

    configure.Filters.Add(
        new ProducesResponseTypeAttribute(StatusCodes.Status406NotAcceptable));

    configure.Filters.Add(
        new ProducesResponseTypeAttribute(StatusCodes.Status500InternalServerError));
}).AddNewtonsoftJson();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddCustomSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddPolicy("Open", builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseStaticFiles();
    app.UseSwagger();
    app.UseSwaggerUI(setupAction =>
    {
        setupAction.SwaggerEndpoint("RitchieBoardGamesSpecification/swagger.json", "Board Games API");
        setupAction.RoutePrefix = "swagger";

        setupAction.InjectStylesheet("/assets/custom-ui.css");
    });
}

app.UseCustomExceptionHandler();

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseCors("Open");

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }
