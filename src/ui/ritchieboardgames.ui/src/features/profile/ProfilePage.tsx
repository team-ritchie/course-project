import style from './profilePage.module.css';
import { observer } from 'mobx-react-lite';
import LoadingComponent from '../../app/layout/LoadingComponent';
import { useStore } from '../../app/api/stores/store';

export default observer(function ProfilePage() {
    const { gameEventStore } = useStore();
    if (gameEventStore.loadingInitial)
    return <LoadingComponent />;
  return (
    <div className={style.gameEventDashboard}>
        <h1>Profile</h1>
    </div>
  );
});
