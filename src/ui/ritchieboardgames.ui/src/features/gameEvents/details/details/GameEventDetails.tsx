import { observer } from 'mobx-react-lite';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useStore } from '../../../../app/api/stores/store';
import LoadingComponent from '../../../../app/layout/LoadingComponent';
import Button from '../../../../components/Button/button';
import GameEventCard from '../card/GameEventCard';
import GameEventAttendeeItem from '../gameEventAttendeeItem/GameEventAttendeeItem';

export default observer(function GameEventDetails() {
  const { gameEventStore } = useStore();
  const {
    selectedGameEvent: gameEvent,
    loadGameEvent,
    loadingInitial,
  } = gameEventStore;
  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    if (id) loadGameEvent(id);
  }, [id, loadGameEvent]);

  if (loadingInitial || !gameEvent) return <LoadingComponent />;

  return (
    <>
      <Button to="/gameEvents" content="<" width="75px" color="blue" />
      <GameEventCard gameEvent={gameEvent} />
      <GameEventAttendeeItem gameEvent={gameEvent} />
    </>
  );
});
