import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import MyHeader from '../../../../app/common/Header/header';
import MyText from '../../../../app/common/Text/text';
import { GameEvent } from '../../../../app/models/gameEvent';
import styles from './gameEventAttendeeItem.module.scss';

interface Props {
  gameEvent: GameEvent;
}

export default observer(function GameEventAttendeeItem({
  gameEvent: { attendees, host },
}: Props) {
  if (!attendees) return null;
  return (
    <div>
      <MyHeader content={`Всего участников: ${attendees.length}`} />
      <ul className={styles.list}>
        {attendees.map((attendee) => (
          <li key={attendee.username}>
            <Link to={`/profiles/${attendee.username}`} className={styles.link}>
              {attendee.username === host?.username && (
                <MyText color='red' content="Организатор" className={styles.host} />
              )}
              <img
                src={attendee.image || '/assets/user.png'}
                alt="Profile"
                className={styles.img}
              />
              {attendee.displayName}
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
});
