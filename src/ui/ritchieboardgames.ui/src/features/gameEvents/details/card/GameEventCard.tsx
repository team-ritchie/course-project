import rublePic from '../../../../img/ruble-sign.svg';
import mapPic from '../../../../img/map-marked.svg';
import userPic from '../../../../img/user-alt.svg';
import calendarPic from '../../../../img/calendar.svg';
import style from './gameEventCard.module.css';
import { observer } from 'mobx-react-lite';
import { GameEvent } from '../../../../app/models/gameEvent';
import Button from '../../../../components/Button/button';
import { format } from 'date-fns';
import { useStore } from '../../../../app/api/stores/store';
import MyText from '../../../../app/common/Text/text';

interface Props {
  gameEvent: GameEvent;
}

const GameEventCard = ({ gameEvent }: Props) => {
  const {
    gameEventStore: { updateAttendance, loading, cancelGameEventToggle },
  } = useStore();

  return (
    <div className={style.card}>
      {gameEvent.isCancelled && <MyText content="Отменено" color='red'/>}
      <h2 className={style.cardTitle}>{gameEvent.title}</h2>
      <div className={style.firstRow}>
        <p className={style.description}>{gameEvent.description}</p>
        <div className={style.boardGame}>
          <img
            src={`/assets/boardGameImages/${gameEvent.boardGame}.jpg`}
            alt="Game box"
            className={style.cardImg}
          />
          <p className={style.boardGameName}></p>
        </div>
      </div>
      <div className={style.cardContent}>
        <ul className={style.informationItemsList}>
          <li className={style.informationItem}>
            <img
              src={calendarPic}
              alt="Icon"
              className={style.informationIcon}
            />

            <span className={style.informationText}>
              {format(gameEvent.date!, 'dd MMMM yyyy H:mm')}
            </span>
          </li>

          <li className={style.informationItem}>
            <img src={userPic} alt="Icon" className={style.informationIcon} />
            <span className={style.informationText}>
              {gameEvent.minPlayersCount}/{gameEvent.maxPlayersCount}
            </span>
          </li>
          <li className={style.informationItem}>
            <img src={mapPic} alt="Icon" className={style.informationIcon} />
            <span className={style.informationText}>{gameEvent.venue}</span>
          </li>
          <li className={style.informationItem}>
            <img src={rublePic} alt="Icon" className={style.informationIcon} />
            <span className={style.informationText}>Бесплатно</span>
          </li>
        </ul>
      </div>
      <div className={style.buttons}>
        {gameEvent.isHost ? (
          <>
            <Button
              content={gameEvent.isCancelled ? 'Активировать' : 'Отменить'}
              color={gameEvent.isCancelled ? 'green' : 'red'}
              width="200px"
              onClick={cancelGameEventToggle}
              loading={loading}
            />
            <Button
              to={`/manage/${gameEvent.id}`}
              content="Управлять"
              color="green"
              width="200px"
              disabled={gameEvent.isCancelled}
            />
          </>
        ) : gameEvent.isGoing ? (
          <Button
            content="Отменить участие"
            color="red"
            width="200px"
            onClick={updateAttendance}
            loading={loading}
          />
        ) : (
          <Button
            content="Присоединиться"
            color="green"
            width="250px"
            onClick={updateAttendance}
            loading={loading}
            disabled={gameEvent.isCancelled}
          />
        )}
      </div>
    </div>
  );
};

export default observer(GameEventCard);
