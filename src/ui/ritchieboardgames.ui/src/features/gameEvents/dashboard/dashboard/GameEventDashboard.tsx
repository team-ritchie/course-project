import style from './gameEventDashboard.module.css';
import { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../../../app/api/stores/store';
import LoadingComponent from '../../../../app/layout/LoadingComponent';
import GameEventList from '../list/GameEventList';
import GameEventFilters from '../gameEventFilters/GameEventFilters';
import Button from '../../../../components/Button/button';

export default observer(function GameEventDashboard() {
  const { gameEventStore } = useStore();
  const { loadGameEvents, gameEventRegistry } = gameEventStore;

  useEffect(() => {
    if (gameEventRegistry.size <= 1) {
      loadGameEvents();
    }
  }, [gameEventRegistry.size, loadGameEvents]);

  if (gameEventStore.loadingInitial) return <LoadingComponent />;
  return (
    <div className={style.gameEventDashboard}>
      <div className={style.topMenu}>
        <h1 className={style.title}>Игры поблизости</h1>
        <Button color="green" to="/createGameEvent" content="+" width='50px' />
      </div>
      <GameEventFilters />
      <GameEventList />
    </div>
  );
});
