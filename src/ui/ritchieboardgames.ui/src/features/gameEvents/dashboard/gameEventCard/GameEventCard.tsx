import rublePic from './img/ruble-sign.svg';
import mapPic from './img/map-marked.svg';
import userPic from './img/user-alt.svg';
import calendarPic from './img/calendar.svg';
import { GameEvent } from '../../../../app/models/gameEvent';
import styles from './gameEventCard.module.css';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import GameEventAttendeeItem from './GameEventAttendeeItem/GameEventAttendeeItem';
import Button from '../../../../components/Button/button';
import MyText from '../../../../app/common/Text/text';

interface Props {
  gameEvent: GameEvent;
}

const GameEventCard = ({ gameEvent }: Props) => {
  return (
    <div className={styles.card}>
      <h2 className={styles.cardTitle}>{gameEvent.title}</h2>
      {gameEvent.isHost && (
        <MyText
          className={styles.info}
          color="red"
          content={`Вы создали данное мероприятие`}
        />
      )}
      {gameEvent.isGoing && !gameEvent.isHost && (
        <MyText
          className={styles.info}
          color="blue"
          content={`Вы участвуете!`}
        />
      )}
      {gameEvent.isCancelled && <MyText content="Отменено" color="red" />}

      <div className={styles.cardContent}>
        <ul className={styles.informationItemsList}>
          <li className={styles.informationItem}>
            <img
              src={calendarPic}
              alt="Icon"
              className={styles.informationIcon}
            />
            <span className={styles.informationText}>
              {format(gameEvent.date!, 'dd MMMM yyyy H:mm')}
            </span>
          </li>

          <li className={styles.informationItem}>
            <img src={userPic} alt="Icon" className={styles.informationIcon} />
            <span className={styles.informationText}>
              {gameEvent.minPlayersCount}/{gameEvent.maxPlayersCount}
            </span>
          </li>
          <li className={styles.informationItem}>
            <img src={mapPic} alt="Icon" className={styles.informationIcon} />
            <span className={styles.informationText}>{gameEvent.venue}</span>
          </li>
          <li className={styles.informationItem}>
            <img src={rublePic} alt="Icon" className={styles.informationIcon} />
            <span className={styles.informationText}>Бесплатно</span>
          </li>
        </ul>
        <img
          src={`/assets/boardGameImages/${gameEvent.boardGame}.jpg`}
          alt="Game box"
          className={styles.cardImg}
        />
      </div>
      <GameEventAttendeeItem attendees={gameEvent.attendees!} />
      <Link to={`/gameEvents/${gameEvent.id}`} className={styles.link}>
        <Button color="green" content="Посмотреть" />
      </Link>
    </div>
  );
};

export default observer(GameEventCard);
