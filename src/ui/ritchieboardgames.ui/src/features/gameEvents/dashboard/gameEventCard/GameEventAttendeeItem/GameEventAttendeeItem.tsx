import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import { Profile } from '../../../../../app/models/profile';
import styles from './gameEventAttendeeItem.module.scss';

interface Props {
  attendees: Profile[];
}

export default observer(function GameEventAttendeeItem({ attendees }: Props) {
  return (
    <ul className={styles.list}>
      {attendees.map((attendee) => (
        <li key={attendee.username}>
          <Link to={`/profiles/${attendee.username}`}>
            <img
              src={attendee.image || '/assets/user.png'}
              alt="Profile"
              className={styles.img}
            />
          </Link>
        </li>
      ))}
    </ul>
  );
});
