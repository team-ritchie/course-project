// import { Calendar } from 'react-calendar';
import {  useState } from 'react';
import style from './gameEventFilters.module.css';

export default function ActivityFilters() {
  const [active, setActive] = useState('1');
  function onClick(e: React.MouseEvent) {
    setActive(e.currentTarget.id);
  }
  return (
    <div className={style.tabs}>
      <button
        id="1"
        className={`${style.tab} ${active === '1' && style.active}`}
        onClick={(e) => onClick(e)}
      >
        Все
      </button>
      <button
        id="2"
        className={`${style.tab} ${active === '2' && style.active}`}
        onClick={(e) => onClick(e)}
      >
        Созданы мной
      </button>
      <button
        id="3"
        className={`${style.tab} ${active === '3' && style.active}`}
        onClick={(e) => onClick(e)}
      >
        Я участвую
      </button>
    </div>
  );
}
