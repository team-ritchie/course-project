import { Formik, Form } from 'formik';
import { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useStore } from '../../../app/api/stores/store';
import { GameEventFormValues } from '../../../app/models/gameEvent';
import * as Yup from 'yup';
import MyTextInput from '../../../app/common/form/TextInput/textInput';
import Button from '../../../components/Button/button';
import styles from './gameEventForm.module.css';
import rublePic from '../../../img/ruble-sign.svg';
import mapPic from '../../../img/map-marked.svg';
import userPic from '../../../img/user-alt.svg';
import calendarPic from '../../../img/calendar.svg';
import MyTextArea from '../../../app/common/form/TextArea/textArea';
import MySelectInput from '../../../app/common/form/SelectInput/selectInput';
import { boardGameOptions } from '../../../app/common/options/boardGameOptions';
import MyDateInput from '../../../app/common/form/DateInput/dateInput';
import { v4 as uuid } from 'uuid';
import LoadingComponent from '../../../app/layout/LoadingComponent';

export default function GameEventForm() {
  const history = useHistory();
  const { gameEventStore } = useStore();
  const {
    createGameEvent,
    updateGameEvent,
    loadGameEvent,
    loadingInitial,
  } = gameEventStore;

  const { id } = useParams<{ id: string }>();

  const [gameEvent, setGameEvent] = useState<GameEventFormValues>(
    new GameEventFormValues()
  );

  useEffect(() => {
    if (id)
      loadGameEvent(id).then((gameEvent) =>
        setGameEvent(new GameEventFormValues(gameEvent))
      );
  }, [id, loadGameEvent]);

  function handleFormSubmit(gameEvent: GameEventFormValues) {
    if (!gameEvent.id) {
      let newGameEvent = {
        ...gameEvent,
        id: uuid(),
      };
      createGameEvent(newGameEvent).then(() =>
        history.push(`/gameEvents/${newGameEvent.id}`)
      );
    } else {
      updateGameEvent(gameEvent).then(() =>
        history.push(`/gameEvents/${gameEvent.id}`)
      );
    }
  }

  const validationSchema = Yup.object({
    title: Yup.string().required('Название мероприятия обязательно'),
    date: Yup.date().required('Дата и время обязательны').nullable(),
    description: Yup.string().required('Описание мероприятия обязательно'),
    venue: Yup.string().required('Место мероприятия обязательно'),
    minPlayersCount: Yup.number()
      .min(2)
      .required('Минимум участников 2')
      .nullable(),
    maxPlayersCount: Yup.number().max(10).required('Максимум участников 10'),
    boardGame: Yup.string().min(1).required('Настольная игра обязательня'),
  });

  if (loadingInitial)
    return <LoadingComponent content="Загрузка мероприятия..." />;

  return (
    <div>
      <h1 className={styles.title}>Управление мероприятием</h1>
      <Formik
        enableReinitialize
        initialValues={gameEvent}
        onSubmit={(values) => handleFormSubmit(values)}
        validationSchema={validationSchema}
      >
        {({ handleSubmit, isValid, isSubmitting, dirty }) => (
          <Form
            className={styles.form}
            onSubmit={handleSubmit}
            autoComplete="off"
          >
            <div className={styles.card}>
              <MyTextInput
                className={styles.cardTitle}
                name="title"
                placeholder="Название"
              />
              <div className={styles.firstRow}>
                <MyTextArea
                  rows={3}
                  className={styles.description}
                  name="description"
                  placeholder="Описание"
                />

                <div className={styles.boardGame}>
                  <MySelectInput
                    options={boardGameOptions}
                    placeholder="Наскольная игра"
                    name="boardGame"
                  />
                  <p className={styles.boardGameName}></p>
                </div>
              </div>
              <div className={styles.cardContent}>
                <ul className={styles.informationItemsList}>
                  <li className={styles.informationItem}>
                    <img
                      src={calendarPic}
                      alt="Icon"
                      className={styles.informationIcon}
                    />
                    <MyDateInput
                      placeholderText="Когда"
                      name="date"
                      showTimeSelect
                      timeCaption="time"
                      dateFormat="MMMM d, yyyy h:mm aa"
                    />
                  </li>

                  <li className={styles.informationItem}>
                    <img
                      src={userPic}
                      alt="Icon"
                      className={styles.informationIcon}
                    />

                    <span className={styles.informationText}>
                      <MyTextInput
                        placeholder="Минимально участников"
                        name="minPlayersCount"
                        className={styles.informationText}
                        width="30px"
                      />
                      /
                      <MyTextInput
                        placeholder="Максимально участников"
                        name="maxPlayersCount"
                        className={styles.informationText}
                        width="30px"
                      />
                    </span>
                  </li>

                  <li className={styles.informationItem}>
                    <img
                      src={mapPic}
                      alt="Icon"
                      className={styles.informationIcon}
                    />
                    <MyTextInput
                      placeholder="Место проведения"
                      name="venue"
                      className={styles.informationText}
                    />
                  </li>
                  <li className={styles.informationItem}>
                    <img
                      src={rublePic}
                      alt="Icon"
                      className={styles.informationIcon}
                    />
                    <span className={styles.informationText}>Бесплатно</span>
                  </li>
                </ul>
              </div>
              <div className={styles.buttons}>
                <Button
                  to="/gameEvents"
                  type="button"
                  content="Отмена"
                  color="red"
                />
                <Button
                  disabled={isSubmitting || !dirty || !isValid}
                  type="submit"
                  content="Сохранить"
                  color="green"
                  loading={isSubmitting}
                />
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}
