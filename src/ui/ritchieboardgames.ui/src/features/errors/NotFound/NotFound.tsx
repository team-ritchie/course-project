import Button from '../../../components/Button/button';
import style from './notFound.module.css'

export default function NotFound() {
  return (
    <>
      <div className={style.content}>
        <h1 className={style.text}>Ой,</h1>
        <h2 className={style.text}>тут ничего нет</h2>
      </div>
      <Button to='/gameEvents' content='К мероприятиям' color='green' />
    </>
  );
}
