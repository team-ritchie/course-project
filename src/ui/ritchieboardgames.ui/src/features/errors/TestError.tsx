import { Header, Segment } from 'semantic-ui-react';
import axios from 'axios';
import { useState } from 'react';
import ValidationErrors from './ValidationErrors';
import Button from '../../components/Button/button';

export default function TestErrors() {
  const baseUrl = 'http://localhost:4002/api/';
  const [errors, setErrors] = useState(null);

  function handleNotFound() {
    axios
      .get(baseUrl + 'buggy/not-found')
      .catch((err) => console.log(err.response));
  }

  function handleBadRequest() {
    axios
      .get(baseUrl + 'buggy/bad-request')
      .catch((err) => console.log(err.response));
  }

  function handleServerError() {
    axios
      .get(baseUrl + 'buggy/server-error')
      .catch((err) => console.log(err.response));
  }

  function handleUnauthorised() {
    axios
      .get(baseUrl + 'buggy/unauthorised')
      .catch((err) => console.log(err.response));
  }

  function handleBadGuid() {
    axios.get(baseUrl + 'gameEvents/notaguid').catch((err) => console.log(err));
  }

  function handleValidationError() {
    axios.post(baseUrl + 'gameEvents', {}).catch((err) => setErrors(err));
  }

  return (
    <>
      <Header as="h1" content="Тесты ошибок" />
      {errors && <ValidationErrors errors={errors} />}

      <Segment>
        <Button onClick={handleNotFound} content="Not Found" />
        <Button onClick={handleBadRequest} content="Bad Request" />
        <Button onClick={handleValidationError} content="Validation Error" />
        <Button onClick={handleServerError} content="Server Error" />
        <Button onClick={handleUnauthorised} content="Unauthorised" />
        <Button onClick={handleBadGuid} content="Bad Guid" />
      </Segment>
      
      <Button to="/gameEvents" color="green" content="К мероприятиям" />
    </>
  );
}
