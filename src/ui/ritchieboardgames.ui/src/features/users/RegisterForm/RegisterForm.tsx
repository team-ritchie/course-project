import { ErrorMessage, Form, Formik } from 'formik';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import { useStore } from '../../../app/api/stores/store';
import BigTextInput from '../../../app/common/form/BigTextInput/bigTextInput';
import Button from '../../../components/Button/button';
import Logo from '../../../components/Logo/logo';
import styles from './registerForm.module.css';
import * as Yup from 'yup';
import ValidationErrors from '../../errors/ValidationErrors';

export default observer(function RegisterForm() {
  const { userStore } = useStore();
  return (
    <div className={styles.login}>
      <Logo width={100} />
      <p className={styles.welcome}>
        Зарегистрируйтесь, чтобы участвовать и играх!
      </p>
      <Formik
        initialValues={{
          displayName: '',
          username: '',
          email: '',
          password: '',
          error: null,
        }}
        onSubmit={(v, { setErrors }) =>
          userStore
            .register(v)
            .catch((error) => setErrors({ error }))
        }
        
        validationSchema={Yup.object({
          displayName: Yup.string().required(),
          username: Yup.string().required(),
          email: Yup.string().email().required(),
          password: Yup.string().min(6).required(),
        })}
      >
        {({ handleSubmit, isSubmitting, errors, isValid, dirty }) => (
          <Form className='error' onSubmit={handleSubmit} autoComplete="off">
            <BigTextInput name="displayName" placeholder="Имя" />
            <BigTextInput name="username" placeholder="Логин" />
            <BigTextInput name="email" placeholder="Email" />
            <BigTextInput
              name="password"
              placeholder="Пароль"
              type="password"
            />
            <ErrorMessage
              name="error"
              render={() => <ValidationErrors errors={errors.error} />}
            />
            <a
              href="http://ya.ru"
              className={`${styles.forgotPasswordLink} ${styles.link}`}
            >
              Забыли пароль?
            </a>
            <Button
              loading={isSubmitting}
              color="green"
              content="Зарегистрироваться"
              type="submit"
              disabled={!isValid || !dirty || isSubmitting}
            />
          </Form>
        )}
      </Formik>
      <p className={`${styles.noAccountText}`}>
        <span>Уже есть аккаунт?</span>
        <Link
          to="/login"
          className={`${styles.registrationLink} ${styles.link}`}
        >
          Войти
        </Link>
      </p>
    </div>
  );
});
