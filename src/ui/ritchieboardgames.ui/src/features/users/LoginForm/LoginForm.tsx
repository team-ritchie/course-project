import { ErrorMessage, Form, Formik } from 'formik';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import { useStore } from '../../../app/api/stores/store';
import BigTextInput from '../../../app/common/form/BigTextInput/bigTextInput';
import Button from '../../../components/Button/button';
import Logo from '../../../components/Logo/logo';
import styles from './loginForm.module.css';

export default observer(function LoginForm() {
  const { userStore } = useStore();
  return (
    <div className={styles.login}>
      <Logo width={100} />
      <p className={styles.welcome}>
        Авторизируйтесь, чтобы участвовать и играх!
      </p>
      <Formik
        onSubmit={(v, { setErrors }) =>
          userStore
            .login(v)
            .catch((err) => setErrors({ error: 'Неверный email или пароль' }))
        }
        initialValues={{ email: '', password: '', error: null }}
      >
        {({ handleSubmit, isSubmitting, errors }) => (
          <Form onSubmit={handleSubmit} autoComplete="off">
            <BigTextInput name="email" placeholder="Email" />
            <BigTextInput
              name="password"
              placeholder="Пароль"
              type="password"
            />
            <ErrorMessage
              name='error' render={() => (
                <p className={styles.error}>{errors.error}</p>
                )} 
            />
            <a
              href="http://ya.ru"
              className={`${styles.forgotPasswordLink} ${styles.link}`}
            >
              Забыли пароль?
            </a>
            <Button
              loading={isSubmitting}
              color="green"
              content="Войти"
              type="submit"
            />
          </Form>
        )}
      </Formik>
      <p className={`${styles.noAccountText}`}>
        <span>Нет учетной записи?</span>
        <Link
          to="/register"
          className={`${styles.registrationLink} ${styles.link}`}
        >
          Регистрация
        </Link>
      </p>
    </div>
  );
});
