import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import { useStore } from '../../app/api/stores/store';
import Button from '../../components/Button/button';
import Logo from '../../components/Logo/logo';

export default observer(function HomePage() {
  const { userStore } = useStore();
  return (
    <Container
      style={{
        marginTop: '7em',
        display: 'flex',
        flexDirection: 'column',
        justifyItems: 'center',
        alignItems: 'center',
        gap: '2em',
      }}
    >
      <h1>Ritchie Board Games</h1>
      <Logo width={200} />
      {userStore.isLoggedIn ? (
        <>
          <h1>Добро пожаловать в Ritchie Board Games</h1>
          <h3>
            Смотреть все <Link to="/gameEvents">мероприятия</Link>
          </h3>
        </>
      ) : (
        <Button to="/login" content="Войти" />
      )}
    </Container>
  );
});
