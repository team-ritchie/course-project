import { Dimmer } from "semantic-ui-react";
import Logo from "../../components/Logo/logo";

interface Props {
    inverted?: boolean;
    content?: string;
}

export default function LoadingComponent({ inverted = true, content = 'Загрузка...' }: Props) {
    return <Dimmer active={true} inverted={inverted}>
        <div>
            <Logo width={100} rotate={true} />
        </div>
    </Dimmer>
}