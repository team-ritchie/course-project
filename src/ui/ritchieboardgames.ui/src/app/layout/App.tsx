import Header from '../../components/Header/header';
import style from './app.module.css';
import Footer from '../../components/Footer/footer';
import Container from '../../components/Container/container';
import { Route, Switch, useLocation } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import HomePage from '../../features/home/HomePage';
import GameEventDetails from '../../features/gameEvents/details/details/GameEventDetails';
import GameEventForm from '../../features/gameEvents/form/GameEventForm';
import ProfilePage from '../../features/profile/ProfilePage';
import TestErrors from '../../features/errors/TestError';
import ServerError from '../../features/errors/ServerError';
import NotFound from '../../features/errors/NotFound/NotFound';
import GameEventDashboard from '../../features/gameEvents/dashboard/dashboard/GameEventDashboard';
import { ToastContainer } from 'react-toastify';
import LoginForm from '../../features/users/LoginForm/LoginForm';
import { useStore } from '../api/stores/store';
import { useEffect } from 'react';
import LoadingComponent from './LoadingComponent';
import ModalContainer from '../common/modals/ModalContainer';
import RegisterForm from '../../features/users/RegisterForm/RegisterForm';

function App() {
  const location = useLocation();
  const { commonStore, userStore } = useStore();

  useEffect(() => {
    if (commonStore.token) {
      userStore.getUser().finally(() => commonStore.setAppLoaded());
    } else {
      commonStore.setAppLoaded();
    }
  }, [commonStore, userStore]);

  if (!commonStore.appLoaded)
    return <LoadingComponent content="Загрузка приложения..." />;

  return (
    <>
      <ToastContainer position="top-right" hideProgressBar />
      <ModalContainer />
      <Route exact path="/" component={HomePage} />
      <Route
        path={'/(.+)'}
        render={() => (
          <div className={style.app}>
            <Header />
            <Container>
              <Switch>
                <Route
                  exact
                  path="/gameEvents"
                  component={GameEventDashboard}
                />
                <Route
                  exact
                  path="/gameEvents/:id"
                  component={GameEventDetails}
                />
                <Route
                  key={location.key}
                  path={['/createGameEvent', '/manage/:id']}
                  component={GameEventForm}
                />
                <Route exact path="/profile" component={ProfilePage} />
                <Route path="/errors" component={TestErrors} />
                <Route path="/server-error" component={ServerError} />
                <Route path="/login" component={LoginForm} />
                <Route path="/register" component={RegisterForm} />
                <Route component={NotFound} />
              </Switch>
            </Container>
            <Footer />
          </div>
        )}
      />
    </>
  );
}

export default observer(App);
