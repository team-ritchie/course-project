import { createContext, useContext } from 'react';
import CommonStore from './commonStore';
import GameEventStore from './gameEventStore';
import ModalStore from './modalStore';
import UserStore from './userStore';

interface Store {
  gameEventStore: GameEventStore;
  commonStore: CommonStore;
  userStore: UserStore;
  modalStore: ModalStore;
}

export const store: Store = {
  gameEventStore: new GameEventStore(),
  commonStore: new CommonStore(),
  userStore: new UserStore(),
  modalStore: new ModalStore(),
};

export const StoreContext = createContext(store);

export function useStore() {
  return useContext(StoreContext);
}
