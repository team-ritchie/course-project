import { format } from 'date-fns';
import { makeAutoObservable, runInAction } from 'mobx';
import { GameEvent, GameEventFormValues } from '../../models/gameEvent';
import { Profile } from '../../models/profile';
import agent from '../agent';
import { store } from './store';

export default class GameEventStore {
  gameEventRegistry = new Map<string, GameEvent>();
  selectedGameEvent: GameEvent | undefined = undefined;
  editMode = false;
  loading = false;
  loadingInitial = false;

  constructor() {
    makeAutoObservable(this);
  }

  get gameEventsByDate() {
    return Array.from(this.gameEventRegistry.values()).sort(
      (a, b) => a.date!.getTime() - b.date!.getTime()
    );
  }

  get groupedGameEvents() {
    return Object.entries(
      this.gameEventsByDate.reduce((gameEvents, gameEvent) => {
        const date = format(gameEvent.date!, 'dd MMMM yyyy H:mm');
        gameEvents[date] = gameEvents[date]
          ? [...gameEvents[date], gameEvent]
          : [gameEvent];
        return gameEvents;
      }, {} as { [key: string]: GameEvent[] })
    );
  }

  loadGameEvents = async () => {
    this.loadingInitial = true;
    try {
      const gameEvents = await agent.GameEvents.list();
      gameEvents.forEach((gameEvent) => {
        this.setGameEvent(gameEvent);
      });
      this.setLoadingInitial(false);
    } catch (error) {
      console.log(error);
      this.setLoadingInitial(false);
    }
  };

  loadGameEvent = async (id: string) => {
    let gameEvent = this.getGameEvent(id);
    if (gameEvent) {
      this.selectedGameEvent = gameEvent;
      return gameEvent;
    } else {
      this.setLoadingInitial(true);
      try {
        gameEvent = await agent.GameEvents.details(id);
        this.setGameEvent(gameEvent);
        runInAction(() => {
          this.selectedGameEvent = gameEvent;
        });
        this.setLoadingInitial(false);
        return gameEvent;
      } catch (error) {
        console.log(error);
        this.setLoadingInitial(false);
      }
    }
  };

  private setGameEvent = (gameEvent: GameEvent) => {
    const user = store.userStore.user;
    if (user) {
      gameEvent.isGoing = gameEvent.attendees?.some(
        (a) => a.username === user.username
      );
      gameEvent.isHost = gameEvent.hostUsername === user.username;
      gameEvent.host = gameEvent.attendees?.find(
        (x) => x.username === gameEvent.hostUsername
      );
    }
    gameEvent.date = new Date(gameEvent.date!);
    this.gameEventRegistry.set(gameEvent.id, gameEvent);
  };

  private getGameEvent = (id: string) => {
    return this.gameEventRegistry.get(id);
  };

  setLoadingInitial = (state: boolean) => {
    this.loadingInitial = state;
  };

  createGameEvent = async (gameEvent: GameEventFormValues) => {
    const user = store.userStore.user;
    const attendee = new Profile(user!);
    try {
      await agent.GameEvents.create(gameEvent);
      const newGameEvent = new GameEvent(gameEvent);
      newGameEvent.hostUsername = user!.username;
      newGameEvent.attendees = [attendee];
      this.setGameEvent(newGameEvent);
      runInAction(() => {
        this.selectedGameEvent = newGameEvent;
      });
    } catch (error) {
      console.log(error);
    }
  };

  updateGameEvent = async (gameEvent: GameEventFormValues) => {
    try {
      await agent.GameEvents.update(gameEvent);
      runInAction(() => {
        if (gameEvent.id) {
          let updatedGameEvent = {
            ...this.getGameEvent(gameEvent.id),
            ...gameEvent,
          };
          this.gameEventRegistry.set(
            gameEvent.id,
            updatedGameEvent as GameEvent
          );
          this.selectedGameEvent = updatedGameEvent as GameEvent;
        }
      });
    } catch (error) {
      console.log(error);
      runInAction(() => {
        this.loading = false;
      });
    }
  };

  deleteGameEvent = async (id: string) => {
    this.loading = true;
    try {
      await agent.GameEvents.delete(id);
      runInAction(() => {
        this.gameEventRegistry.delete(id);
        this.loading = false;
      });
    } catch (error) {
      console.log(error);
      runInAction(() => {
        this.loading = false;
      });
    }
  };

  updateAttendance = async () => {
    const user = store.userStore.user;
    this.loading = true;
    try {
      await agent.GameEvents.attend(this.selectedGameEvent!.id);
      runInAction(() => {
        if (this.selectedGameEvent?.isGoing) {
          this.selectedGameEvent.attendees =
            this.selectedGameEvent.attendees?.filter(
              (a) => a.username !== user?.username
            );
          this.selectedGameEvent.isGoing = false;
        } else {
          const attendee = new Profile(user!);
          this.selectedGameEvent?.attendees?.push(attendee);
          this.selectedGameEvent!.isGoing = true;
        }
        this.gameEventRegistry.set(
          this.selectedGameEvent!.id,
          this.selectedGameEvent!
        );
      });
    } catch (error) {
      console.log(error);
    } finally {
      this.loading = false;
    }
  };

  cancelGameEventToggle = async () => {
    this.loading = true;
    try {
      await agent.GameEvents.attend(this.selectedGameEvent!.id);
      runInAction(() => {
        this.selectedGameEvent!.isCancelled =
          !this.selectedGameEvent?.isCancelled;
        this.gameEventRegistry.set(
          this.selectedGameEvent!.id,
          this.selectedGameEvent!
        );
      });
    } catch (error) {
      console.log(error);
    } finally {
      runInAction(() => (this.loading = false));
    }
  };
}
