import { Profile } from './profile';

export interface GameEvent {
  id: string;
  title: string;
  date: Date | null;
  description: string;
  city: string;
  venue: string;
  minPlayersCount: number;
  maxPlayersCount: number;
  minAge: number;
  maxAge: number;
  boardGame: string;
  hostUsername: string;
  isCancelled: boolean;
  isGoing: boolean;
  isHost?: boolean;
  host?: Profile;
  attendees: Profile[];
}

export class GameEvent implements GameEvent {
  constructor(init?: GameEventFormValues) {
    Object.assign(this, init);
  }
}

export class GameEventFormValues {
  id?: string = undefined;
  title: string = '';
  description: string = '';
  date: Date | null = null;
  city: string = 'Москва';
  venue: string = '';
  boardGame: string = '';

  minPlayersCount: number = 2;
  maxPlayersCount: number = 6;

  minAge: number = 10;
  maxAge: number = 99;

  constructor(gameEvent?: GameEvent) {
    if (gameEvent) {
      this.id = gameEvent.id;
      this.title = gameEvent.title;
      this.description = gameEvent.description;
      this.date = gameEvent.date;
      this.city = gameEvent.city;
      this.venue = gameEvent.city;
      this.boardGame = gameEvent.boardGame;
      this.minPlayersCount = gameEvent.minPlayersCount;
      this.maxPlayersCount = gameEvent.maxPlayersCount;
      this.minAge = gameEvent.minAge;
      this.maxAge = gameEvent.maxAge;
    }
  }
}
