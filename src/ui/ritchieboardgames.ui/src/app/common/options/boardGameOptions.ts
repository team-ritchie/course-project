export const boardGameOptions = [
  {text: 'Неудержимые единорожки', value: 'neuderzhimyeEdinorozhki'},
  {text: 'Манчкин', value: 'munchkin'},
  {text: 'Catan: Колонизаторы', value: 'catan'},
  {text: 'Монополия', value: 'monopoly'},
  {text: 'Мафия. Вся семья в сборе. Маски', value: 'mafiyaVsyaSemyaVSboreMaski'},
  {text: 'Dungeons & Dragons', value: 'DnD'},
]