import { useField } from 'formik';
import { Select } from 'semantic-ui-react';
import styles from './selectInput.module.css';

interface Props {
  placeholder: string;
  name: string;
  label?: string;
  className?: string;
  options: any;
}

export default function MySelectInput(props: Props) {
  const [field, meta, helpers] = useField(props.name);
  return (
    <div className={props.className}>
      <label>{props.label}</label>
      <Select
        clearable
        options={props.options}
        value={field.value || null}
        onChange={(e, d) => helpers.setValue(d.value)}
        onBlur={() => helpers.setTouched(true)}
        placeholder={props.placeholder}
        className={styles.select}
      />
      {meta.touched && meta.error ? (
        <p className={styles.error}>{meta.error}</p>
      ) : null}
    </div>
  );
}
