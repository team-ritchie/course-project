import { useField } from 'formik';
import styles from './textArea.module.css';

interface Props {
  placeholder: string;
  name: string;
  label?: string;
  className?: string;
  rows?: number;
}

export default function MyTextArea(props: Props) {
  const [field, meta] = useField(props.name);
  return (
    <div className={props.className}>
      <label></label>
      <textarea {...field} {...props} className={styles.input} />
      {meta.touched && meta.error ? (
        <p className={styles.error}>
          {meta.error}
        </p>
      ) : null}
    </div>
  );
}
