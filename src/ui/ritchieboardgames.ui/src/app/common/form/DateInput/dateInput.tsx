import { useField } from 'formik';
import styles from './dateInput.module.css';
import DatePicker, {ReactDatePickerProps} from 'react-datepicker';


export default function MyDateInput(props: Partial<ReactDatePickerProps>) {
  const [field, meta, helpers] = useField(props.name!);
  return (
    <div className={props.className}>
      <DatePicker
        className={styles.datePicker}
        {...field}
        {...props}
        selected={(field.value && new Date(field.value)) || null}
        onChange={(value) => helpers.setValue(value)}
      />
      {meta.touched && meta.error ? (
        <p className={styles.error}>{meta.error}</p>
      ) : null}
    </div>
  );
}
