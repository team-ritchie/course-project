import { useField } from 'formik';
import styles from './bigTextInput.module.css';

interface Props {
  placeholder: string;
  name: string;
  label?: string;
  className?: string;
  width?: string;
  type?: string;
}

export default function BigTextInput(props: Props) {
  const [field, meta] = useField(props.name);
  return (
    <div className={props.className}>
      <label>{props.label}</label>
      <input
        {...field}
        {...props}
        className={styles.input}
        style={{ width: props.width }}
      />
      {meta.touched && meta.error ? (
        <p className={styles.error}>{meta.error}</p>
      ) : null}
    </div>
  );
}
