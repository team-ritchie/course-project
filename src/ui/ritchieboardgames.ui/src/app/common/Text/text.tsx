import styles from './text.module.scss';

interface Props {
  content: string;
  color?: 'green' | 'blue'| 'red';
  size?: 'normal' | 'big' | 'medium' | 'small';
  className?: string;
}

export default function MyText(props: Props) {
  const color = props.color ? props.color : 'green';

  return (
    <p className={`${styles[color]}  ${styles.header} ${props.className}`}>{props.content}</p>
  );
}
