import styles from './header.module.scss';

interface Props {
  content: string;
  color?: 'green' | 'blue';
  size?: 'normal' | 'big' | 'medium' | 'small';
}

export default function MyHeader(props: Props) {
  const color = props.color ? props.color : 'green';

  return (
    <h1 className={`${styles[color]}  ${styles.header}`}>{props.content}</h1>
  );
}
