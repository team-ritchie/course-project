import style from './header.module.css';
import Logo from '../Logo/logo';
import { useStore } from '../../app/api/stores/store';
import { Dropdown, Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react-lite';

export default observer(function Header() {
  const {
    userStore: { user, logout },
  } = useStore();

  return (
    <div className={style.header}>
        <div className={style.headerContent}>
          <Logo width={32} />
          <p className={style.headerText}>Ritchie Board Games</p>
            <Image
              src={user?.image || '/assets/user.png'}
              avatar
              spaced="right"
            />
            <Dropdown pointing="top right" text={user?.displayName}>
              <Dropdown.Menu>
                <Dropdown.Item
                  as={Link}
                  to={`/profile/${user?.displayName}`}
                  text="Мой профиль"
                  icon="user"
                />
                <Dropdown.Item onClick={logout} text="Выйти" icon="power" />
              </Dropdown.Menu>
            </Dropdown>
        </div>
    </div>
  );
});
