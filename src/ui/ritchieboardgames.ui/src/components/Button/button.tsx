import { Link } from 'react-router-dom';
import style from './button.module.css';

interface Props {
  color?: 'red' | 'green' | 'blue';
  content: string;
  onClick?: () => void;
  to?: string;
  width?: string;
  type?: 'button' | 'submit' | 'reset';
  disabled?: boolean;
  loading?: boolean;
}

const Button = ({
  color = 'blue',
  content,
  onClick,
  to,
  width,
  type = 'button',
  disabled = false,
  loading = false,
}: Props) => {
  const className = style[color];
  return (
    <button
      type={type}
      style={{ width }}
      onClick={onClick}
      className={`${style.button} ${className} ${
        loading ? ' ui basic loading button' : ''
      }`}
      disabled={disabled}
    >
      {to ? (
        <Link to={to} className={style.link}>
          {content}
        </Link>
      ) : (
        content
      )}
    </button>
  );
};

export default Button;
