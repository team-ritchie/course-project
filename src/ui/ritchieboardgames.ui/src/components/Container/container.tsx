import style from './container.module.css';

export default function Container({ children }: any) {
    return <div className={style.container}>{children}</div>;
}
