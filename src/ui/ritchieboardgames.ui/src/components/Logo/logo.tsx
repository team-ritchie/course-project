import style from './logo.module.css';

interface Props {
  width: number;
  rotate?: boolean;
}

const Logo = ({ width, rotate = false }: Props) => {
  return (
    <img
      style={{ width }}
      src="/assets/logo.png"
      alt="Logo"
      className={`${style.logo} ${rotate ? style.logoRotate : ''}`}
    />
  );
};

export default Logo;
